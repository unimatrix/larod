/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <systemd/sd-bus.h>

#include "error.h"
#include "larod.h"

/**
 * @brief D-Bus signature for model basic data.
 *
 * t: model size in bytes.
 * t: model ID.
 * t: chip ID.
 * i: chip type (larodChip enum).
 * i: larodAccess enum.
 * s: model name.
 */
#define MODEL_SIGNATURE "tttiis"

/**
 * @brief Type containing information about a model.
 */
struct larodModel {
    // TODO: Track larodConnection here (whenever applicable).
    const uint64_t id;        ///< Model ID.
    const uint64_t chipId;    ///< Chip ID.
    const size_t size;        ///< Size of model.
    const char* name;         ///< Name of model.
    larodTensor** inputs;     ///< Pointers to input tensors.
    size_t numInputs;         ///< Number of input tensors.
    larodTensor** outputs;    ///< Pointers to output tensors.
    size_t numOutputs;        ///< Number of output tensors.
    const larodChip chip;     ///< Chip type.
    const larodAccess access; ///< Access specifier for model.
};

/**
 * @brief Extract model from sd-bus message.
 *
 * Returns true if a model was read from the message, or if the array of
 * models has reached its end. In the latter case @p model will be set to
 * NULL.
 *
 * @param msg Message to read model from.
 * @param model Output pointer to be filled with newly allocated model.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @see larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool readModelFromMessage(sd_bus_message* msg, larodModel** model,
                          larodError** error);
