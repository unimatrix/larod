/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include "larod.h"

/**
 * @brief Create an error handle.
 *
 * This will create and allocate an error handle containing error information.
 *
 * @param error An uninitialized error handle.
 * @param code Error code to be initialized in @p error.
 * @param fmt Error text to be initialized in @p error. It can contain
 * printf()-style format specifiers with appropriate arguments following @p fmt.
 * @return Errno-style return code. Value of zero means success.
 */
__attribute__((format(printf, 3, 4))) int
    larodCreateError(larodError** error, const larodErrorCode code,
                     const char* fmt, ...);

/**
 * @brief Prepend the error message of an error handle.
 *
 * @param fmt Error text to be prepended in @p error. It can contain
 * printf()-style format specifiers with appropriate arguments following @p fmt.
 * @return Errno-style return code. Value of zero means success.
 */
__attribute__((format(printf, 2, 3))) int
    larodPrependErrorMsg(larodError** error, const char* fmt, ...);
