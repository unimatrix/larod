/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "jobrequest.h"

#include <assert.h>
#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include "error.h"
#include "larod.h"
#include "model.h"
#include "tensor.h"

// #define MINIMUM_JOB_PRIORITY 0
#define MAXIMUM_JOB_PRIORITY 100
#define DEFAULT_JOB_PRIORITY 50

// InferenceRequest is renamed to JobRequest. larodCreateInferenceRequest is
// not included here since it's not a rename.
__asm__(".symver larodSetJobRequestInputs, "
        "larodSetInferenceRequestInputs@LIBLAROD_1.0.0");
__asm__(".symver larodSetJobRequestModel, "
        "larodSetInferenceRequestModel@LIBLAROD_1.0.0");
__asm__(".symver larodSetJobRequestOutputs, "
        "larodSetInferenceRequestOutputs@LIBLAROD_1.0.0");
__asm__(".symver larodSetJobRequestPriority, "
        "larodSetInferenceRequestPriority@LIBLAROD_1.0.0");
__asm__(".symver larodDestroyJobRequest, "
        "larodDestroyInferenceRequest@LIBLAROD_1.0.0");

larodJobRequest* larodCreateJobRequest(const larodModel* model,
                                       larodTensor** inputTensors,
                                       size_t numInputs,
                                       larodTensor** outputTensors,
                                       size_t numOutputs, larodMap* params,
                                       larodError** error) {
    // Check inputs.
    if (!model) {
        larodCreateError(error, EINVAL, "Model pointer is NULL");

        return NULL;
    }

    if (!inputTensors) {
        larodCreateError(error, EINVAL, "Input tensors pointer is NULL");

        return NULL;
    }

    if (!outputTensors) {
        larodCreateError(error, EINVAL, "Output tensors pointer is NULL");

        return NULL;
    }

    if (numInputs != model->numInputs) {
        larodCreateError(error, EINVAL,
                         "Number of input tensors does not match model (got "
                         "%zu, expected %zu)",
                         numInputs, model->numInputs);

        return NULL;
    }

    if (numOutputs != model->numOutputs) {
        larodCreateError(error, EINVAL,
                         "Number of output tensors does not match model (got "
                         "%zu, expected %zu)",
                         numOutputs, model->numOutputs);

        return NULL;
    }

    // Allocate job request.
    larodJobRequest* jobReq = calloc(1, sizeof(*jobReq));
    if (!jobReq) {
        larodCreateError(error, ENOMEM, "%s", strerror(ENOMEM));

        return NULL;
    }

    jobReq->modelId = model->id;
    jobReq->chipId = model->chipId;
    jobReq->priority = DEFAULT_JOB_PRIORITY;
    jobReq->inputs = NULL;
    jobReq->outputs = NULL;
    jobReq->params = NULL;

    jobReq->inputs = calloc(numInputs, sizeof(*jobReq->inputs));
    if (!jobReq->inputs) {
        larodCreateError(error, ENOMEM,
                         "Could not allocate job request inputs");

        goto error;
    }

    for (size_t i = 0; i < numInputs; ++i) {
        jobReq->inputs[i] = NULL;
    }

    jobReq->outputs = calloc(numOutputs, sizeof(*jobReq->outputs));
    if (!jobReq->outputs) {
        larodCreateError(error, ENOMEM,
                         "Could not allocate job request outputs");

        goto error;
    }

    for (size_t i = 0; i < numOutputs; ++i) {
        jobReq->outputs[i] = NULL;
    }

    // Copy input tensors.
    for (size_t i = 0; i < numInputs; i++) {
        const larodTensor* tensor = inputTensors[i];
        if (!tensor) {
            larodCreateError(error, EINVAL, "Input tensor %zu is NULL", i);

            goto error;
        }

        larodTensor* dupTensor = copyTensor(tensor, error);
        if (!dupTensor) {
            larodPrependErrorMsg(error, "Could not duplicate input tensor: ");

            goto error;
        }

        jobReq->inputs[i] = dupTensor;
        jobReq->numInputs++;
    }

    if (!validateTensors(jobReq->inputs, numInputs, error)) {
        larodPrependErrorMsg(error, "Input tensors are invalid: ");

        goto error;
    }

    // Copy output tensors.
    for (size_t i = 0; i < numOutputs; i++) {
        const larodTensor* t = outputTensors[i];
        if (!t) {
            larodCreateError(error, EINVAL, "Output tensor %zu is NULL", i);

            goto error;
        }

        larodTensor* dupTensor = copyTensor(t, error);
        if (!dupTensor) {
            larodPrependErrorMsg(error, "Could not duplicate output tensor: ");

            goto error;
        }

        jobReq->outputs[i] = dupTensor;
        jobReq->numOutputs++;
    }

    if (!validateTensors(jobReq->outputs, numOutputs, error)) {
        larodPrependErrorMsg(error, "Output tensors are invalid: ");

        goto error;
    }

    // Copy params.
    if (params) {
        jobReq->params = copyMap(params, error);
        if (!jobReq->params) {
            goto error;
        }
    }

    return jobReq;

error:
    larodDestroyJobRequest(&jobReq);
    return NULL;
}

larodJobRequest* larodCreateInferenceRequest(
    const larodModel* model, larodTensor** inputTensors, size_t numInputs,
    larodTensor** outputTensors, size_t numOutputs, larodError** error) {
    return larodCreateJobRequest(model, inputTensors, numInputs, outputTensors,
                                 numOutputs, NULL, error);
}

bool larodSetJobRequestInputs(larodJobRequest* jobReq, larodTensor** tensors,
                              const size_t numTensors, larodError** error) {
    if (!jobReq) {
        larodCreateError(error, EINVAL, "Job request pointer is NULL");

        return false;
    }

    if (!numTensors) {
        larodCreateError(error, EINVAL, "numTensors must be non-zero");

        return false;
    }

    if (!tensors) {
        larodCreateError(error, EINVAL, "Tensors pointer is NULL");

        return false;
    }

    if (!validateTensors(tensors, numTensors, error)) {
        return false;
    }

    larodTensor** newTensors = copyTensorArray(tensors, numTensors, error);
    if (!newTensors) {
        return false;
    }

    larodDestroyTensors(&jobReq->inputs, jobReq->numInputs);
    jobReq->inputs = newTensors;
    jobReq->numInputs = numTensors;

    return true;
}

bool larodSetJobRequestOutputs(larodJobRequest* jobReq, larodTensor** tensors,
                               size_t numTensors, larodError** error) {
    if (!jobReq) {
        larodCreateError(error, EINVAL, "Job request pointer is NULL");

        return false;
    }

    if (!numTensors) {
        larodCreateError(error, EINVAL, "numTensors must be non-zero");

        return false;
    }

    if (!tensors) {
        larodCreateError(error, EINVAL, "Tensor pointer is NULL");

        return false;
    }

    if (!validateTensors(tensors, numTensors, error)) {
        return false;
    }

    larodTensor** newTensors = copyTensorArray(tensors, numTensors, error);
    if (!newTensors) {
        return false;
    }

    larodDestroyTensors(&jobReq->outputs, jobReq->numOutputs);
    jobReq->outputs = newTensors;
    jobReq->numOutputs = numTensors;

    return true;
}

void larodDestroyJobRequest(larodJobRequest** jobReq) {
    if (!jobReq || !(*jobReq)) {
        return;
    }

    if ((*jobReq)->inputs) {
        larodDestroyTensors(&(*jobReq)->inputs, (*jobReq)->numInputs);
    }

    if ((*jobReq)->outputs) {
        larodDestroyTensors(&(*jobReq)->outputs, (*jobReq)->numOutputs);
    }

    if ((*jobReq)->params) {
        larodDestroyMap(&(*jobReq)->params);
    }

    free(*jobReq);
    *jobReq = NULL;
}

bool larodSetJobRequestPriority(larodJobRequest* jobReq, const uint8_t priority,
                                larodError** error) {
    if (!jobReq) {
        larodCreateError(error, EINVAL, "Pointer jobReq is NULL");

        return false;
    }

    if (priority > MAXIMUM_JOB_PRIORITY) {
        larodCreateError(error, EINVAL,
                         "Priority argument is too large (%hhu > %hhu)",
                         priority, MAXIMUM_JOB_PRIORITY);

        return false;
    }

    jobReq->priority = priority;

    return true;
}

bool larodSetJobRequestModel(larodJobRequest* jobReq, const larodModel* model,
                             larodError** error) {
    if (!jobReq) {
        larodCreateError(error, EINVAL, "Job request argument is NULL");
        return false;
    }
    if (!model) {
        larodCreateError(error, EINVAL, "Model argument is NULL");
        return false;
    }

    jobReq->modelId = model->id;
    return true;
}

bool larodSetJobRequestParams(larodJobRequest* jobReq, const larodMap* params,
                              larodError** error) {
    if (!jobReq) {
        larodCreateError(error, EINVAL, "jobReq is NULL");

        return false;
    }

    if (!params) {
        larodCreateError(error, EINVAL, "params is NULL");

        return false;
    }

    larodDestroyMap(&jobReq->params);
    jobReq->params = copyMap(params, error);
    if (!jobReq->params) {
        return false;
    }

    return true;
}

bool validateJobReqOpt(const larodJobRequest* jobReq) {
    assert(jobReq);
    assert(jobReq->inputs);
    assert(jobReq->outputs);

    // clang-format off
    return (jobReq->numInputs > 0) &&
           (jobReq->numOutputs > 0) &&
           (jobReq->priority <= MAXIMUM_JOB_PRIORITY) &&
           (jobReq->modelId != LAROD_INVALID_MODEL_ID);
    // clang-format on
}
