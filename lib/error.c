/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "error.h"

#include <assert.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

int larodCreateError(larodError** error, const larodErrorCode code,
                     const char* fmt, ...) {
    if (!error) {
        return EINVAL;
    }

    assert(fmt);
    assert(code <= LAROD_ERROR_MAX_ERRNO);

    char* errMsg = NULL;

    int ret = 0;
    *error = malloc(sizeof(larodError));
    if (!(*error)) {
        ret = ENOMEM;

        goto error;
    }

    va_list args;
    va_start(args, fmt);
    int len = vsnprintf(NULL, 0, fmt, args);
    va_end(args);
    if (len < 0) {
        ret = errno;

        goto error;
    }

    errMsg = malloc((size_t)(len + 1));
    if (!errMsg) {
        ret = ENOMEM;

        goto error;
    }

    va_start(args, fmt);
    int lenWritten = vsnprintf(errMsg, (size_t)(len + 1), fmt, args);
    va_end(args);

    if (lenWritten != len) {
        ret = errno;

        goto error;
    }

    (*error)->msg = errMsg;
    (*error)->code = code;

    return ret;

error:
    free(*error);
    free(errMsg);

    *error = NULL;

    return ret;
}

void larodClearError(larodError** error) {
    if (!error || !(*error)) {
        return;
    }

    free((char*) (*error)->msg);
    free(*error);

    (*error) = NULL;
}

int larodPrependErrorMsg(larodError** error, const char* fmt, ...) {
    if (!error || !*error) {
        return EINVAL;
    }

    assert(fmt);

    // Get length of the expanded format string.
    va_list args;
    va_start(args, fmt);
    int len = vsnprintf(NULL, 0, fmt, args);
    va_end(args);
    if (len < 0) {
        return errno;
    }

    // Concatenate error messages.
    char* catStr = malloc(strlen((*error)->msg) + (size_t) len + 1);
    if (!catStr) {
        return ENOMEM;
    }

    va_start(args, fmt);
    int lenWritten = vsnprintf(catStr, (size_t) len + 1, fmt, args);
    va_end(args);
    if (lenWritten != len) {
        free(catStr);

        return errno;
    }

    strcat(catStr, (*error)->msg);
    free((char*) (*error)->msg);
    (*error)->msg = catStr;

    return 0;
}
