/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef COM_AXIS_LAROD_VERSION_H
#define COM_AXIS_LAROD_VERSION_H

/**
 * @brief Version of larod as a string literal.
 *
 * This is a string literal in the format
 * "LAROD_VERSION_MAJOR.LAROD_VERSION_MINOR.LAROD_VERSION_PATCH", where
 * LAROD_VERSION_MAJOR, LAROD_VERSION_MINOR, LAROD_VERSION_PATCH are the macros
 * defined below.
 */
#define LAROD_VERSION_STR "@VERSION_STR@"

/**
 * @brief Major version of larod.
 */
#define LAROD_VERSION_MAJOR @VERSION_MAJOR@

/**
 * @brief Minor version of larod.
 */
#define LAROD_VERSION_MINOR @VERSION_MINOR@

/**
 * @brief Patch version of larod.
 */
#define LAROD_VERSION_PATCH @VERSION_PATCH@

#endif
