/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "hashset.h"

#include <gtest/gtest.h>

class HashSetTest : public ::testing::Test {
protected:
    HashSetTest()
        : set(nullptr), data1(reinterpret_cast<void*>(0x01)),
          data2(reinterpret_cast<void*>(0x02)),
          data3(reinterpret_cast<void*>(0x03)) {
        hashSetConstruct(&set);
    };
    ~HashSetTest() { hashSetDestruct(&set); }

    HashSet* set;
    void* data1;
    void* data2;
    void* data3;
};

TEST_F(HashSetTest, SizeIsZeroInitially) {
    size_t sz;
    ASSERT_EQ(hashSetGetSize(set, &sz), 0);
    ASSERT_EQ(sz, 0);

    void** data;
    ASSERT_EQ(hashSetExtractAll(set, &data, &sz), 0);
    ASSERT_EQ(sz, 0);
    free(data);
}

TEST_F(HashSetTest, EraseNonExistent) {
    ASSERT_EQ(hashSetErase(set, data1), ENODATA);
}

TEST_F(HashSetTest, EraseNull) {
    ASSERT_EQ(hashSetErase(set, nullptr), EINVAL);
}

TEST_F(HashSetTest, InsertNull) {
    ASSERT_EQ(hashSetInsert(&set, nullptr), EINVAL);
}

TEST_F(HashSetTest, InsertOne) {
    ASSERT_EQ(hashSetInsert(&set, data1), 0);

    size_t sz;
    ASSERT_EQ(hashSetGetSize(set, &sz), 0);
    ASSERT_EQ(sz, 1);

    void** data;
    ASSERT_EQ(hashSetExtractAll(set, &data, &sz), 0);
    ASSERT_EQ(sz, 1);
    ASSERT_EQ(data[0], data1);
    free(data);
}

TEST_F(HashSetTest, InsertSeveral) {
    ASSERT_EQ(hashSetInsert(&set, data1), 0);
    ASSERT_EQ(hashSetInsert(&set, data2), 0);
    ASSERT_EQ(hashSetInsert(&set, data3), 0);

    size_t sz;
    ASSERT_EQ(hashSetGetSize(set, &sz), 0);
    ASSERT_EQ(sz, 3);

    void** data;
    ASSERT_EQ(hashSetExtractAll(set, &data, &sz), 0);
    ASSERT_EQ(sz, 3);
    bool hasElement[3] = {false, false, false};
    for (size_t i = 0; i < 3; ++i) {
        hasElement[reinterpret_cast<uintptr_t>(data[i]) - 1] = true;
    }
    ASSERT_TRUE(hasElement[0]);
    ASSERT_TRUE(hasElement[1]);
    ASSERT_TRUE(hasElement[2]);
    free(data);
}

TEST_F(HashSetTest, EraseOne) {
    ASSERT_EQ(hashSetInsert(&set, data1), 0);

    ASSERT_EQ(hashSetErase(set, data1), 0);

    size_t sz;
    ASSERT_EQ(hashSetGetSize(set, &sz), 0);
    ASSERT_EQ(sz, 0);

    void** data;
    ASSERT_EQ(hashSetExtractAll(set, &data, &sz), 0);
    ASSERT_EQ(sz, 0);
    free(data);
}

TEST_F(HashSetTest, EraseSeveral) {
    ASSERT_EQ(hashSetInsert(&set, data1), 0);
    ASSERT_EQ(hashSetInsert(&set, data2), 0);
    ASSERT_EQ(hashSetInsert(&set, data3), 0);

    ASSERT_EQ(hashSetErase(set, data1), 0);
    ASSERT_EQ(hashSetErase(set, data2), 0);
    ASSERT_EQ(hashSetErase(set, data3), 0);

    size_t sz;
    ASSERT_EQ(hashSetGetSize(set, &sz), 0);
    ASSERT_EQ(sz, 0);

    void** data;
    ASSERT_EQ(hashSetExtractAll(set, &data, &sz), 0);
    ASSERT_EQ(sz, 0);
    free(data);
}

TEST_F(HashSetTest, RehashingWorks) {
    static constexpr size_t NBR_OF_ELEMENTS = 130;

    for (size_t i = 1; i <= NBR_OF_ELEMENTS; ++i) {
        ASSERT_EQ(hashSetInsert(&set, reinterpret_cast<void*>(i)), 0);
    }

    size_t sz;
    ASSERT_EQ(hashSetGetSize(set, &sz), 0);
    ASSERT_EQ(sz, NBR_OF_ELEMENTS);
}
