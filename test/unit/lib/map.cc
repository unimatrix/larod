/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <limits>

#include "larod.h"
#include "testutils.hh"

extern "C" {
#include "map.h"
}

#include <gtest/gtest.h>

// Maximum length for keys and string values.
#define MAX_LEN_STR 128

using namespace std;
using namespace larod::test;

static bool hasError(bool ret, larodError** error) {
    larodError* err = *error;
    larodClearError(error);
    return !ret && err;
}

static bool noError(bool ret, larodError** error) {
    if (*error) {
        string msg = getErrMsgAndClear(error);
        gtestPrint(string("ERROR: ") + msg);
        return false;
    } else {
        return ret;
    }
}

class MapTest : public ::testing::Test {
protected:
    MapTest() {
        map = larodCreateMap(&error);
        if (error) {
            string msg = getErrMsgAndClear(&error);
            gtestPrint(msg);
            exit(EXIT_FAILURE);
        }
    }

    ~MapTest() {
        larodDestroyMap(&map);
        larodClearError(&error);
    }

    void successfulSetInt(const char* key, int64_t value) {
        bool ret = larodMapSetInt(map, key, value, &error);
        ASSERT_TRUE(noError(ret, &error));
    }

    void unsuccessfulSetInt(const char* key, int64_t value) {
        bool ret = larodMapSetInt(map, key, value, &error);
        ASSERT_TRUE(hasError(ret, &error));
    }

    void successfulGetInt(const char* key, int64_t expectedValue) {
        // Make sure they differ initially.
        int64_t actualValue = expectedValue + 1;
        bool ret = larodMapGetInt(map, key, &actualValue, &error);
        ASSERT_EQ(expectedValue, actualValue);
        ASSERT_TRUE(noError(ret, &error));
    }

    void unsuccessfulGetInt(const char* key) {
        int64_t value = 0;
        bool ret = larodMapGetInt(map, key, &value, &error);
        ASSERT_EQ(value, 0);
        ASSERT_TRUE(hasError(ret, &error));
    }

    void successfulSetStr(const char* key, const char* value) {
        bool ret = larodMapSetStr(map, key, value, &error);
        ASSERT_TRUE(noError(ret, &error));
    }

    void unsuccessfulSetStr(const char* key, const char* value) {
        bool ret = larodMapSetStr(map, key, value, &error);
        ASSERT_TRUE(hasError(ret, &error));
    }

    void successfulGetStr(const char* key, const char* expectedValue) {
        const char* actualValue = larodMapGetStr(map, key, &error);
        ASSERT_TRUE(noError(true, &error));
        ASSERT_STREQ(expectedValue, actualValue);
    }

    void unsuccessfulGetStr(const char* key) {
        const char* value = larodMapGetStr(map, key, &error);
        ASSERT_FALSE(value);
        ASSERT_TRUE(hasError(false, &error));
    }

    void successfulSetIntArr2(const char* key, int64_t v1, int64_t v2) {
        bool ret = larodMapSetIntArr2(map, key, v1, v2, &error);
        ASSERT_TRUE(noError(ret, &error));
    }

    void unsuccessfulSetIntArr2(const char* key, int64_t v1, int64_t v2) {
        bool ret = larodMapSetIntArr2(map, key, v1, v2, &error);
        ASSERT_TRUE(hasError(ret, &error));
    }

    void successfulGetIntArr2(const char* key, int64_t e0, int64_t e1) {
        const int64_t* actualValue = larodMapGetIntArr2(map, key, &error);
        ASSERT_TRUE(noError(true, &error));
        ASSERT_EQ(e0, actualValue[0]);
        ASSERT_EQ(e1, actualValue[1]);
    }

    void unsuccessfulGetIntArr2(const char* key) {
        const int64_t* value = larodMapGetIntArr2(map, key, &error);
        ASSERT_FALSE(value);
        ASSERT_TRUE(hasError(false, &error));
    }

    void successfulSetIntArr4(const char* key, int64_t v1, int64_t v2,
                              int64_t v3, int64_t v4) {
        bool ret = larodMapSetIntArr4(map, key, v1, v2, v3, v4, &error);
        ASSERT_TRUE(noError(ret, &error));
    }

    void unsuccessfulSetIntArr4(const char* key, int64_t v1, int64_t v2,
                                int64_t v3, int64_t v4) {
        bool ret = larodMapSetIntArr4(map, key, v1, v2, v3, v4, &error);
        ASSERT_TRUE(hasError(ret, &error));
    }

    void successfulGetIntArr4(const char* key, int64_t e0, int64_t e1,
                              int64_t e2, int64_t e3) {
        const int64_t* actualValue = larodMapGetIntArr4(map, key, &error);
        ASSERT_TRUE(noError(true, &error));
        ASSERT_EQ(e0, actualValue[0]);
        ASSERT_EQ(e1, actualValue[1]);
        ASSERT_EQ(e2, actualValue[2]);
        ASSERT_EQ(e3, actualValue[3]);
    }

    void unsuccessfulGetIntArr4(const char* key) {
        const int64_t* value = larodMapGetIntArr4(map, key, &error);
        ASSERT_FALSE(value);
        ASSERT_TRUE(hasError(false, &error));
    }

    larodMap* map = nullptr;
    larodError* error = nullptr;
    const int64_t a0 = 1;
    const int64_t a1 = 2;
    const int64_t a2 = 3;
    const int64_t a3 = 4;
};

TEST_F(MapTest, EmptyKeys) {
    unsuccessfulSetInt("", 123);
    unsuccessfulSetStr("", "value");
    unsuccessfulSetIntArr2("", a0, a1);
    unsuccessfulSetIntArr4("", a0, a1, a2, a3);

    unsuccessfulGetInt("");
    unsuccessfulGetStr("");
    unsuccessfulGetIntArr2("");
    unsuccessfulGetIntArr4("");
}

TEST_F(MapTest, NullKeys) {
    unsuccessfulSetInt(nullptr, 123);
    unsuccessfulSetStr(nullptr, "value");
    unsuccessfulSetIntArr2(nullptr, a0, a1);
    unsuccessfulSetIntArr4(nullptr, a0, a1, a2, a3);

    unsuccessfulGetInt(nullptr);
    unsuccessfulGetStr(nullptr);
    unsuccessfulGetIntArr2(nullptr);
    unsuccessfulGetIntArr4(nullptr);
}

TEST_F(MapTest, TooLongKeys) {
    string keyStr(MAX_LEN_STR, 'a');
    const char* key = keyStr.c_str();

    unsuccessfulSetInt(key, 123);
    unsuccessfulSetStr(key, "value");
    unsuccessfulSetIntArr2(key, a0, a1);
    unsuccessfulSetIntArr4(key, a0, a1, a2, a3);

    unsuccessfulGetInt(key);
    unsuccessfulGetStr(key);
    unsuccessfulGetIntArr2(key);
    unsuccessfulGetIntArr4(key);
}

TEST_F(MapTest, MaxLenKeys) {
    string intKey(MAX_LEN_STR - 1, 'i');
    successfulSetInt(intKey.c_str(), 123);

    string strKey(MAX_LEN_STR - 1, 's');
    successfulSetStr(strKey.c_str(), "value");

    string arr2Key(MAX_LEN_STR - 1, 'a');
    successfulSetIntArr2(arr2Key.c_str(), a0, a1);

    string arr4Key(MAX_LEN_STR - 1, 'b');
    successfulSetIntArr4(arr4Key.c_str(), a0, a1, a2, a3);

    successfulGetInt(intKey.c_str(), 123);
    successfulGetStr(strKey.c_str(), "value");
    successfulGetIntArr2(arr2Key.c_str(), a0, a1);
    successfulGetIntArr4(arr4Key.c_str(), a0, a1, a2, a3);
}

TEST_F(MapTest, TooLongValues) {
    string value(MAX_LEN_STR, 'v');
    unsuccessfulSetStr("key", value.c_str());

    unsuccessfulGetStr("key");
}

TEST_F(MapTest, MaxLenValues) {
    string value(MAX_LEN_STR - 1, 'v');
    successfulSetStr("key", value.c_str());

    successfulGetStr("key", value.c_str());
}

TEST_F(MapTest, NullErrors) {
    bool ret = larodMapSetInt(map, "i", 42, nullptr);
    ASSERT_TRUE(ret);

    int64_t value = 0;
    ret = larodMapGetInt(map, "i", &value, nullptr);
    ASSERT_EQ(value, 42);
    ASSERT_TRUE(ret);

    ret = larodMapGetInt(map, "unknown_key", &value, nullptr);
    ASSERT_EQ(value, 42);
    ASSERT_FALSE(ret);

    ret = larodMapSetStr(map, "s", "value", nullptr);
    ASSERT_TRUE(ret);

    const char* s = larodMapGetStr(map, "s", nullptr);
    ASSERT_STREQ(s, "value");

    ret = larodMapSetIntArr2(map, "arr2", a0, a1, nullptr);
    ASSERT_TRUE(ret);

    const int64_t* arr2 = larodMapGetIntArr2(map, "arr2", nullptr);
    ASSERT_EQ(arr2[0], a0);
    ASSERT_EQ(arr2[1], a1);

    ret = larodMapSetIntArr4(map, "arr4", a0, a1, a2, a3, nullptr);
    ASSERT_TRUE(ret);

    const int64_t* arr4 = larodMapGetIntArr4(map, "arr4", nullptr);
    ASSERT_EQ(arr4[0], a0);
    ASSERT_EQ(arr4[1], a1);
    ASSERT_EQ(arr4[2], a2);
    ASSERT_EQ(arr4[3], a3);
}

TEST_F(MapTest, NullValues) {
    successfulSetInt("i", 123);

    unsuccessfulSetStr("s", nullptr);

    bool ret = larodMapGetInt(map, "i", nullptr, &error);
    ASSERT_TRUE(hasError(ret, &error));

    ret = larodMapGetInt(map, "unknown_key", nullptr, &error);
    ASSERT_TRUE(hasError(ret, &error));
}

TEST_F(MapTest, NullArgForLarodMap) {
    bool ret = larodMapSetInt(nullptr, "i", 1, &error);
    ASSERT_TRUE(hasError(ret, &error));

    ret = larodMapSetStr(nullptr, "s", "value", &error);
    ASSERT_TRUE(hasError(ret, &error));

    ret = larodMapSetIntArr2(nullptr, "arr2", a0, a1, &error);
    ASSERT_TRUE(hasError(ret, &error));

    ret = larodMapSetInt(nullptr, "arr4", 1, &error);
    ASSERT_TRUE(hasError(ret, &error));

    int64_t value = -1;
    ret = larodMapGetInt(nullptr, "i", &value, &error);
    ASSERT_TRUE(hasError(ret, &error));
    ASSERT_EQ(value, -1);

    const char* s = larodMapGetStr(nullptr, "s", &error);
    ASSERT_FALSE(s);
    ASSERT_TRUE(hasError(false, &error));

    const int64_t* arr2 = larodMapGetIntArr2(nullptr, "arr2", &error);
    ASSERT_FALSE(arr2);
    ASSERT_TRUE(hasError(false, &error));

    const int64_t* arr4 = larodMapGetIntArr4(nullptr, "arr4", &error);
    ASSERT_FALSE(arr4);
    ASSERT_TRUE(hasError(false, &error));
}

TEST_F(MapTest, SetAndGetInt) {
    successfulSetInt("key", 42);
    successfulGetInt("key", 42);
}

TEST_F(MapTest, SetAndGetStr) {
    successfulSetStr("key", "value");
    successfulGetStr("key", "value");
}

TEST_F(MapTest, SetAndGetIntArr2) {
    successfulSetIntArr2("key", a0, a1);
    successfulGetIntArr2("key", a0, a1);
}

TEST_F(MapTest, SetAndGetIntArr4) {
    successfulSetIntArr4("key", a0, a1, a2, a3);
    successfulGetIntArr4("key", a0, a1, a2, a3);
}

TEST_F(MapTest, OverwriteValue) {
    successfulSetInt("key", 1);
    successfulGetInt("key", 1);

    successfulSetInt("key", 2);
    successfulGetInt("key", 2);
}

TEST_F(MapTest, OverwriteValueWithNewType) {
    successfulSetInt("key", 1);

    unsuccessfulSetStr("key", "value");
    unsuccessfulSetIntArr2("key", a0, a1);
    unsuccessfulSetIntArr4("key", a0, a1, a2, a3);

    successfulGetInt("key", 1);
}

TEST_F(MapTest, GetWrongValueTypes) {
    successfulSetStr("key", "value");

    unsuccessfulGetInt("key");
    unsuccessfulGetIntArr2("key");
    unsuccessfulGetIntArr4("key");

    successfulGetStr("key", "value");
}

TEST_F(MapTest, GetFromNonExistentKeys) {
    unsuccessfulGetInt("key");
    unsuccessfulGetStr("key");
    unsuccessfulGetIntArr2("key");
    unsuccessfulGetIntArr4("key");
}

TEST_F(MapTest, MultipleValidSetsAndGets) {
    successfulSetIntArr2("arr2", a0, a1);
    successfulSetIntArr4("arr4", a0, a1, a2, a3);
    successfulSetInt("int", numeric_limits<int64_t>::min());

    string strKey(MAX_LEN_STR - 1, 'k');
    successfulSetStr(strKey.c_str(), "value");

    successfulGetIntArr2("arr2", a0, a1);
    successfulGetIntArr4("arr4", a0, a1, a2, a3);
    successfulGetInt("int", numeric_limits<int64_t>::min());
    successfulGetStr(strKey.c_str(), "value");
}
