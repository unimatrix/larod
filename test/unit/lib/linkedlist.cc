/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "linkedlist.h"

#include <gtest/gtest.h>

class LinkedListTest : public ::testing::Test {
protected:
    LinkedListTest()
        : list(nullptr), data1(reinterpret_cast<void*>(0x01)),
          data2(reinterpret_cast<void*>(0x02)),
          data3(reinterpret_cast<void*>(0x03)) {
        listConstruct(&list);
    }
    ~LinkedListTest() { listDestruct(&list); }

    LinkedList* list;
    void* data1;
    void* data2;
    void* data3;
};

TEST_F(LinkedListTest, SizeIsZeroInitially) {
    uint64_t sz;
    ASSERT_EQ(listGetSize(list, &sz), 0);
    ASSERT_EQ(sz, 0);
}

TEST_F(LinkedListTest, EraseNull) {
    void* data = nullptr;
    ASSERT_EQ(listErase(list, data), EINVAL);
}

TEST_F(LinkedListTest, EraseFromEmpty) {
    ASSERT_EQ(listErase(list, data1), ENODATA);
}

TEST_F(LinkedListTest, firstNodeIsNull) {
    const Node* node;
    ASSERT_EQ(listGetFirstNode(list, &node), 0);
    ASSERT_EQ(node, nullptr);
}

TEST_F(LinkedListTest, AddOneAndErase) {
    ASSERT_EQ(listPushBack(list, data1), 0);

    uint64_t sz;
    ASSERT_EQ(listGetSize(list, &sz), 0);
    ASSERT_EQ(sz, 1);

    const Node* node;
    ASSERT_EQ(listGetFirstNode(list, &node), 0);
    ASSERT_EQ(node->data, data1);

    ASSERT_EQ(listErase(list, data1), 0);

    ASSERT_EQ(listGetSize(list, &sz), 0);
    ASSERT_EQ(sz, 0);

    ASSERT_EQ(listGetFirstNode(list, &node), 0);
    ASSERT_EQ(node, nullptr);

    ASSERT_EQ(listErase(list, data1), ENODATA);
}

TEST_F(LinkedListTest, AddOneAndEraseAndAdd) {
    ASSERT_EQ(listPushBack(list, data1), 0);

    ASSERT_EQ(listErase(list, data1), 0);

    ASSERT_EQ(listPushBack(list, data2), 0);

    uint64_t sz;
    ASSERT_EQ(listGetSize(list, &sz), 0);
    ASSERT_EQ(sz, 1);

    const Node* node;
    ASSERT_EQ(listGetFirstNode(list, &node), 0);
    ASSERT_EQ(node->data, data2);
    ASSERT_EQ(node->next, nullptr);
}

TEST_F(LinkedListTest, AddTwoAndEraseLast) {
    ASSERT_EQ(listPushBack(list, data1), 0);
    ASSERT_EQ(listPushBack(list, data2), 0);

    uint64_t sz;
    ASSERT_EQ(listGetSize(list, &sz), 0);
    ASSERT_EQ(sz, 2);

    const Node* node;
    ASSERT_EQ(listGetFirstNode(list, &node), 0);
    ASSERT_EQ(node->data, data1);

    ASSERT_EQ(listErase(list, data2), 0);

    ASSERT_EQ(listGetSize(list, &sz), 0);
    ASSERT_EQ(sz, 1);

    ASSERT_EQ(listGetFirstNode(list, &node), 0);
    ASSERT_EQ(node->data, data1);
    ASSERT_EQ(node->next, nullptr);

    ASSERT_EQ(listErase(list, data2), ENODATA);
}

TEST_F(LinkedListTest, AddTwoAndEraseFirst) {
    ASSERT_EQ(listPushBack(list, data1), 0);
    ASSERT_EQ(listPushBack(list, data2), 0);

    ASSERT_EQ(listErase(list, data1), 0);

    uint64_t sz;
    ASSERT_EQ(listGetSize(list, &sz), 0);
    ASSERT_EQ(sz, 1);

    const Node* node;
    ASSERT_EQ(listGetFirstNode(list, &node), 0);
    ASSERT_EQ(node->data, data2);
    ASSERT_EQ(node->next, nullptr);

    ASSERT_EQ(listErase(list, data1), ENODATA);
}

TEST_F(LinkedListTest, AddTwoAndEraseLastAndAdd) {
    ASSERT_EQ(listPushBack(list, data1), 0);
    ASSERT_EQ(listPushBack(list, data2), 0);

    ASSERT_EQ(listErase(list, data2), 0);

    ASSERT_EQ(listPushBack(list, data3), 0);

    uint64_t sz;
    ASSERT_EQ(listGetSize(list, &sz), 0);
    ASSERT_EQ(sz, 2);

    const Node* node;
    ASSERT_EQ(listGetFirstNode(list, &node), 0);
    ASSERT_EQ(node->data, data1);
    ASSERT_EQ(node->next->data, data3);

    ASSERT_EQ(listErase(list, data2), ENODATA);
}

TEST_F(LinkedListTest, AddSeveralAndEraseMiddle) {
    ASSERT_EQ(listPushBack(list, data1), 0);
    ASSERT_EQ(listPushBack(list, data2), 0);
    ASSERT_EQ(listPushBack(list, data3), 0);

    uint64_t sz;
    ASSERT_EQ(listGetSize(list, &sz), 0);
    ASSERT_EQ(sz, 3);

    ASSERT_EQ(listErase(list, data2), 0);

    ASSERT_EQ(listGetSize(list, &sz), 0);
    ASSERT_EQ(sz, 2);

    const Node* node;
    ASSERT_EQ(listGetFirstNode(list, &node), 0);
    ASSERT_EQ(node->data, data1);
    ASSERT_EQ(node->next->data, data3);
    ASSERT_EQ(node->next->next, nullptr);

    ASSERT_EQ(listErase(list, data2), ENODATA);
}
