/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <cmath>
#include <gtest/gtest.h>

#include "larod.h"
#include "tensormetadata.hh"

using namespace std;
using namespace larod;

/**
 * @brief Test fixture for TensorMetadata unit tests.
 *
 * Sets up some mock data used in the tests.
 */
class TensorMetadataTest : public ::testing::Test {
public:
    // Valid constructor input.
    const larodTensorDataType VALID_DATA_TYPE = LAROD_TENSOR_DATA_TYPE_INT8;
    const larodTensorLayout VALID_LAYOUT = LAROD_TENSOR_LAYOUT_NCHW;
    const vector<size_t> VALID_DIMS = {1, 224, 224, 3};
    const vector<size_t> VALID_PITCHES = {224 * 224 * 3, 224 * 224 * 3, 224 * 3,
                                          3};
    const size_t VALID_BYTE_SIZE = 1 * 224 * 224 * 3 * 1;
    const string VALID_NAME = "Tensor Name";

    // Valid but != to valid input data above.
    const larodTensorDataType DIFFERENT_DATA_TYPE =
        LAROD_TENSOR_DATA_TYPE_FLOAT32;
    const larodTensorLayout DIFFERENT_LAYOUT = LAROD_TENSOR_LAYOUT_NHWC;
    const size_t DIFFERENT_BYTE_SIZE = 1 * 224 * 300 * 3 * 4;
    const vector<size_t> DIFFERENT_DIMS = {1, 224, 300, 3};
    const vector<size_t> DIFFERENT_PITCHES = {
        224 * 300 * 3 * 4, 224 * 300 * 3 * 4, 300 * 3 * 4, 3 * 4};

    const TensorMetadata VALID_TENSOR =
        TensorMetadata(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS, VALID_PITCHES,
                       VALID_BYTE_SIZE, VALID_NAME);
};

TEST_F(TensorMetadataTest, ConstructorValidInput) {
    ASSERT_NO_THROW(TensorMetadata(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS,
                                   VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME));
}

TEST_F(TensorMetadataTest, ConstructorUndefinedByteSize) {
    ASSERT_NO_THROW(TensorMetadata(VALID_DATA_TYPE, LAROD_TENSOR_LAYOUT_NHWC,
                                   VALID_DIMS, VALID_PITCHES, 0, VALID_NAME));
}

TEST_F(TensorMetadataTest, ConstructorUnspecifiedDims) {
    ASSERT_NO_THROW(TensorMetadata(VALID_DATA_TYPE, VALID_LAYOUT, {},
                                   VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME));
}

TEST_F(TensorMetadataTest, ConstructorUnspecifiedPitches) {
    ASSERT_NO_THROW(TensorMetadata(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS,
                                   {}, VALID_BYTE_SIZE, VALID_NAME));
}

TEST_F(TensorMetadataTest, ConstructorInvalidDataType) {
    ASSERT_THROW(TensorMetadata(LAROD_TENSOR_DATA_TYPE_INVALID, VALID_LAYOUT,
                                VALID_DIMS, VALID_PITCHES, VALID_BYTE_SIZE,
                                VALID_NAME),
                 invalid_argument);
}

TEST_F(TensorMetadataTest, ConstructorTooLargeDataType) {
    ASSERT_THROW(TensorMetadata(static_cast<larodTensorDataType>(
                                    LAROD_TENSOR_DATA_TYPE_MAX + 1),
                                VALID_LAYOUT, VALID_DIMS, VALID_PITCHES,
                                VALID_BYTE_SIZE, VALID_NAME),
                 invalid_argument);
}

TEST_F(TensorMetadataTest, ConstructorInvalidLayout) {
    ASSERT_THROW(TensorMetadata(VALID_DATA_TYPE, LAROD_TENSOR_LAYOUT_INVALID,
                                VALID_DIMS, VALID_PITCHES, VALID_BYTE_SIZE,
                                VALID_NAME),
                 invalid_argument);
}

TEST_F(TensorMetadataTest, ConstructorTooLargeLayout) {
    ASSERT_THROW(
        TensorMetadata(
            VALID_DATA_TYPE,
            static_cast<larodTensorLayout>(LAROD_TENSOR_LAYOUT_MAX + 1),
            VALID_DIMS, VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME),
        invalid_argument);
}

TEST_F(TensorMetadataTest, ConstructorInvalidDims) {
    ASSERT_THROW(TensorMetadata(VALID_DATA_TYPE, VALID_LAYOUT, {1, 0, 224, 3},
                                VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME),
                 invalid_argument);
}

TEST_F(TensorMetadataTest, ConstructorTooLongDims) {
    vector<size_t> TOO_LONG_DIMS(LAROD_TENSOR_MAX_LEN + 1, 1);

    ASSERT_THROW(TensorMetadata(VALID_DATA_TYPE,
                                LAROD_TENSOR_LAYOUT_UNSPECIFIED, TOO_LONG_DIMS,
                                VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME),
                 invalid_argument);
}

TEST_F(TensorMetadataTest, ConstructorNCHWWithDimsMismatch) {
    ASSERT_THROW(TensorMetadata(VALID_DATA_TYPE, LAROD_TENSOR_LAYOUT_NCHW,
                                {1, 224, 3}, VALID_PITCHES, 0, VALID_NAME),
                 invalid_argument);
}

TEST_F(TensorMetadataTest, ConstructorNHWCWithDimsMismatch) {
    ASSERT_THROW(TensorMetadata(VALID_DATA_TYPE, LAROD_TENSOR_LAYOUT_NHWC,
                                {1, 224, 3}, VALID_PITCHES, VALID_BYTE_SIZE,
                                VALID_NAME),
                 invalid_argument);
}

TEST_F(TensorMetadataTest, ConstructorTooShortPitches) {
    ASSERT_THROW(TensorMetadata(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS,
                                {224 * 224, 224 * 224, 224}, VALID_BYTE_SIZE,
                                VALID_NAME),
                 invalid_argument);
}

TEST_F(TensorMetadataTest, ConstructorTooLongPitches) {
    ASSERT_THROW(TensorMetadata(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS,
                                {224 * 224 * 3, 224 * 224 * 3, 224 * 3, 3, 1},
                                VALID_BYTE_SIZE, VALID_NAME),
                 invalid_argument);
}

TEST_F(TensorMetadataTest, ConstructorInvalidZeroPitches) {
    ASSERT_THROW(TensorMetadata(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS,
                                {0, 224 * 224 * 3, 224 * 3, 3}, VALID_BYTE_SIZE,
                                VALID_NAME),
                 invalid_argument);
}

TEST_F(TensorMetadataTest, ConstructorInvalidPitches) {
    ASSERT_THROW(TensorMetadata(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS,
                                DIFFERENT_PITCHES, VALID_BYTE_SIZE, VALID_NAME),
                 invalid_argument);
}

TEST_F(TensorMetadataTest, AssertEqualSameInput) {
    const TensorMetadata VALID_TENSOR_COPY =
        TensorMetadata(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS, VALID_PITCHES,
                       VALID_BYTE_SIZE, VALID_NAME);

    ASSERT_NO_THROW(
        TensorMetadata::assertEqual(VALID_TENSOR, VALID_TENSOR_COPY));
}

TEST_F(TensorMetadataTest, AssertEqualDifferentDataType) {
    const TensorMetadata DIFFERENT_DATA_TYPE_TENSOR =
        TensorMetadata(DIFFERENT_DATA_TYPE, VALID_LAYOUT, VALID_DIMS,
                       VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME);

    ASSERT_THROW(
        TensorMetadata::assertEqual(VALID_TENSOR, DIFFERENT_DATA_TYPE_TENSOR),
        invalid_argument);
}

TEST_F(TensorMetadataTest, AssertEqualDifferentLayout) {
    const TensorMetadata DIFFERENT_LAYOUT_TENSOR =
        TensorMetadata(VALID_DATA_TYPE, DIFFERENT_LAYOUT, VALID_DIMS,
                       VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME);

    ASSERT_THROW(
        TensorMetadata::assertEqual(VALID_TENSOR, DIFFERENT_LAYOUT_TENSOR),
        invalid_argument);
}

TEST_F(TensorMetadataTest, AssertEqualDifferentDims) {
    const TensorMetadata DIFFERENT_DIMS_TENSOR =
        TensorMetadata(VALID_DATA_TYPE, VALID_LAYOUT, DIFFERENT_DIMS,
                       VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME);

    ASSERT_THROW(
        TensorMetadata::assertEqual(VALID_TENSOR, DIFFERENT_DIMS_TENSOR),
        invalid_argument);
}

TEST_F(TensorMetadataTest, AssertEqualDifferentPitches) {
    vector<size_t> ALIGNED_PITCHES(4);
    ALIGNED_PITCHES[3] =
        static_cast<size_t>(ceil(static_cast<float>(VALID_DIMS[3]) / 32.f)) *
        32;
    ALIGNED_PITCHES[2] = ALIGNED_PITCHES[3] * VALID_DIMS[2];
    ALIGNED_PITCHES[1] = ALIGNED_PITCHES[2] * VALID_DIMS[1];
    ALIGNED_PITCHES[0] = ALIGNED_PITCHES[1] * VALID_DIMS[0];
    const TensorMetadata DIFFERENT_PITCHES_TENSOR =
        TensorMetadata(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS,
                       ALIGNED_PITCHES, ALIGNED_PITCHES[0], VALID_NAME);

    ASSERT_THROW(
        TensorMetadata::assertEqual(VALID_TENSOR, DIFFERENT_PITCHES_TENSOR),
        invalid_argument);
}

TEST_F(TensorMetadataTest, AssertEqualDifferentByteSize) {
    const TensorMetadata DIFFERENT_BYTE_SIZE_TENSOR =
        TensorMetadata(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS, {},
                       DIFFERENT_BYTE_SIZE, VALID_NAME);

    ASSERT_THROW(
        TensorMetadata::assertEqual(VALID_TENSOR, DIFFERENT_BYTE_SIZE_TENSOR),
        invalid_argument);
}

TEST_F(TensorMetadataTest, AssertEqualUnspecifiedDataType) {
    const TensorMetadata UNSPECIFIED_DATA_TYPE_TENSOR =
        TensorMetadata(LAROD_TENSOR_DATA_TYPE_UNSPECIFIED, VALID_LAYOUT,
                       VALID_DIMS, VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME);

    ASSERT_NO_THROW(TensorMetadata::assertEqual(VALID_TENSOR,
                                                UNSPECIFIED_DATA_TYPE_TENSOR));
}

TEST_F(TensorMetadataTest, AssertEqualUnspecifiedLayout) {
    const TensorMetadata UNSPECIFIED_LAYOUT_TENSOR =
        TensorMetadata(VALID_DATA_TYPE, LAROD_TENSOR_LAYOUT_UNSPECIFIED,
                       VALID_DIMS, VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME);

    ASSERT_NO_THROW(
        TensorMetadata::assertEqual(VALID_TENSOR, UNSPECIFIED_LAYOUT_TENSOR));
}

TEST_F(TensorMetadataTest, AssertEqualUnspecifiedDims) {
    const TensorMetadata UNSPECIFIED_DIMS_TENSOR =
        TensorMetadata(VALID_DATA_TYPE, VALID_LAYOUT, {}, VALID_PITCHES,
                       VALID_BYTE_SIZE, VALID_NAME);

    ASSERT_NO_THROW(
        TensorMetadata::assertEqual(VALID_TENSOR, UNSPECIFIED_DIMS_TENSOR));
}

TEST_F(TensorMetadataTest, AssertEqualUnspecifiedPitches) {
    const TensorMetadata UNSPECIFIED_DIMS_TENSOR =
        TensorMetadata(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS, {},
                       VALID_BYTE_SIZE, VALID_NAME);

    ASSERT_NO_THROW(
        TensorMetadata::assertEqual(VALID_TENSOR, UNSPECIFIED_DIMS_TENSOR));
}
