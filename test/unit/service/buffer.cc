/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "buffer.hh"

#include <gtest/gtest.h>
#include <sys/mman.h>

#include "testutils.hh"

using namespace std;
using namespace larod;

class BufferTest : public ::testing::Test {
protected:
    BufferTest() : fd(larod::test::createTmpFile(fdSz)) {}

    size_t fdSz = 4;
    FileDescriptor fd;
};

TEST_F(BufferTest, ConstructorValid) {
    ASSERT_NO_THROW(Buffer(std::move(fd), 0, 0, 0));
}

TEST_F(BufferTest, ConstructorNegativeFd) {
    ASSERT_THROW(Buffer(-1, 0, 0, 0), invalid_argument);
}

TEST_F(BufferTest, ConstructorNegativeOffset) {
    ASSERT_THROW(Buffer(std::move(fd), 0, -1, 0), invalid_argument);
}

TEST_F(BufferTest, ConstructorOffsetGreaterThanMaxSize) {
    ASSERT_THROW(Buffer(std::move(fd), 1, 2, 0), invalid_argument);
}

TEST_F(BufferTest, Read) {
    Buffer buffer(std::move(fd), 0, 0, 0);
    vector<uint8_t> buf(fdSz);
    ASSERT_NO_THROW(buffer.read(buf));
}

TEST_F(BufferTest, ReadWithOffset) {
    Buffer buffer(std::move(fd), 0, static_cast<int64_t>(fdSz / 2), 0);
    vector<uint8_t> buf(fdSz / 2);
    ASSERT_NO_THROW(buffer.read(buf));
}

TEST_F(BufferTest, ReadWithMaxSize) {
    Buffer buffer(std::move(fd), fdSz / 2, 0, 0);
    vector<uint8_t> buf(fdSz / 2);
    ASSERT_NO_THROW(buffer.read(buf));
}

TEST_F(BufferTest, ReadWithLargerBufferThanMaxSize) {
    Buffer buffer(std::move(fd), fdSz / 2, 0, 0);
    vector<uint8_t> buf(fdSz);
    ASSERT_THROW(buffer.read(buf), FileDescriptorError);
}

TEST_F(BufferTest, ReadWithLargerOffsetThanFdSize) {
    Buffer buffer(std::move(fd), 0, static_cast<int64_t>(fdSz), 0);
    vector<uint8_t> buf(fdSz);
    ASSERT_THROW(buffer.read(buf), FileDescriptorError);
}

TEST_F(BufferTest, Write) {
    Buffer buffer(std::move(fd), 0, 0, 0);
    vector<uint8_t> buf(fdSz);
    ASSERT_NO_THROW(buffer.write(buf));
}

TEST_F(BufferTest, WriteWithOffset) {
    Buffer buffer(std::move(fd), 0, static_cast<int64_t>(fdSz / 2), 0);
    vector<uint8_t> buf(fdSz / 2);
    ASSERT_NO_THROW(buffer.write(buf));
}

TEST_F(BufferTest, WriteWithMaxSize) {
    Buffer buffer(std::move(fd), fdSz / 2, 0, 0);
    vector<uint8_t> buf(fdSz / 2);
    ASSERT_NO_THROW(buffer.write(buf));
}

TEST_F(BufferTest, WriteWithLargerBufferThanMaxSize) {
    Buffer buffer(std::move(fd), fdSz / 2, 0, 0);
    vector<uint8_t> buf(fdSz);
    ASSERT_THROW(buffer.write(buf), FileDescriptorError);
}

TEST_F(BufferTest, GetMappingTwice) {
    Buffer buffer(std::move(fd), fdSz, 0, 0);
    uint8_t* mapPtr1 = buffer.getMapping(fdSz, PROT_READ | PROT_WRITE);
    uint8_t* mapPtr2 = buffer.getMapping(fdSz, PROT_READ | PROT_WRITE);
    ASSERT_EQ(mapPtr1, mapPtr2);
}

TEST_F(BufferTest, ReadAfterMapping) {
    Buffer buffer(std::move(fd), fdSz, 0, 0);

    uint8_t* mapPtr = buffer.getMapping(fdSz, PROT_READ);
    const uint8_t val = 123;
    mapPtr[0] = val;

    vector<uint8_t> buf(fdSz);
    ASSERT_NO_THROW(buffer.read(buf));
    ASSERT_EQ(buf[0], val);
}

TEST_F(BufferTest, MapAfterWriting) {
    Buffer buffer(std::move(fd), fdSz, 0, 0);

    const uint8_t val = 123;
    vector<uint8_t> buf(fdSz);
    buf[0] = val;
    ASSERT_NO_THROW(buffer.write(buf));

    uint8_t* mapPtr = buffer.getMapping(fdSz, PROT_READ);
    ASSERT_EQ(mapPtr[0], val);
}

TEST_F(BufferTest, MapSmallThenLarge) {
    Buffer buffer(std::move(fd), fdSz, 0, 0);
    ASSERT_NO_THROW(buffer.getMapping(fdSz / 2, PROT_READ | PROT_WRITE));
    ASSERT_THROW(buffer.getMapping(fdSz, PROT_READ | PROT_WRITE),
                 FileDescriptorError);
}

TEST_F(BufferTest, MapGreaterThanMaxSize) {
    Buffer buffer(std::move(fd), fdSz, 0, 0);
    ASSERT_THROW(buffer.getMapping(fdSz + 1, PROT_READ | PROT_WRITE),
                 FileDescriptorError);
}

TEST_F(BufferTest, MapWithOffsetGreaterThanMaxSize) {
    Buffer buffer(std::move(fd), fdSz, static_cast<int64_t>(fdSz) / 2, 0);
    ASSERT_THROW(buffer.getMapping(fdSz / 2 + 1, PROT_READ | PROT_WRITE),
                 FileDescriptorError);
}

TEST_F(BufferTest, WriteMapReadWithOffset) {
    Buffer buffer(std::move(fd), fdSz, static_cast<int64_t>(fdSz) / 2, 0);

    // Write to buffer.
    vector<uint8_t> buf(fdSz / 2);
    const uint8_t val1 = 123;
    buf[0] = val1;
    ASSERT_NO_THROW(buffer.write(buf));

    // Read from mapped pointer, verify output and write a new value.
    uint8_t* mapPtr = buffer.getMapping(fdSz / 2, PROT_READ | PROT_WRITE);
    ASSERT_EQ(mapPtr[0], val1);
    const uint8_t val2 = 42;
    mapPtr[0] = val2;

    // Read file and verify output.
    buf = vector<uint8_t>(fdSz / 2);
    ASSERT_NO_THROW(buffer.read(buf));
    ASSERT_EQ(buf[0], val2);
}
