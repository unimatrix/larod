#pragma once

class TensorAttributes : public ::testing::Test {
protected:
    TensorAttributes() : error(nullptr), tensors(nullptr) {
        tensors = larodCreateTensors(NUM_TENSORS, &error);
        if (error) {
            throw std::runtime_error("Could not create tensors (" +
                                     std::to_string(error->code) +
                                     "): " + error->msg);
        }

        for (size_t i = 0; i < NUM_TENSORS; i++) {
            // For this test, by default we assume read/write and map.
            if (!larodSetTensorFdProps(
                    tensors[i], LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP,
                    &error)) {
                throw std::runtime_error("Could not set props (" +
                                         std::to_string(error->code) +
                                         "): " + error->msg);
            }
        }
    }

    ~TensorAttributes() {
        larodClearError(&error);
        larodDestroyTensors(&tensors, NUM_TENSORS);
    }

    static const size_t NUM_TENSORS = 1;

    larodError* error;
    larodTensor** tensors;
};
