/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "imageutil.hh"

#include <algorithm>
#include <cstring>
#include <stdexcept>

using namespace std;

static uint8_t clampToUint8(int v);
static pair<float, int> compareNv12Buffers(size_t width, size_t height,
                                           size_t pitch,
                                           const uint8_t* bufferOne,
                                           const uint8_t* bufferTwo);
static pair<float, int> compareRgbBuffers(size_t width, size_t height,
                                          size_t pitch,
                                          const uint8_t* bufferOne,
                                          const uint8_t* bufferTwo,
                                          bool interleaved);

vector<uint8_t> generateYcbcrTestPattern(size_t width, size_t pitch,
                                         size_t height,
                                         unsigned int randomSeed) {
    size_t bufferSize = 3 * pitch * height / 2;
    vector<uint8_t> buffer(bufferSize, 0);
    uint8_t* yBuffer = buffer.data();
    uint8_t* chromaBuffer = buffer.data() + pitch * height;
    srand(randomSeed);

    // Loop over all 2x2 blocks.
    for (size_t yi = 0; yi < height / 2; yi++) {
        for (size_t xi = 0; xi < width / 2; xi++) {
            size_t cbIdx = yi * pitch + 2 * xi;
            size_t crIdx = yi * pitch + 2 * xi + 1;
            size_t yIdx00 = 2 * yi * pitch + 2 * xi;
            size_t yIdx01 = 2 * yi * pitch + 2 * xi + 1;
            size_t yIdx10 = (2 * yi + 1) * pitch + 2 * xi;
            size_t yIdx11 = (2 * yi + 1) * pitch + 2 * xi + 1;

            // Randomize
            yBuffer[yIdx00] = static_cast<uint8_t>(rand() % 256);
            yBuffer[yIdx01] = static_cast<uint8_t>(rand() % 256);
            yBuffer[yIdx10] = static_cast<uint8_t>(rand() % 256);
            yBuffer[yIdx11] = static_cast<uint8_t>(rand() % 256);
            chromaBuffer[cbIdx] = static_cast<uint8_t>(rand() % 256);
            chromaBuffer[crIdx] = static_cast<uint8_t>(rand() % 256);

            // Force some corner values.
            if (xi == 0) {
                yBuffer[yIdx00] = 0;
                yBuffer[yIdx01] = 255;
                yBuffer[yIdx10] = 16;
                yBuffer[yIdx11] = 128;

                switch (yi) {
                case 0:
                    chromaBuffer[cbIdx] = 0;
                    chromaBuffer[crIdx] = 0;
                    break;
                case 1:
                    chromaBuffer[cbIdx] = 255;
                    chromaBuffer[crIdx] = 0;
                    break;
                case 2:
                    chromaBuffer[cbIdx] = 0;
                    chromaBuffer[crIdx] = 255;
                    break;
                case 3:
                    chromaBuffer[cbIdx] = 255;
                    chromaBuffer[crIdx] = 255;
                    break;
                case 4:
                    chromaBuffer[cbIdx] = 128;
                    chromaBuffer[crIdx] = 0;
                    break;
                case 5:
                    chromaBuffer[cbIdx] = 0;
                    chromaBuffer[crIdx] = 128;
                    break;
                case 6:
                    chromaBuffer[cbIdx] = 128;
                    chromaBuffer[crIdx] = 128;
                    break;
                }
            }
        }
    }

    return buffer;
}

vector<uint8_t> convertYcbcrToRgb(const vector<uint8_t>& ycbcrBuffer,
                                  size_t width, size_t pitch, size_t height,
                                  bool interleaved) {
    vector<uint8_t> rgbBuffer(3 * pitch * height, 0);

    const uint8_t* yBuffer = ycbcrBuffer.data();
    const uint8_t* chromaBuffer = ycbcrBuffer.data() + pitch * height;

    for (size_t yi = 0; yi < height; yi++) {
        for (size_t xi = 0; xi < width; xi++) {
            int y = yBuffer[yi * pitch + xi];
            int cb = chromaBuffer[(yi / 2) * pitch + (xi & ~0x1UL)];
            int cr = chromaBuffer[(yi / 2) * pitch + (xi & ~0x1UL) + 1];

            // cb and cr are stored with a 128 shift.
            cb -= 128;
            cr -= 128;

            // Do calculations in 32 bit signed space.
            int r = (256 * y + 359 * cr + 128) >> 8;
            int g = (256 * y - 88 * cb - 183 * cr + 128) >> 8;
            int b = (256 * y + 454 * cb + 128) >> 8;

            size_t idx = yi * pitch + xi;
            if (interleaved) {
                rgbBuffer[idx * 3] = clampToUint8(r);
                rgbBuffer[idx * 3 + 1] = clampToUint8(g);
                rgbBuffer[idx * 3 + 2] = clampToUint8(b);
            } else { // Planar
                rgbBuffer[idx] = clampToUint8(r);
                rgbBuffer[idx + pitch * height] = clampToUint8(g);
                rgbBuffer[idx + 2 * pitch * height] = clampToUint8(b);
            }
        }
    }

    return rgbBuffer;
}

pair<float, int> compareBuffers(size_t width, size_t height, size_t pitch,
                                const uint8_t* bufferOne,
                                const uint8_t* bufferTwo, string_view format) {
    if (format == "nv12") {
        return compareNv12Buffers(width, height, pitch, bufferOne, bufferTwo);
    } else if (format == "rgb-planar") {
        return compareRgbBuffers(width, height, pitch, bufferOne, bufferTwo,
                                 false);
    } else if (format == "rgb-interleaved") {
        return compareRgbBuffers(width, height, pitch, bufferOne, bufferTwo,
                                 true);
    }

    throw runtime_error("Unknown image format");
}

pair<float, int> compareNv12Buffers(size_t width, size_t height, size_t pitch,
                                    const uint8_t* bufferOne,
                                    const uint8_t* bufferTwo) {
    unsigned int absDiffSumY = 0;
    unsigned int absDiffSumCbCr = 0;

    unsigned int maxAbsDiffY = 0;
    unsigned int maxAbsDiffCbCr = 0;

    for (size_t yi = 0; yi < height; yi++) {
        for (size_t xi = 0; xi < width; xi++) {
            size_t idx = yi * pitch + xi;
            unsigned int yDiff =
                static_cast<unsigned int>(abs(bufferOne[idx] - bufferTwo[idx]));

            absDiffSumY += yDiff;
            maxAbsDiffY = max(yDiff, maxAbsDiffY);
        }
    }
    for (size_t yi = 0; yi < height / 2; yi++) {
        for (size_t xi = 0; xi < width; xi++) {
            size_t idx = (height + yi) * pitch + xi;
            unsigned int cbCrDiff =
                static_cast<unsigned int>(abs(bufferOne[idx] - bufferTwo[idx]));

            absDiffSumCbCr += cbCrDiff;
            maxAbsDiffCbCr = max(cbCrDiff, maxAbsDiffCbCr);
        }
    }

    float nbrPixels = static_cast<float>(3 * width * height / 2);
    float meanAbsDiff =
        static_cast<float>(absDiffSumY + absDiffSumCbCr) / nbrPixels;
    unsigned int maxAbsDiff = max(maxAbsDiffY, maxAbsDiffCbCr);

    return pair<float, int>(meanAbsDiff, maxAbsDiff);
}

pair<float, int> compareRgbBuffers(size_t width, size_t height, size_t pitch,
                                   const uint8_t* bufferOne,
                                   const uint8_t* bufferTwo, bool interleaved) {
    unsigned int absDiffSumR = 0;
    unsigned int absDiffSumG = 0;
    unsigned int absDiffSumB = 0;

    unsigned int maxAbsDiffR = 0;
    unsigned int maxAbsDiffG = 0;
    unsigned int maxAbsDiffB = 0;

    size_t planePixels = interleaved ? 1 : pitch * height;
    for (size_t yi = 0; yi < height; yi++) {
        for (size_t xi = 0; xi < width; xi++) {
            size_t idx = interleaved ? yi * pitch + xi * 3 : yi * pitch + xi;
            unsigned int rDiff =
                static_cast<unsigned int>(abs(bufferOne[idx] - bufferTwo[idx]));
            unsigned int gDiff = static_cast<unsigned int>(abs(
                bufferOne[idx + planePixels] - bufferTwo[idx + planePixels]));
            unsigned int bDiff = static_cast<unsigned int>(
                abs(bufferOne[idx + 2 * planePixels] -
                    bufferTwo[idx + 2 * planePixels]));

            absDiffSumR += rDiff;
            absDiffSumG += gDiff;
            absDiffSumB += bDiff;

            maxAbsDiffR = max(rDiff, maxAbsDiffR);
            maxAbsDiffG = max(gDiff, maxAbsDiffG);
            maxAbsDiffB = max(bDiff, maxAbsDiffB);
        }
    }

    float nbrPixels = static_cast<float>(width * height);
    float meanAbsDiff =
        static_cast<float>(absDiffSumR + absDiffSumG + absDiffSumB) /
        (3 * nbrPixels);
    unsigned int maxAbsDiff = max(maxAbsDiffR, maxAbsDiffG);
    maxAbsDiff = max(maxAbsDiff, maxAbsDiffB);

    return pair<float, int>(meanAbsDiff, maxAbsDiff);
}

static uint8_t clampToUint8(int v) {
    return static_cast<uint8_t>(clamp<int>(v, 0, 255));
}

size_t calcBufferSize(string_view format, size_t height, size_t pitch) {
    if (format == "nv12") {
        return 3 * height * pitch / 2;
    } else if (format == "rgb-planar") {
        return height * pitch * 3;
    } else if (format == "rgb-interleaved") {
        return height * pitch;
    } else {
        throw runtime_error("Unknown image format");
    }
}

size_t castToSizeT(int64_t num, const string& name) {
    if (num < 0) {
        throw runtime_error(name + " is negative");
    }
    uint64_t unsignedNum = static_cast<uint64_t>(num);
    if (unsignedNum >= numeric_limits<size_t>::max()) {
        throw runtime_error(name + " is too large");
    }

    return static_cast<size_t>(num);
}

size_t calcPitch(size_t width, string_view format) {
    if (format == "nv12" || format == "rgb-planar") {
        return width;
    } else if (format == "rgb-interleaved") {
        return 3 * width;
    }

    throw runtime_error("Unknown format: " + string(format));
}
