/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <gtest/gtest.h>
#include <string>
#include <vector>

#include "imageutil.hh"
#include "larod.h"

struct ImageProcFuncTest : public ::testing::Test {
public:
    // Argparse attributes
    inline static larodChip chip;
    inline static bool debugPrints;
    inline static larodMap* modelParams;
    inline static larodMap* jobParams;
    inline static const char* inputFilePath = nullptr;
    inline static const char* verificationFilePath = nullptr;
    inline static bool verify;
    inline static unsigned int randomSeed;
    inline static float meanAbsDiffThreshold;
    inline static unsigned int maxAbsDiffThreshold;

    static void gtestDebugPrint(std::string msg);

    // Is run before every TEST_F.
    ImageProcFuncTest();

    // Is run after every TEST_F.
    ~ImageProcFuncTest();

protected:
    void setupLarod();
    void setupInputOutputMetaData();
    void setupTensors();
    void createJobRequestAndRun();
    void verifyOutput();
    std::vector<uint8_t> getVerificationBuffer();

    larodConnection* conn = nullptr;
    larodError* error = nullptr;
    larodModel* model = nullptr;
    larodJobRequest* jobReq = nullptr;
    larodTensor** inputTensors = nullptr;
    larodTensor** outputTensors = nullptr;

    size_t numInputs = 0;
    size_t inputWidth = 0;
    size_t inputHeight = 0;
    size_t inputPitch = 0;
    size_t inputBufferSize = 0;
    std::string inputFormat = "";

    size_t numOutputs = 0;
    size_t outputWidth = 0;
    size_t outputHeight = 0;
    size_t outputPitch = 0;
    size_t outputBufferSize = 0;
    std::string outputFormat = "";

    std::vector<uint8_t> patternBuffer;
    uint8_t* outputBuffer = nullptr;

    int inFd = -1;
    int outFd = -1;
};
