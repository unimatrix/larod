/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <gtest/gtest.h>

#include "larod.h"
#include "testmacros.h"

// Verify that establishing a connection to larod fails when the lib version
// numbers do not match the service version numbers. Note that this test links
// to a dummy version of larod where the numbers are set to invalid values.
TEST(larodFunctionalTest, Version) {
    larodError* error = nullptr;
    larodConnection* conn = nullptr;

    bool ret = larodConnect(&conn, &error);
    ASSERT_FALSE(ret);
    ASSERT_EQ(conn, nullptr);
    ASSERT_LAROD_ERROR(error);
    ASSERT_EQ(error->code, LAROD_ERROR_VERSION_MISMATCH);
    larodClearError(&error);
}
