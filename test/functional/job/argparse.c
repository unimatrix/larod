/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "argparse.h"

#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "larod.h"
#include "utils.h"

#define KEY_USAGE (127)

static int parseOpt(int key, char* arg, struct argp_state* state);

/**
 * @brief Parses a string as an unsigned long long.
 *
 * @param arg String to parse.
 * @param i Pointer to the number being the result of parsing.
 * @return Positive errno style return code (zero means success).
 */
static int parsePosInt(char* arg, unsigned long long* i);

/**
 * @brief Parses a string to JobTestBufType.
 *
 * @param arg String to parse.
 * @return A valid buffer type or BUF_TYPE_INVALID on error.
 */
static JobTestBufType parseBufferType(const char* arg);

const struct argp_option opts[] = {
    {"chip", 'c', "CHIP", 0,
     "Chooses chip CHIP to run tests on, where CHIP is the enum type "
     "larodChip from the library. If not specified, the default chip is the "
     "debug chip.",
     0},
    {"debug", 'd', NULL, 0, "Enable debug prints.", 0},
    {"model", 'g', "MODEL", 0,
     "Specifies from which path to load a model. Can be left blank if at least "
     "one model parameter is specified or if the selected chip is the debug "
     "chip",
     0},
    {"input", 'i', "INPUT", 0,
     "Add input file. This option can be repeated several times to add input "
     "files for each tensor of the network. If 0 files are provided there will "
     "be temp files used for inputs. If 1 or more input files are provided "
     "then the number of files must match the model number of input tensors "
     "exactly.",
     0},
    {"model-params", 'b', "KEY:VAL", 0,
     "Add additonal parameters when loading model (c.f. larodLoadModel()). "
     "KEY should be a string and VAL should either be a string, integer or an "
     "array of integers separated by \",\". For example "
     "\"image.input.format:rgb-interleaved\", \"image.input.row-pitch:224\" or "
     "\"image.input.size:224,224\".",
     0},
    {"job-params", 'e', "KEY:VAL", 0,
     "Add additional parameters when running a job (c.f. "
     "larodCreateJobRequest()). See option \"--model-params\" for a description"
     " of KEY and VAL.",
     0},
    {"verify", 'v', "REFERENCE", 0,
     "Add reference output file. This can be repeated multiple times to add "
     "reference files for each output tensor of the network. If reference "
     "files are provided then their number must match exactly the number of "
     "output tensors for the model.",
     0},
    {"input-buf-type", 'a', "INPUT_BUF_TYPE", 0,
     "Specify buffer type for input buffers. All input buffers will be "
     "handled according to this choice. Valid options are ext-disk (external "
     "disk-based buffer), ext-dma (externally allocated dmabuf), larod-disk "
     "(memfd allocated by larod), larod-dma (dmabuf allocated by larod). "
     "Default is ext-disk.",
     0},
    {"output-buf-type", 'o', "OUTPUT_BUF_TYPE", 0,
     "Specify buffer type for output buffers. All output buffers will be "
     "handled according to this choice. Valid options are ext-disk (external "
     "disk-based buffer), ext-dma (externally allocated dmabuf), larod-disk "
     "(memfd allocated by larod), larod-dma (dmabuf allocated by larod). "
     "Default is ext-disk.",
     0},
    {"track-inputs", 't', NULL, 0,
     "Set unique IDs on input tensors which makes larod cache the tensors' "
     "buffers and operate on that cached data. This flag must be set if "
     "larod-allocated buffers are used since buffers allocated from larod are "
     "always tracked.",
     0},
    {"track-outputs", 'u', NULL, 0,
     "Set unique IDs on output tensors which makes larod cache the tensors' "
     "buffers and operate on that cached data. This flag must be set if "
     "larod-allocated buffers are used since buffers allocated from larod are "
     "always tracked.",
     0},
    {"force-map-input", 'j', NULL, 0,
     "For external buffers: set LAROD_FD_PROP_MAP as only fd property of input "
     "buffers, effectively forcing the backend to mmap the inputs. In "
     "particular if used together with --dma-input then external dma buffers "
     "will be allocated and the LAROD_FD_PROP_DMABUF will be removed from the "
     "input tensors' fd props. For larod-allocated buffers: this flag is not "
     "allowed for larod dmabuf:s and for larod memfd the LAROD_FD_PROP_MAP "
     "will be set together with other flags in a best effort.",
     0},
    {"force-map-output", 'k', NULL, 0,
     "For external buffers: set LAROD_FD_PROP_MAP as only fd property of "
     "output buffers, effectively forcing the backend to mmap the outputs. In "
     "particular if used together with --dma-input then external dma buffers "
     "will be allocated and the LAROD_FD_PROP_DMABUF will be removed from the "
     "output tensors' fd props. For larod-allocated buffers: this flag is not "
     "allowed for larod dmabuf:s and for larod memfd the LAROD_FD_PROP_MAP "
     "will be set together with other flags in a best effort.",
     0},
    {"help", 'h', NULL, 0, "Print this help text and exit.", 0},
    {"usage", KEY_USAGE, NULL, 0, "Print short usage message and exit.", 0},
    {0}};
const struct argp argp = {opts,
                          parseOpt,
                          NULL,
                          "Executes the Larod job functional test through "
                          "Google test framework. Undefined tensor "
                          "data type defaults to 8 bit size.",
                          NULL,
                          NULL,
                          NULL};

int parseArgs(int argc, char** argv, args_t* args) {
    int ret = argp_parse(&argp, argc, argv, ARGP_NO_HELP, NULL, args);
    if (ret) {
        return ret;
    }

    // Make sure a model or parameters are provided if not using debug chip.
    if (!args->model && !args->hasModelParams &&
        args->chip != LAROD_CHIP_DEBUG) {
        fprintf(stderr,
                "%s: Path to model file or model parameters must be specified "
                "when using other backend than DebugChip (use option "
                "\"--model\")\n",
                argv[0]);

        return EINVAL;
    }

    // Sanity checK: --force-map-input cannot be used  with larod-allocated
    // inputs.
    if (args->forceMapInput && (args->inputBufType == BUF_TYPE_LAROD_DMABUF ||
                                args->inputBufType == BUF_TYPE_LAROD_DISKFD)) {
        fprintf(stderr,
                "%s: --force-map-input cannot be used with larod-allocated "
                "inputs since larod API does not provide any way to allocate "
                "buffers that have only LAROD_FD_PROP_MAP!\n",
                argv[0]);
        return EINVAL;
    }
    // Verify that trackInputs is set when using larod-allocated buffers.
    if (!args->trackInputs) {
        if ((args->inputBufType == BUF_TYPE_LAROD_DISKFD) ||
            (args->inputBufType == BUF_TYPE_LAROD_DMABUF)) {
            fprintf(stderr,
                    "%s: --track-inputs must be set when using larod-allocated "
                    "input buffers!\n",
                    argv[0]);
            return EINVAL;
        }
    }

    // Sanity checK: --force-map-output cannot be used  with larod-allocated
    // outputs.
    if (args->forceMapOutput &&
        (args->outputBufType == BUF_TYPE_LAROD_DMABUF ||
         args->outputBufType == BUF_TYPE_LAROD_DISKFD)) {
        fprintf(stderr,
                "%s: --force-map-output cannot be used with larod-allocated "
                "outputs since larod API does not provide any way to allocate "
                "buffers that have only LAROD_FD_PROP_MAP!\n",
                argv[0]);
        return EINVAL;
    }
    // Verify that trackOutputs is set when using larod-allocated buffers.
    if (!args->trackOutputs) {
        if ((args->outputBufType == BUF_TYPE_LAROD_DISKFD) ||
            (args->outputBufType == BUF_TYPE_LAROD_DMABUF)) {
            fprintf(stderr,
                    "%s: --track-outputs must be set when using "
                    "larod-allocated output buffers!\n",
                    argv[0]);
            return EINVAL;
        }
    }

    return 0;
}

int parseOpt(int key, char* arg, struct argp_state* state) {
    args_t* args = state->input;
    switch (key) {
    case 'a':
        args->inputBufType = parseBufferType(arg);
        if (args->inputBufType == BUF_TYPE_INVALID) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, EINVAL,
                         "unknown input-buf-type argument");
        }
        break;
    case 'c': {
        unsigned long long chip;
        int ret = parsePosInt(arg, &chip);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret, "invalid chip type");
        }
        // Make sure we don't overflow when casting to enum below.
        if (chip > INT_MAX) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ERANGE,
                         "chip type value too large");
        }
        args->chip = (larodChip) chip;
        break;
    }
    case 'b': {
        int ret = addParam(args->modelParams, arg);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret,
                         "could not add \"%s\" to model parameters", arg);
        }
        args->hasModelParams = true;
        break;
    }
    case 'e': {
        int ret = addParam(args->jobParams, arg);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret,
                         "could not add \"%s\" to job parameters", arg);
        }
        break;
    }
    case 'd':
        args->debugPrints = true;
        break;
    case 'g':
        args->model = arg;
        break;
    case 'h':
        argp_state_help(state, stdout, ARGP_HELP_STD_HELP);
        break;
    case 'i':
        if (args->numInputFiles < MAX_NUM_FILES) {
            args->inputFiles[args->numInputFiles] = arg;
            args->numInputFiles++;
        } else {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ERANGE,
                         "Too many input files provided, max is %d",
                         MAX_NUM_FILES);
        }
        break;
    case 'o':
        args->outputBufType = parseBufferType(arg);
        if (args->outputBufType == BUF_TYPE_INVALID) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, EINVAL,
                         "unknown output-buf-type argument");
        }
        break;
    case 'v':
        if (args->numVerificationFiles < MAX_NUM_FILES) {
            args->verificationFiles[args->numVerificationFiles] = arg;
            args->numVerificationFiles++;
        } else {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ERANGE,
                         "Too many verification files provided, max is %d",
                         MAX_NUM_FILES);
        }
        break;
    case 't':
        args->trackInputs = true;
        break;
    case 'u':
        args->trackOutputs = true;
        break;
    // TODO: Remove 'z' when we no longer need backward compat here (when
    // nn-test is updated).
    case 'z':
        args->inputBufType = BUF_TYPE_EXT_DMABUF;
        break;
    // TODO: Remove 'w' when we no longer need backward compat here (when
    // nn-test is updated).
    case 'w':
        args->outputBufType = BUF_TYPE_EXT_DMABUF;
        break;
    case 'j':
        args->forceMapInput = true;
        break;
    case 'k':
        args->forceMapOutput = true;
        break;
    case KEY_USAGE:
        argp_state_help(state, stdout, ARGP_HELP_USAGE | ARGP_HELP_EXIT_OK);
        break;
    case ARGP_KEY_INIT:
        args->model = NULL;
        args->numInputFiles = 0;
        memset(args->inputFiles, 0, sizeof(args->inputFiles));
        args->numVerificationFiles = 0;
        memset(args->verificationFiles, 0, sizeof(args->verificationFiles));
        args->chip = LAROD_CHIP_DEBUG;
        args->debugPrints = false;
        args->modelParams = NULL;
        args->jobParams = NULL;
        args->hasModelParams = false;
        args->inputBufType = BUF_TYPE_EXT_DISKFD;
        args->outputBufType = BUF_TYPE_EXT_DISKFD;
        args->trackInputs = false;
        args->trackOutputs = false;
        args->forceMapInput = false;
        args->forceMapOutput = false;

        args->modelParams = larodCreateMap(NULL);
        if (!args->modelParams) {
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "could not allocate model parameters");
        }

        args->jobParams = larodCreateMap(NULL);
        if (!args->jobParams) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "could not allocate job parameters");
        }
        break;
    case ARGP_KEY_END:
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

static int parsePosInt(char* arg, unsigned long long* i) {
    char* endPtr;
    *i = strtoull(arg, &endPtr, 0);
    if (*endPtr != '\0') {
        return EINVAL;
    } else if (arg[0] == '-' || *i == 0) {
        return EINVAL;
    } else if (*i == ULLONG_MAX) {
        return ERANGE;
    }

    return 0;
}

JobTestBufType parseBufferType(const char* arg) {
    if (!strncmp(arg, "ext-disk", 8)) {
        return BUF_TYPE_EXT_DISKFD;
    }
    if (!strncmp(arg, "ext-dma", 7)) {
        return BUF_TYPE_EXT_DMABUF;
    }
    if (!strncmp(arg, "larod-disk", 10)) {
        return BUF_TYPE_LAROD_DISKFD;
    }
    if (!strncmp(arg, "larod-dma", 9)) {
        return BUF_TYPE_LAROD_DMABUF;
    }
    return BUF_TYPE_INVALID;
}

void destroyArgs(args_t* args) {
    larodDestroyMap(&args->modelParams);
    larodDestroyMap(&args->jobParams);
}
