/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>

#include "larod.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_NUM_FILES 30

typedef enum {
    BUF_TYPE_INVALID = 0,
    BUF_TYPE_EXT_DISKFD,
    BUF_TYPE_EXT_DMABUF,
    BUF_TYPE_LAROD_DISKFD,
    BUF_TYPE_LAROD_DMABUF
} JobTestBufType;

typedef struct args_t {
    char* model;
    size_t numInputFiles;
    char* inputFiles[MAX_NUM_FILES];
    size_t numVerificationFiles;
    char* verificationFiles[MAX_NUM_FILES];
    larodMap* modelParams;
    larodMap* jobParams;
    larodChip chip;
    bool debugPrints;
    bool hasModelParams;
    JobTestBufType inputBufType;
    JobTestBufType outputBufType;
    bool trackInputs;
    bool trackOutputs;
    bool forceMapInput;
    bool forceMapOutput;
} args_t;

int parseArgs(int argc, char** argv, args_t* args);

void destroyArgs(args_t* args);

#ifdef __cplusplus
}
#endif
