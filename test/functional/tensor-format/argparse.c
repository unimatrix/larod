/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "argparse.h"

#include <argp.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "larod.h"
#include "utils.h"

#define KEY_USAGE (127)

/**
 * @brief Parses a string as an unsigned long long
 *
 * @param arg String to parse.
 * @param i Pointer to the number being the result of parsing.
 * @return Positive errno style return code (zero means success).
 */
static int parsePosInt(char* arg, unsigned long long* i);

static int parseOpt(int key, char* arg, struct argp_state* state);

const struct argp_option opts[] = {
    {"chip", 'c', "CHIP", 0,
     "Chooses chip CHIP to run tests on, where CHIP is the enum type "
     "larodChip from the library. If not specified, the default chip is the "
     "debug chip.",
     0},
    {"model", 'g', "MODEL", 0,
     "Specifies from which path to load a model. This option is optional "
     "since some backends do not use a dedicated model file to run jobs with.",
     0},
    {"model-params", 'b', "KEY:VAL", 0,
     "Add additonal parameters when loading model (c.f. larodLoadModel()). "
     "KEY should be a string and VAL should either be a string, integer or an "
     "array of integers separated by \",\". For example "
     "\"image.input.format:rgb-interleaved\", \"image.input.row-pitch:224\" or "
     "\"image.input.size:224,224\".",
     0},
    {"job-params", 'e', "KEY:VAL", 0,
     "Add additional parameters when running a job (c.f. "
     "larodCreateJobRequest()). See option \"--model-params\" for a description"
     " of KEY and VAL.",
     0},
    {"input", 'i', "INPUT_FORMAT", 0,
     "Set expected format of an input tensor for the model to be loaded. "
     "INPUT_FORMAT is a string specifying the format of the tensor. The "
     "string is expected to be of the form DATA_TYPE:LAYOUT:DIMS:PITCHES, "
     "where DATA_TYPE and LAYOUT are (case insensitive) strings "
     "specifying the data type and layout of the tensor. DIMS and PITCHES "
     "are expected to be comma-separated lists of integers representing "
     "the values of the dimensions/pitches of the tensor. See below for "
     "an example of the full format string.",
     0},
    {"output", 'o', "OUTPUT_FORMAT", 0,
     "Set expected format of an output tensor for the model to be loaded. "
     "OUTPUT_FORMAT is a string specifying the format of the tensor. The "
     "string is expected to be of the form DATA_TYPE:LAYOUT:DIMS:PITCHES, "
     "where DATA_TYPE and LAYOUT are (case insensitive) strings specifying "
     "the data type and layout of the tensor. DIMS and PITCHES are expected "
     "to be comma-separated lists of integers representing the values of "
     "the dimensions/pitches of the tensor. See below for an example of "
     "the full format string.",
     0},
    {"help", 'h', NULL, 0, "Print this help text and exit.", 0},
    {"usage", KEY_USAGE, NULL, 0, "Print short usage message and exit.", 0},
    {0}};
const struct argp argp = {
    opts,
    parseOpt,
    NULL,
    "Test verifying that checks of tensor meta data, such as data type, "
    "layout, dims and pitches, work as expected.\vThe tensor format string for "
    "a model that takes a tensor containing unsigned 8 bit integer data in "
    "format NHWC, where the dimensions are N=1, H=224, W=224 and C=3 (and no "
    "alignment) should be given as: uint8:nhwc:1,224,224,3:150528,150528,672,3",
    NULL,
    NULL,
    NULL};

bool parseArgs(int argc, char** argv, args_t* args) {
    if (argp_parse(&argp, argc, argv, ARGP_NO_HELP, NULL, args)) {
        return false;
    }

    // Check option conditions.

    // Model file must be supplied.
    if (!args->modelFile && !args->hasModelParams) {
        fprintf(stderr,
                "%s: Model file or model parameters must be specified\n",
                argv[0]);

        return false;
    }

    // Chip must be supplied.
    if (!args->chip) {
        fprintf(stderr, "%s: Chip not specified\n", argv[0]);

        return false;
    }

    // Input format(s) must be supplied.
    if (!*args->inputFormats) {
        fprintf(stderr, "%s: No input tensor specified\n", argv[0]);

        return false;
    }

    // Output format(s) must be supplied.
    if (!*args->outputFormats) {
        fprintf(stderr, "%s: No output tensor specified\n", argv[0]);

        return false;
    }

    return true;
}

int parseOpt(int key, char* arg, struct argp_state* state) {
    args_t* args = state->input;
    switch (key) {
    case 'c': {
        unsigned long long chip;
        int ret = parsePosInt(arg, &chip);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret, "invalid chip type");
        }
        // Make sure we don't overflow when casting to enum below.
        if (chip > INT_MAX) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ERANGE,
                         "chip type value too large");
        }
        args->chip = (larodChip) chip;
        break;
    }
    case 'g':
        args->modelFile = arg;
        break;
    case 'b': {
        int ret = addParam(args->modelParams, arg);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret,
                         "could not add \"%s\" to model parameters", arg);
        }
        args->hasModelParams = true;
        break;
    }
    case 'e': {
        int ret = addParam(args->jobParams, arg);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret,
                         "could not add \"%s\" to job parameters", arg);
        }
        break;
    }
    case 'i':
        if (!strlen(arg)) {
            destroyArgs(args);
            argp_error(state, "input format cannot be empty.");
        }

        if (args->numInputs < MAX_NUM_FORMATS) {
            args->inputFormats[args->numInputs++] = arg;
        } else {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, EINVAL,
                         "Too many input formats provided, max is %d",
                         MAX_NUM_FORMATS);
        }
        break;
    case 'o':
        if (!strlen(arg)) {
            destroyArgs(args);
            argp_error(state, "output format cannot be empty.");
        }

        if (args->numOutputs < MAX_NUM_FORMATS) {
            args->outputFormats[args->numOutputs++] = arg;
        } else {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, EINVAL,
                         "Too many output formats provided, max is %d",
                         MAX_NUM_FORMATS);
        }
        break;
    case 'h':
        argp_state_help(state, stdout, ARGP_HELP_STD_HELP);
        break;
    case KEY_USAGE:
        argp_state_help(state, stdout, ARGP_HELP_USAGE | ARGP_HELP_EXIT_OK);
        break;
    case ARGP_KEY_INIT:
        args->modelFile = NULL;

        args->modelParams = larodCreateMap(NULL);
        if (!args->modelParams) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "could not allocate model parameters");
        }

        args->jobParams = larodCreateMap(NULL);
        if (!args->jobParams) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "could not allocate job parameters");
        }

        args->chip = LAROD_CHIP_DEBUG;
        args->debug = false;
        memset(args->inputFormats, 0, sizeof(args->inputFormats));
        args->numInputs = 0;
        memset(args->outputFormats, 0, sizeof(args->outputFormats));
        args->numOutputs = 0;
        break;
    case ARGP_KEY_END:
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

static int parsePosInt(char* arg, unsigned long long* i) {
    char* endPtr;
    *i = strtoull(arg, &endPtr, 0);
    if (*endPtr != '\0') {
        return EINVAL;
    } else if (arg[0] == '-' || *i == 0) {
        return EINVAL;
    } else if (*i == ULLONG_MAX) {
        return ERANGE;
    }

    return 0;
}

void destroyArgs(args_t* args) {
    assert(args);

    larodDestroyMap(&args->jobParams);
    larodDestroyMap(&args->modelParams);
}
