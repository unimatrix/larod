/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <cstdlib>
#include <gtest/gtest.h>

#include "argparse.h"
#include "larod.h"
#include "tensorformattest.hh"
#include "testutils.hh"

using namespace std;
using namespace larod::test;

size_t TensorFormatTest::numInputs;
size_t TensorFormatTest::numOutputs;

/**
 * @brief Test that tensors created by model meet expectations.
 *
 * In this test the meta data fields of input and output tensors created by @c
 * larodCreateModelInputs() and @c larodCreateModelOutputs() are compared to
 * specifications from command line.
 */
TEST_F(TensorFormatTest, RetrieveInfo) {
    size_t numInputs = 0;
    larodError* error = nullptr;
    larodTensor** inTensors =
        larodCreateModelInputs(modelPtr, &numInputs, &error);

    ASSERT_NE(inTensors, nullptr);

    for (size_t i = 0; i < numInputs; ++i) {
        ASSERT_NE(inTensors[i], nullptr);

        vector<TensorTuple> tup = parseTensors(inputFormats);
        ASSERT_TRUE(compareTensor(tup[i], inTensors[i]));
    }
    ASSERT_EQ(error, nullptr);

    size_t numOutputs = 0;
    larodTensor** outTensors =
        larodCreateModelOutputs(modelPtr, &numOutputs, &error);

    ASSERT_NE(outTensors, nullptr);

    for (size_t i = 0; i < numOutputs; ++i) {
        ASSERT_NE(outTensors[i], nullptr);

        vector<TensorTuple> tup = parseTensors(outputFormats);
        ASSERT_TRUE(compareTensor(tup[i], outTensors[i]));
    }
    ASSERT_EQ(error, nullptr);

    larodDestroyTensors(&inTensors, numInputs);
    larodDestroyTensors(&outTensors, numOutputs);
}

/**
 * @brief Test that running jobs fails when expected.
 *
 * This test case verifies that running a job fails when using input or output
 * tensors that do not match what the model expects.
 *
 * In @c tensorFormatHelper() a job is requested of the larod service. A @c
 * larodJobRequest is created and supplied with @c larodTensor objects based on
 * specifications from command line.
 *
 * Before creating the @c larodJobRequest the tensors are modified in as
 * specified by the different lambda functions passed to @c
 * tensorFormatHelper(). In this test case the lambda functions are designed to
 * change one or more fields of a tensor such that the tensor is valid in its
 * own right, but does not match what is specified by the model.
 *
 * The second parameter of @c tensorFormatHelper() represents what @c
 * larodRunJob() is expected to return. In this particular test case the
 * jobs are expected to fail, and thus 'false' is given for each test.
 */
TEST_F(TensorFormatTest, WrongFormat) {
    const bool EXPECT_FAILURE = false;

    vector<size_t> tuplesOrder = {INPUT_TUPLES, OUTPUT_TUPLES};
    vector<size_t> numTensors = {numInputs, numOutputs};
    for (size_t i : tuplesOrder) {
        for (size_t j = 0; j < numTensors[i]; ++j) {
            // Wrong data type.
            ASSERT_TRUE(tensorFormatHelper(
                [i, j](vector<TensorTuplesVector>& tensorFormats, bool&) {
                    vector<TensorTuple>& tuples = tensorFormats[i];
                    TensorTuple& tup = tuples[j];
                    larodTensorDataType& type = get<0>(tup);
                    type = (type == LAROD_TENSOR_DATA_TYPE_UINT8) ?
                               LAROD_TENSOR_DATA_TYPE_INT8 :
                               LAROD_TENSOR_DATA_TYPE_UINT8;
                },
                EXPECT_FAILURE));

            // Wrong layout.
            ASSERT_TRUE(tensorFormatHelper(
                [i, j](vector<TensorTuplesVector>& tensorFormats, bool& skip) {
                    vector<TensorTuple>& tuples = tensorFormats[i];
                    TensorTuple& tup = tuples[j];
                    larodTensorLayout& layout = get<1>(tup);

                    // Only NCHW or NHWC may be changed currently.
                    if (layout != LAROD_TENSOR_LAYOUT_NCHW ||
                        layout != LAROD_TENSOR_LAYOUT_NHWC) {
                        gtestPrint(
                            "Skipping input layout test, not NHWC or NCHW");
                        skip = true;
                    } else {
                        layout = (layout == LAROD_TENSOR_LAYOUT_NHWC) ?
                                     LAROD_TENSOR_LAYOUT_NCHW :
                                     LAROD_TENSOR_LAYOUT_NHWC;
                    }
                },
                EXPECT_FAILURE));

            // Wrong number of dims.
            ASSERT_TRUE(tensorFormatHelper(
                [i, j](vector<TensorTuplesVector>& tensorFormats, bool& skip) {
                    vector<TensorTuple>& tuples = tensorFormats[i];
                    TensorTuple& tup = tuples[j];
                    larodTensorDims& dims = get<2>(tup);
                    larodTensorLayout& layout = get<1>(tup);

                    // larod does not allow changing dims if layout is NCHW or
                    // NHWC.
                    if (layout == LAROD_TENSOR_LAYOUT_NCHW ||
                        layout == LAROD_TENSOR_LAYOUT_NHWC) {
                        gtestPrint(
                            "Skipping dims test, not allowed for NHWC or NCHW");
                        skip = true;
                    } else {
                        if (dims.len < LAROD_TENSOR_MAX_LEN) {
                            dims.dims[dims.len] = 42;
                            ++dims.len;
                        } else {
                            --dims.len;
                        }
                    }
                },
                EXPECT_FAILURE));

            // Wrong dim value.
            ASSERT_TRUE(tensorFormatHelper(
                [i, j](vector<TensorTuplesVector>& tensorFormats, bool&) {
                    vector<TensorTuple>& tuples = tensorFormats[i];
                    TensorTuple& tup = tuples[j];
                    larodTensorDims& dims = get<2>(tup);
                    ++dims.dims[0];
                },
                EXPECT_FAILURE));
        }
    }
}

/**
 * @brief Test that running jobs with unspecified parameters works.
 *
 * This test case verifies that running a job does not fail when running
 * jobs with tensors where one or more meta data field is marked as
 * unspecified works as intended.
 *
 * In @c tensorFormatHelper() a job is requested of the larod service. A
 * @c larodJobRequest is created and supplied with @c larodTensor objects
 * based on specifications from command line.
 *
 * Before creating the @c larodJobRequest the tensors are modified as
 * specified by the different lambda functions passed to @c
 * tensorFormatHelper(). In this test case the lambda functions are designed to
 * change one or more field of the tensors from a value specified at command
 * line into 'unspecified'.
 *
 * The second parameter of @c tensorFormatHelper() represents what @c
 * larodRunJob() is expected to return. In this particular test case the
 * jobs are expected to succeed, and thus 'true' is given for each test.
 */
TEST_F(TensorFormatTest, UnspecifiedFormat) {
    vector<size_t> tuplesOrder = {INPUT_TUPLES, OUTPUT_TUPLES};
    vector<size_t> numTensors = {numInputs, numOutputs};
    const bool EXPECT_SUCCESS = true;
    for (auto i : tuplesOrder) {
        for (size_t j = 0; j < numTensors[i]; ++j) {
            // Unspecified data type.
            ASSERT_TRUE(tensorFormatHelper(
                [i, j](vector<TensorTuplesVector>& tensorFormats, bool&) {
                    vector<TensorTuple>& tuples = tensorFormats[i];
                    TensorTuple& tup = tuples[j];
                    get<0>(tup) = LAROD_TENSOR_DATA_TYPE_UNSPECIFIED;
                },
                EXPECT_SUCCESS));

            // Unspecified layout.
            ASSERT_TRUE(tensorFormatHelper(
                [i, j](vector<TensorTuplesVector>& tensorFormats, bool&) {
                    vector<TensorTuple>& tuples = tensorFormats[i];
                    TensorTuple& tup = tuples[j];
                    get<1>(tup) = LAROD_TENSOR_LAYOUT_UNSPECIFIED;
                },
                EXPECT_SUCCESS));

            // Unspecified dims.
            ASSERT_TRUE(tensorFormatHelper(
                [i, j](vector<TensorTuplesVector>& tensorFormats, bool&) {
                    vector<TensorTuple>& tuples = tensorFormats[i];
                    TensorTuple& tup = tuples[j];
                    get<2>(tup).len = 0;
                },
                EXPECT_SUCCESS));

            // Unspecified pitches.
            ASSERT_TRUE(tensorFormatHelper(
                [i, j](vector<TensorTuplesVector>& tensorFormats, bool&) {
                    vector<TensorTuple>& tuples = tensorFormats[i];
                    TensorTuple& tup = tuples[j];
                    get<3>(tup).len = 0;
                },
                EXPECT_SUCCESS));

            // All values unspecified.
            ASSERT_TRUE(tensorFormatHelper(
                [i, j](vector<TensorTuplesVector>& tensorFormats, bool&) {
                    vector<TensorTuple>& tuples = tensorFormats[i];
                    TensorTuple& tup = tuples[j];
                    get<0>(tup) = LAROD_TENSOR_DATA_TYPE_UNSPECIFIED;
                    get<1>(tup) = LAROD_TENSOR_LAYOUT_UNSPECIFIED;
                    get<2>(tup).len = 0;
                    get<3>(tup).len = 0;
                },
                EXPECT_SUCCESS));
        }
    }

    // All values for all input and output unspecified.
    ASSERT_TRUE(tensorFormatHelper(
        [tuplesOrder, numTensors](vector<TensorTuplesVector>& tensorFormats,
                                  bool&) {
            for (auto i : tuplesOrder) {
                for (size_t j = 0; j < numTensors[i]; ++j) {
                    vector<TensorTuple>& tuples = tensorFormats[i];
                    TensorTuple& tup = tuples[j];

                    get<0>(tup) = LAROD_TENSOR_DATA_TYPE_UNSPECIFIED;
                    get<1>(tup) = LAROD_TENSOR_LAYOUT_UNSPECIFIED;
                    get<2>(tup).len = 0;
                    get<3>(tup).len = 0;
                }
            }
        },
        EXPECT_SUCCESS));
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);

    args_t args;

    if (parseArgs(argc, argv, &args) == 0) {
        destroyArgs(&args);
        exit(EXIT_FAILURE);
    }
    auto argsHandler = unique_ptr<args_t, function<void(args_t*)>>{
        &args, [](args_t* a) { destroyArgs(a); }};

    if (args.modelFile) {
        TensorFormatTest::modelFile = args.modelFile;
    }
    TensorFormatTest::chip = args.chip;
    TensorFormatTest::modelParams = args.modelParams;
    TensorFormatTest::jobParams = args.jobParams;
    TensorFormatTest::inputFormats =
        vector<string>(args.inputFormats, args.inputFormats + args.numInputs);
    TensorFormatTest::numInputs = args.numInputs;
    TensorFormatTest::outputFormats = vector<string>(
        args.outputFormats, args.outputFormats + args.numOutputs);
    TensorFormatTest::numOutputs = args.numOutputs;

    return RUN_ALL_TESTS();
}
