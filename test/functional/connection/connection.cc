/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <gtest/gtest.h>

#include "larod.h"

/**
 * @brief Helper macro to verify failing API call.
 *
 * Helper macro to verify that a Larod function returns valid errors upon
 * failure: return value should be 'false', a non-null larodError pointer is
 * created and correct error code should be set in the larodError instance.
 *
 * @param retVal Return value from API call, expected to be 'false'.
 * @param errStructPtr The larodError* that was used in Larod call.
 * @param errCode Expected error code, matched towards errStructPtr->code.
 */
#define ASSERT_LAROD_ERROR(retVal, errStructPtr, errCode)                      \
    ASSERT_FALSE(retVal);                                                      \
    ASSERT_NE(errStructPtr, nullptr);                                          \
    ASSERT_EQ(errStructPtr->code, errCode);                                    \
    larodClearError(&errStructPtr);

// Verify that we can make and close a connection
TEST(larodFunctionalTest, Connection) {
    larodError* error = nullptr;
    larodConnection* conn = nullptr;

    // Make sure we can make a connection
    bool ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    // Make sure that larod knows about our connection
    uint64_t nbrOfSessions = 0;
    ret = larodGetNumSessions(conn, &nbrOfSessions, &error);
    ASSERT_TRUE(ret);
    ASSERT_GT(nbrOfSessions, 0);
    ASSERT_EQ(error, nullptr);

    // Make sure we can close our connection
    ret = larodDisconnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn, nullptr);
    ASSERT_EQ(error, nullptr);
}

// Check input parameter validation of larodConnect.
TEST(larodFunctionalTest, ConnectInvalidParameter) {
    larodError* error = nullptr;

    bool ret = larodConnect(nullptr, &error);
    ASSERT_LAROD_ERROR(ret, error, EINVAL);
    ASSERT_EQ(error, nullptr);
}

// Check input parameter validation of larodDisconnect.
TEST(larodFunctionalTest, DisconnectInvalidParameter) {
    larodError* error = nullptr;

    bool ret = larodDisconnect(nullptr, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);
}

// Check input parameter validation of GetNbrOfSessions
TEST(larodFunctionalTest, GetNumSessionsInvalidParameter) {
    larodError* error = nullptr;
    larodConnection* conn = nullptr;
    uint64_t nbrOfSessions = 0;

    bool ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    ret = larodGetNumSessions(nullptr, &nbrOfSessions, &error);
    ASSERT_LAROD_ERROR(ret, error, EINVAL);
    ASSERT_EQ(error, nullptr);

    ret = larodGetNumSessions(conn, nullptr, &error);
    ASSERT_LAROD_ERROR(ret, error, EINVAL);
    ASSERT_EQ(error, nullptr);

    ret = larodDisconnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn, nullptr);
    ASSERT_EQ(error, nullptr);
}
