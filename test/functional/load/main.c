/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "load.h"

int main(int argc, char** argv) {
    bool ret;
    args_t args;

    ret = parseArgs(argc, argv, &args);
    if (!ret) {
        goto end;
    }

    ret = doLoad(&args);
    if (!ret) {
        goto end;
    }

end:
    destroyArgs(&args);

    return ret == false ? EXIT_FAILURE : EXIT_SUCCESS;
}
