/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "jobreqfunctest.hh"

#include <fcntl.h>
#include <gtest/gtest.h>
#include <stdexcept>

#include "testutils.hh"

using namespace std;
using namespace larod::test;

// Parameters from argparse.
string JobReqFuncTest::modelFile;
bool JobReqFuncTest::debugPrints = false;
larodChip JobReqFuncTest::chip = LAROD_CHIP_INVALID;
larodMap* JobReqFuncTest::modelParams = nullptr;
larodMap* JobReqFuncTest::jobParams = nullptr;

void JobReqFuncTest::SetUp() {
    larodError* error = nullptr;

    bool ret = larodConnect(&conn, &error);
    if (!ret || !conn) {
        throw runtime_error("Failed to open larod connection: " +
                            getErrMsgAndClear(&error));
    }

    int modelFd = -1;
    if (modelFile.size()) {
        modelFd = open(modelFile.c_str(), O_RDONLY);
        if (modelFd < 0) {
            throw runtime_error("Failed to open model file: " +
                                getErrMsgAndClear(&error));
        }
    }
    model = larodLoadModel(conn, modelFd, chip, LAROD_ACCESS_PRIVATE,
                           "larod-model", modelParams, &error);
    if (modelFd >= 0) {
        close(modelFd);
    }
    if (!model) {
        throw runtime_error("Failed loading model: " +
                            getErrMsgAndClear(&error));
    }

    numInputs = 0;
    inputTensors =
        larodAllocModelInputs(conn, model, 0, &numInputs, nullptr, &error);
    if (!inputTensors) {
        throw runtime_error("Failed creating model inputs: " +
                            getErrMsgAndClear(&error));
    }

    numOutputs = 0;
    outputTensors =
        larodAllocModelOutputs(conn, model, 0, &numOutputs, nullptr, &error);
    if (!outputTensors) {
        throw runtime_error("Failed creating model outputs: " +
                            getErrMsgAndClear(&error));
    }
}

void JobReqFuncTest::TearDown() {
    larodDestroyTensors(&inputTensors, numInputs);
    larodDestroyTensors(&outputTensors, numOutputs);

    if (!larodDeleteModel(conn, model, nullptr)) {
        throw runtime_error("Failed deleting model");
    }

    larodDestroyModel(&model);

    if (!larodDisconnect(&conn, nullptr)) {
        throw runtime_error("Failed closing larod connection");
    }

    // Call base GoogleTest Test::TearDown() for framework cleanup.
    Test::TearDown();
}
