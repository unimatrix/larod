/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once
#include <gtest/gtest.h>

#include "larod.h"

class ApiCompatVersion1 : public ::testing::Test {
public:
    void TearDown() override;

protected:
    larodError* error = nullptr;
    larodConnection* conn = nullptr;
    int modelFd = -1;
    larodModel* model = nullptr;
    larodTensor** inputTensors = nullptr;
    size_t numInputs = 0;
    larodTensor** outputTensors = nullptr;
    size_t numOutputs = 0;
    larodInferenceRequest* infReq = nullptr;
};
