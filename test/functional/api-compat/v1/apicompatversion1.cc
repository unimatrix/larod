/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "apicompatversion1.hh"

#include <stdexcept>

using namespace std;

void ApiCompatVersion1::TearDown() {
    larodDestroyTensors(&inputTensors, numInputs);
    larodDestroyTensors(&outputTensors, numOutputs);
    larodDestroyInferenceRequest(&infReq);
    larodClearError(&error);

    if (model && !larodDeleteModel(conn, model, nullptr)) {
        throw runtime_error("Failed deleting model!");
    }
    larodDestroyModel(&model);

    if (!larodDisconnect(&conn, nullptr)) {
        throw runtime_error("Failed closing larod connection!");
    }

    if (modelFd >= 0) {
        close(modelFd);
    }

    // Call base GoogleTest Test::TearDown() for framework cleanup.
    Test::TearDown();
}
