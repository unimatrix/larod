/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <functional>
#include <gmock/gmock.h>
#include <sys/syscall.h>

namespace {

// Syscall wrapper functions.
std::function<int(int)> closew;
std::function<int(int)> dupw;
std::function<int(const char*, int)> openw;
std::function<ssize_t(int, void*, size_t)> readw;
std::function<ssize_t(int, const void*, size_t)> writew;
std::function<void*(void* addr, size_t length, int prot, int flags, int fd,
                    off_t offset)>
    mmapw;
std::function<int(void* addr, size_t length)> munmapw;

} // namespace

// Redefinition of syscalls.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wredundant-decls"
#ifdef __cplusplus
extern "C" {
#endif

int close(int fd) {
    return closew(fd);
}

int dup(int fd) {
    return dupw(fd);
}

int open(const char* pathname, int flags) {
    return openw(pathname, flags);
}

ssize_t read(int fd, void* buf, size_t count) {
    return readw(fd, buf, count);
}

ssize_t write(int fd, const void* buf, size_t count) {
    return writew(fd, buf, count);
}

void* mmap(void* addr, size_t length, int prot, int flags, int fd,
           off_t offset) {
    return mmapw(addr, length, prot, flags, fd, offset);
}

int munmap(void* addr, size_t length) {
    return munmapw(addr, length);
}

#ifdef __cplusplus
}
#endif
#pragma GCC diagnostic pop

namespace larod {
namespace test {

class SyscallMock {
public:
    SyscallMock() {
        closew = [this](int fd) { return close(fd); };

        dupw = [this](int fd) { return dup(fd); };

        openw = [this](const char* pathname, int flags) {
            return open(pathname, flags);
        };

        readw = [this](int fd, void* buf, size_t count) {
            return read(fd, buf, count);
        };

        writew = [this](int fd, const void* buf, size_t count) {
            return write(fd, buf, count);
        };

        mmapw = [this](void* addr, size_t length, int prot, int flags, int fd,
                       off_t offset) {
            return mmap(addr, length, prot, flags, fd, offset);
        };

        munmapw = [this](void* addr, size_t length) {
            return munmap(addr, length);
        };
    }

    ~SyscallMock() {
        closew = {};
        dupw = {};
        openw = {};
        readw = {};
        writew = {};
        mmapw = {};
        munmapw = {};
    }

    MOCK_METHOD(int, close, (int fd));
    MOCK_METHOD(int, dup, (int fd));
    MOCK_METHOD(int, open, (const char* pathname, int flags));
    MOCK_METHOD(ssize_t, read, (int fd, void* buf, size_t count));
    MOCK_METHOD(ssize_t, write, (int fd, const void* buf, size_t count));
    MOCK_METHOD(void*, mmap,
                (void* addr, size_t length, int prot, int flags, int fd,
                 off_t offset));
    MOCK_METHOD(int, munmap, (void* addr, size_t length));
};

} // namespace test
} // namespace larod
