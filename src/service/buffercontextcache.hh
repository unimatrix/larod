/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <unordered_set>

namespace larod {

/**
 * @brief @c BackEndUnits (Observers) can inherit from this class which acts as
 * an interface (Observable) for deleting cached context-based data to buffers.
 */
class BufferContextCache {
public:
    virtual ~BufferContextCache() = default;

    /**
     * @brief Removes all context-based data to @p cookie.
     *
     * Some backends might want to store a context for each @c Buffer that is
     * long-lived (c.f. Buffer::isSaved()). Each backend is free to cache such
     * contexts for every seen buffer cookie when @c runJobVirtual() is called.
     * This overridable method provides a way for a backend to delete such a
     * caching for a @p cookie whenever a buffer should no longer be cached in
     * the service.
     *
     * To handle a cached context correctly, the backend should:
     *  - Inherit from this class.
     *
     *  - Call Buffer::addObserver() from the backend to add the backend as an
     *    observer to the @c Buffer.
     *
     *  - Make the necessary cachings for the cookie in @p runJobVirtual(). The
     *    caching is not allowed to store a direct reference of the @c Buffer,
     *    but may copy relevant data.
     *
     *  - Implement this function to delete all context data associated to @p
     *    cookie.
     *
     * @param cookie The cookie to remove to context-based data from.
     */
    virtual void deleteBufferContext(uint64_t cookie) noexcept = 0;
};
} // namespace larod
