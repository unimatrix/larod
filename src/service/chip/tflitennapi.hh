/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <tensorflow/lite/delegates/nnapi/nnapi_delegate.h>

#include "tflite.hh"

namespace larod {
namespace backendunit {

class TFLiteNNAPI : public TFLite, public BufferContextCache {
public:
    TFLiteNNAPI();
    virtual ~TFLiteNNAPI();

    TFLiteNNAPI(const TFLiteNNAPI&) = delete;
    TFLiteNNAPI& operator=(const TFLiteNNAPI&) = delete;

    void deleteBufferContext(uint64_t cookie) noexcept override;

protected:
    std::unique_ptr<tflite::Interpreter>
        buildInterpreter(const tflite::FlatBufferModel& model) override;

    // TODO: Temporarily disable buffer_handles since it's broken for models
    //       with QUANTIZE nodes.
#if 0
    void setupInput(const std::vector<TfLiteTensor*>& tfInputTensors,
                    const std::vector<Tensor>& inputTensors,
                    tflite::Interpreter* interpreter) override;
    void setupOutput(const std::vector<TfLiteTensor*>& tfInputTensors,
                     const std::vector<Tensor>& inputTensors,
                     tflite::Interpreter* interpreter) override;
#endif

    /**
     * @brief Context to use buffer handles in NNAPI.
     *
     * For mmapable buffers, we create a buffer context. The buffer's fd is
     * mmaped in TFLite NNAPI delegate and we can thus circumvent one copy
     * during inference using a buffer handle pointing to the registered memory
     * buffer.
     */
    class BufferContext {
    public:
        BufferContext(const Buffer& buffer, const size_t size,
                      const int reqProt);
        ~BufferContext();

        BufferContext(BufferContext&& other) noexcept;
        BufferContext& operator=(BufferContext&& other) noexcept;
        BufferContext(const BufferContext& other) = delete;
        BufferContext& operator=(const BufferContext& other) = delete;

        /**
         * @brief Get a buffer handle pointing to the memory buffer.
         *
         * The buffer handle should be used with one
         * tflite::Interpreter::SetBufferHandle(). The buffer handle will be
         * unregistered when one registers a new buffer handle on the same
         * tensor.
         *
         * @param delegate NNAPI delegate where buffer will be registered.
         * @return Registered buffer handle.
         */
        TfLiteBufferHandle getHandle(tflite::StatefulNnApiDelegate& delegate);

        /**
         * @brief Get Access rights to memory buffer.
         */
        int getProt() const { return prot; }

    private:
        ANeuralNetworksMemory* memory = nullptr;
        int prot = 0; ///< Access rights to memory buffer.
    };

    /// Map of COOKIE -> BufferContext.
    std::unordered_map<uint64_t, std::shared_ptr<BufferContext>> bufferContexts;
    std::mutex mtxBufContexts; ///< Mutex for bufferContexts.

private:
    static inline const std::string THIS_NAMESPACE = "TFLiteNNAPI::";

    tflite::StatefulNnApiDelegate statefulNnApiDelegate;

    /**
     * @brief Create a buffer context.
     *
     * This will search through bufferContexts to find a buffer context for the
     * buffer based on the buffer's cookie. If no such context is found, it is
     * created and inserted into bufferContexts.
     *
     * @param buffer Buffer to create context for.
     * @param size Size of buffer that should be wrapped in context.
     * @param reProt The minimum access rights to the buffer's fd.
     * @return A shared pointer to a BufferContext.
     */
    std::shared_ptr<BufferContext> createBufferContext(Buffer& buffer,
                                                       const size_t size,
                                                       const int reqProt);
};

} // namespace backendunit
} // namespace larod
