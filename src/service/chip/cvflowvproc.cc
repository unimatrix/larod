/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "cvflowvproc.hh"

#include <cavalry_mem.h>
#include <sys/mman.h>

#include "cavalry.hh"
#include "larod.h"
#include "log.hh"
#include "memfd.hh"

using namespace std;

using larod::allocator::Cavalry;
using larod::allocator::MemFd;

namespace {

uint64_t inline align32(uint64_t val) {
    return ((val + 31) >> 5) << 5;
}

} // namespace

namespace larod {
namespace backendunit {

// Set up YUV (actually YCbCr color space defined by the JFIF file format
// used in JPEG) to RGB conversion matrix.
yuv2rgb_mat_t CvFlowVProc::yuv2RgbMat = {
    1, 1.402f, 0.344136f, 0.714136f, 1.772f, 0, {},
};

CvFlowVProc::CvFlowVProc()
    : BackEndUnit(Cavalry::getInstance(), Cavalry::getInstance()) {
    size_t binSz = initVProc();
    firmwareBinary = allocator::Cavalry::getInstance()->allocate(
        binSz, LAROD_FD_PROP_DMABUF | LAROD_FD_PROP_MAP, {});

    // FIXME: Unsafe cast to uint32_t from unsigned long (Ambarella is the
    // culprit here...).
    if (vproc_load_mfd(Cavalry::getInstance()->getCavalryFd(),
                       firmwareBinary->getMapping(firmwareBinary->getMaxSize(),
                                                  PROT_READ | PROT_WRITE),
                       firmwareBinary->getFd(),
                       static_cast<uint32_t>(firmwareBinary->getMaxSize())) <
        0) {
        throw runtime_error("Could not load VProc");
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

CvFlowVProc::~CvFlowVProc() {
    stopProcessingQueues();

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed");
}

uint32_t CvFlowVProc::initVProc() {
    uint32_t size;
    if (vproc_init(BIN_NAME.c_str(), &size) < 0) {
        throw runtime_error("Could not initialize VProc");
    }

    return size;
}

void CvFlowVProc::checkScaleFactor(const double widthQuotient,
                                   const double heightQuotient,
                                   const double maxScaleFactor) {
    if ((widthQuotient > maxScaleFactor) ||
        (widthQuotient < (1.0 / maxScaleFactor))) {
        throw invalid_argument("This VProc op only supports up to " +
                               to_string(maxScaleFactor) +
                               "x width scale factors");
    }

    if ((heightQuotient > maxScaleFactor) ||
        (heightQuotient < (1.0 / maxScaleFactor))) {
        throw invalid_argument("This VProc op only supports up to " +
                               to_string(maxScaleFactor) +
                               "x height scale factors");
    }
}

CvFlowVProc::OpType CvFlowVProc::checkOpParams(
    const pair<imageparams::MetaData, imageparams::MetaData>& imgMetaData,
    const pair<bool, imageparams::CropInfo>& crop) {
    auto& [inputMetaData, outputMetaData] = imgMetaData;
    bool formatDiff = inputMetaData.format != outputMetaData.format;
    bool dimsDiff = (inputMetaData.width != outputMetaData.width) ||
                    (inputMetaData.height != outputMetaData.height);
    double maxScaleFactor;
    OpType opType;

    if (crop.first || dimsDiff) {
        if (formatDiff) {
            opType = OpType::DEFORMATION;
            maxScaleFactor = DEFORM_MAX_SCALE_FACTOR;
        } else {
            opType = OpType::CROP_SCALE;
            maxScaleFactor = CROP_SCALE_MAX_FACTOR;
        }
    } else {
        if (!formatDiff) {
            throw invalid_argument("This operation does nothing");
        }
        opType = OpType::CONVERT_FORMAT;
    }

    // Note that the casting is ok due to the limit checks done in
    // checkMetaData() when loading the model.
    if (crop.first) {
        // Compute quotients to compare with maximum scale factors allowed.
        checkScaleFactor(static_cast<double>(crop.second.w) /
                             static_cast<double>(outputMetaData.width),
                         static_cast<double>(crop.second.h) /
                             static_cast<double>(outputMetaData.height),
                         maxScaleFactor);
    } else if (dimsDiff) {
        // Compute quotients to compare with maximum scale factors allowed.
        checkScaleFactor(static_cast<double>(inputMetaData.width) /
                             static_cast<double>(outputMetaData.width),
                         static_cast<double>(inputMetaData.height) /
                             static_cast<double>(outputMetaData.height),
                         maxScaleFactor);
    }

    return opType;
}

void CvFlowVProc::checkMetaData(imageparams::MetaData& metaData,
                                const string& desc) {
    if (!metaData.pitchSpecified) {
        // Enforce the alignment required by the VProc hardware.
        metaData.rowPitch = align32(metaData.width);
    }

    // Check image alignment.
    if (metaData.format == imageparams::Format::NV12) {
        if (metaData.width % NV12_HW_ALIGNMENT) {
            throw invalid_argument(desc +
                                   " in NV12 must have width divisible by " +
                                   to_string(NV12_HW_ALIGNMENT) + " for VProc");
        }

        if (metaData.height % NV12_HW_ALIGNMENT) {
            throw invalid_argument(desc +
                                   " in NV12 must have height divisible by " +
                                   to_string(NV12_HW_ALIGNMENT) + " for VProc");
        }
    }

    if (metaData.rowPitch % PITCH_ALIGNMENT) {
        throw invalid_argument(desc + " must have pitch divisible by " +
                               to_string(PITCH_ALIGNMENT) + " for VProc");
    }

    // Check image format.
    if ((metaData.format != imageparams::Format::NV12) &&
        (metaData.format != imageparams::Format::RGB_PLANAR)) {
        throw invalid_argument(
            desc +
            " format must be in either NV12 or planar rbg format for VProc");
    }
}

CvFlowVProc::Context::Context(
    const pair<imageparams::MetaData, imageparams::MetaData>& metaData)
    : imgMetaData(metaData) {
    auto& [inputMetaData, outputMetaData] = imgMetaData;

    // Verify all general aspects of the meta data.
    checkMetaData(inputMetaData, "Input");
    checkMetaData(outputMetaData, "Output");

    // At this point we don't know if there will be cropping or scaling done
    // by the model, but we can detect whether it will convert format.
    if (inputMetaData.format != outputMetaData.format) {
        if (inputMetaData.format != imageparams::Format::NV12) {
            throw invalid_argument(
                "VProc convert format op only supports NV12 as input format");
        }

        if (outputMetaData.format != imageparams::Format::RGB_PLANAR) {
            throw invalid_argument(
                "VProc convert format op only supports planar "
                "RGB as output format");
        }
    }
}

pair<vector<TensorMetadata>, vector<TensorMetadata>>
    CvFlowVProc::loadModelVirtual(const span<uint8_t>& data,
                                  const uint64_t modelId,
                                  const ParamsMap& params) {
    if (data.size() != 0) {
        throw invalid_argument("VProc does not use model data");
    }

    lock_guard<mutex> lockGuard(mtxContexts);

    shared_ptr<Context> ctx =
        make_shared<Context>(imageparams::getInputOutputInfo(
            params, numeric_limits<uint32_t>::max()));

    if (!contexts
             .emplace(piecewise_construct, forward_as_tuple(modelId),
                      forward_as_tuple(ctx))
             .second) {
        throw runtime_error("Could not save context");
    }

    auto inputs = createTensorMetadataFromImgMetadata(ctx->imgMetaData.first);
    auto outputs = createTensorMetadataFromImgMetadata(ctx->imgMetaData.second);

    return {inputs, outputs};
}

vector<TensorMetadata> CvFlowVProc::createTensorMetadataFromImgMetadata(
    const imageparams::MetaData& metadata) {
    size_t byteSize = imageparams::getByteSize(metadata);

    switch (metadata.format) {
    case imageparams::Format::NV12: {
        return {TensorMetadata(
            LAROD_TENSOR_DATA_TYPE_UINT8, LAROD_TENSOR_LAYOUT_420SP,
            {3, metadata.height, metadata.width},
            {byteSize, metadata.height * metadata.rowPitch, metadata.rowPitch},
            byteSize, "YUV420SP")};
    }
    case imageparams::Format::RGB_PLANAR: {
        return {TensorMetadata(
            LAROD_TENSOR_DATA_TYPE_UINT8, LAROD_TENSOR_LAYOUT_NCHW,
            {1, 3, metadata.height, metadata.width},
            {1 * byteSize, byteSize, metadata.height * metadata.rowPitch,
             metadata.rowPitch},
            byteSize, "RGB")};
    }
    default:
        // Should never happen...
        throw invalid_argument("Invalid image format");
    }
}

void CvFlowVProc::deleteModelVirtual(const uint64_t modelId) {
    lock_guard<mutex> lockGuard(mtxContexts);

    auto it = contexts.find(modelId);
    if (it == contexts.end()) {
        // Should never happen...
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    contexts.erase(it);
}

void CvFlowVProc::runJobVirtual(const uint64_t modelId,
                                vector<Tensor>& inputTensors,
                                vector<Tensor>& outputTensors,
                                const ParamsMap& params) {
    shared_ptr<Context> ctx;
    {
        lock_guard<mutex> lockGuard(mtxContexts);
        auto it = contexts.find(modelId);
        if (it == contexts.end()) {
            throw ModelNotFound("Model " + to_string(modelId) + " not found");
        }
        ctx = it->second;
    }
    auto& [inputMetaData, outputMetaData] = ctx->imgMetaData;

    // Sanity check sizes (these should have already been checked previously in
    // the call chain).
    assert(inputTensors.size() == 1);
    assert(outputTensors.size() == 1);
    assert(outputTensors[0].getPitches().back() == outputMetaData.rowPitch);

    auto crop = imageparams::getCropInfo(params, inputMetaData.width,
                                         inputMetaData.height,
                                         numeric_limits<uint32_t>::max());
    auto opType = checkOpParams(ctx->imgMetaData, crop);

    Buffer& inputBuffer = inputTensors[0].getBuffer();
    if (!(inputBuffer.getFdProps() &
          (LAROD_FD_PROP_DMABUF | LAROD_FD_PROP_READWRITE))) {
        throw invalid_argument(
            "Input tensor fd property not supported by this chip; "
            "supported: LAROD_FD_PROP_READWRITE, LAROD_FD_PROP_DMABUF");
    }

    Buffer& outputBuffer = outputTensors[0].getBuffer();
    if (!(outputBuffer.getFdProps() &
          (LAROD_FD_PROP_DMABUF | LAROD_FD_PROP_READWRITE))) {
        throw invalid_argument(
            "Output tensor fd property not supported by this chip; "
            "supported: LAROD_FD_PROP_READWRITE, LAROD_FD_PROP_DMABUF");
    }

    // Setup input based on the fd types given.
    unique_ptr<Cavalry::Buffer> tmpInputBuffer = nullptr;
    unsigned long inputSize = imageparams::getByteSize(inputMetaData);
    if (!(inputBuffer.getFdProps() & LAROD_FD_PROP_DMABUF)) {
        tmpInputBuffer =
            Cavalry::convertBufferPtr(Cavalry::getInstance()->allocate(
                inputSize, LAROD_FD_PROP_DMABUF | LAROD_FD_PROP_MAP, {}));

        // Copy input data to the cavalry file descriptor.
        span<uint8_t> buf(static_cast<uint8_t*>(tmpInputBuffer->virtAddr),
                          inputSize);
        inputBuffer.read(buf);
    }

    larod::Buffer& jobInputBuffer =
        tmpInputBuffer ? *tmpInputBuffer : inputBuffer;

    const Cavalry::Buffer* cavInBuf = Cavalry::castBuffer(jobInputBuffer);
    if (cavInBuf) {
        // larod allocated the buffer. Flush the cache.
        cavInBuf->flushCache();
    }

    // Setup output based on the fd types given.
    unique_ptr<Cavalry::Buffer> tmpOutputBuffer = nullptr;
    unsigned long outputSize = imageparams::getByteSize(outputMetaData);
    if (!(outputBuffer.getFdProps() & LAROD_FD_PROP_DMABUF)) {
        tmpOutputBuffer =
            Cavalry::convertBufferPtr(Cavalry::getInstance()->allocate(
                outputSize, LAROD_FD_PROP_DMABUF | LAROD_FD_PROP_MAP, {}));
    }

    larod::Buffer& jobOutputBuffer =
        tmpOutputBuffer ? *tmpOutputBuffer : outputBuffer;

    const Cavalry::Buffer* cavOutBuf = Cavalry::castBuffer(jobOutputBuffer);
    if (cavOutBuf) {
        // larod allocated the buffer. Invalidate the cache.
        cavOutBuf->invalidateCache();
    }

    // Run job with cavalry file descriptors.
    switch (opType) {
    case OpType::CONVERT_FORMAT:
        convertFormat(jobInputBuffer, jobOutputBuffer, inputMetaData,
                      outputMetaData);
        break;
    case OpType::CROP_SCALE:
        cropScale(jobInputBuffer, jobOutputBuffer, inputMetaData,
                  outputMetaData, crop);
        break;
    case OpType::DEFORMATION:
        cropScaleConvert(jobInputBuffer, jobOutputBuffer, inputMetaData,
                         outputMetaData, crop);
        break;
    default:
        // Should never happen...
        throw runtime_error("Invalid VProc operation");
    }

    if (!(outputBuffer.getFdProps() & LAROD_FD_PROP_DMABUF)) {
        // Copy output data from our temporary Cavalry::Buffer. We know that we
        // have created it since outputBuffer does not have fd prop
        // LAROD_FD_PROP_DMABUF (but only LAROD_FD_PROP_READWRITE).
        assert(cavOutBuf);
        span<uint8_t> buf(static_cast<uint8_t*>(cavOutBuf->virtAddr),
                          outputSize);
        outputBuffer.write(buf);
    }
}

unique_ptr<Buffer> CvFlowVProc::allocateInput(const size_t size,
                                              const uint32_t fdProps,
                                              const ParamsMap& params) const {
    return allocate(size, fdProps, params);
}

unique_ptr<Buffer> CvFlowVProc::allocateOutput(const size_t size,
                                               const uint32_t fdProps,
                                               const ParamsMap& params) const {
    return allocate(size, fdProps, params);
}

unique_ptr<Buffer> CvFlowVProc::allocate(const size_t size,
                                         const uint32_t fdProps,
                                         const ParamsMap& params) {
    if ((fdProps & LAROD_FD_PROP_DMABUF) &&
        (fdProps & LAROD_FD_PROP_READWRITE)) {
        throw new invalid_argument(
            "Can not allocate buffer with both properties: LAROD_PROP_DMABUF "
            ", LAROD_PROP_READWRITE");
    }

    if (fdProps == 0) {
        return Cavalry::getInstance()->allocate(
            size, LAROD_FD_PROP_DMABUF | LAROD_FD_PROP_MAP, params);
    } else if (fdProps & LAROD_FD_PROP_DMABUF) {
        return Cavalry::getInstance()->allocate(size, fdProps, params);
    }

    return MemFd::getInstance()->allocate(size, fdProps, params);
}

void CvFlowVProc::convertFormat(const Buffer& inputBuffer,
                                const Buffer& outputBuffer,
                                const imageparams::MetaData& inputMetaData,
                                const imageparams::MetaData& outputMetaData) {
    assert(static_cast<uint32_t>(inputMetaData.height) ==
           static_cast<uint32_t>(outputMetaData.height));
    assert(static_cast<uint32_t>(inputMetaData.width) ==
           static_cast<uint32_t>(outputMetaData.width));

    // Safe casts since meta data is already bounds checked.
    uint32_t height = static_cast<uint32_t>(inputMetaData.height);
    uint32_t width = static_cast<uint32_t>(inputMetaData.width);
    uint32_t inputPitch = static_cast<uint32_t>(inputMetaData.rowPitch);
    uint32_t inputOffset = static_cast<uint32_t>(inputBuffer.getStartOffset());

    // Set y-channel input params.
    vect_desc_mfd_t yInput;
    yInput.data_addr_fd = inputBuffer.getFd();
    yInput.data_addr_offset = inputOffset;
    yInput.pitch = inputPitch;
    yInput.shape.h = height;
    yInput.shape.w = width;
    yInput.shape.d = 1;
    yInput.roi.xoffset = 0;
    yInput.roi.yoffset = 0;
    yInput.roi.width = width;
    yInput.roi.height = height;
    yInput.color_space = CS_NV12;

    // Set uv-channel input params.
    vect_desc_mfd_t uvInput;
    uvInput.data_addr_fd = inputBuffer.getFd();
    uvInput.data_addr_offset = inputOffset + height * inputPitch;
    uvInput.pitch = inputPitch;
    uvInput.shape.h = height / 2;
    uvInput.shape.w = width / 2;
    uvInput.shape.d = 2;
    uvInput.roi.xoffset = 0;
    uvInput.roi.yoffset = 0;
    uvInput.roi.width = width / 2;
    uvInput.roi.height = height / 2;
    uvInput.color_space = CS_NV12;

    // Safe casts since meta data is already bounds checked.
    uint32_t outputPitch = static_cast<uint32_t>(outputMetaData.rowPitch);
    uint32_t outputOffset =
        static_cast<uint32_t>(outputBuffer.getStartOffset());

    // Set rgb-channel output params.
    vect_desc_mfd_t rgbOutput;
    rgbOutput.data_addr_fd = outputBuffer.getFd();
    rgbOutput.data_addr_offset = outputOffset;
    rgbOutput.pitch = outputPitch;
    rgbOutput.shape.h = height;
    rgbOutput.shape.w = width;
    rgbOutput.shape.d = 3;
    rgbOutput.roi.xoffset = 0;
    rgbOutput.roi.yoffset = 0;
    rgbOutput.roi.width = width;
    rgbOutput.roi.height = height;
    rgbOutput.color_space = CS_RGB;

    int ret = vproc_yuv2rgb_420_mfd(&yInput, &uvInput, &rgbOutput, &yuv2RgbMat);
    if (ret < 0) {
        throw runtime_error("Could not perform convert format op");
    }
}

void CvFlowVProc::cropScale(const Buffer& inputBuffer,
                            const Buffer& outputBuffer,
                            const imageparams::MetaData& inputMetaData,
                            const imageparams::MetaData& outputMetaData,
                            const pair<bool, imageparams::CropInfo> crop) {
    assert(inputMetaData.format == outputMetaData.format);

    color_space_t imgFormat;
    switch (inputMetaData.format) {
    case imageparams::Format::NV12: {
        imgFormat = CS_NV12;
        break;
    }
    case imageparams::Format::RGB_PLANAR: {
        imgFormat = CS_RGB;
        break;
    }
    default:
        // Should never happen...
        throw invalid_argument("Invalid input image format");
    }

    // Safe casts since meta data is already bounds checked.
    uint32_t inputHeight = static_cast<uint32_t>(inputMetaData.height);
    uint32_t inputWidth = static_cast<uint32_t>(inputMetaData.width);
    uint32_t inputPitch = static_cast<uint32_t>(inputMetaData.rowPitch);
    uint32_t inputOffset = static_cast<uint32_t>(inputBuffer.getStartOffset());
    uint32_t cropStartX = crop.first ? static_cast<uint32_t>(crop.second.x) : 0;
    uint32_t cropStartY = crop.first ? static_cast<uint32_t>(crop.second.y) : 0;
    uint32_t cropEndX =
        crop.first ? static_cast<uint32_t>(crop.second.w) : inputWidth;
    uint32_t cropEndY =
        crop.first ? static_cast<uint32_t>(crop.second.h) : inputHeight;

    // Set input params.
    vect_desc_mfd_t input;
    input.data_addr_fd = inputBuffer.getFd();
    input.data_addr_offset = inputOffset;
    input.pitch = inputPitch;
    input.shape.h = inputHeight;
    input.shape.w = inputWidth;
    input.shape.d = 3;
    input.roi.xoffset = cropStartX;
    input.roi.yoffset = cropStartY;
    input.roi.height = cropEndY;
    input.roi.width = cropEndX;
    input.color_space = imgFormat;

    // Safe casts since meta data is already bounds checked.
    uint32_t outputHeight = static_cast<uint32_t>(outputMetaData.height);
    uint32_t outputWidth = static_cast<uint32_t>(outputMetaData.width);
    uint32_t outputPitch = static_cast<uint32_t>(outputMetaData.rowPitch);
    uint32_t outputOffset =
        static_cast<uint32_t>(outputBuffer.getStartOffset());

    // Set output params.
    vect_desc_mfd_t output;
    output.data_addr_fd = outputBuffer.getFd();
    output.data_addr_offset = outputOffset;
    output.pitch = outputPitch;
    output.shape.h = outputHeight;
    output.shape.w = outputWidth;
    output.shape.d = 3;
    output.roi.xoffset = 0;
    output.roi.yoffset = 0;
    output.roi.width = outputWidth;
    output.roi.height = outputHeight;
    output.color_space = imgFormat;

    int ret = vproc_resize_mfd(&input, &output);
    if (ret < 0) {
        throw runtime_error("Vproc could not perform crop-scale");
    }
}

void CvFlowVProc::cropScaleConvert(
    const Buffer& inputBuffer, const Buffer& outputBuffer,
    const imageparams::MetaData& inputMetaData,
    const imageparams::MetaData& outputMetaData,
    const pair<bool, imageparams::CropInfo> crop) {
    // Safe casts since meta data is already bounds checked.
    uint32_t inputHeight = static_cast<uint32_t>(inputMetaData.height);
    uint32_t inputWidth = static_cast<uint32_t>(inputMetaData.width);
    uint32_t inputPitch = static_cast<uint32_t>(inputMetaData.rowPitch);
    uint32_t inputOffset = static_cast<uint32_t>(inputBuffer.getStartOffset());
    uint32_t cropStartX = crop.first ? static_cast<uint32_t>(crop.second.x) : 0;
    uint32_t cropStartY = crop.first ? static_cast<uint32_t>(crop.second.y) : 0;
    uint32_t cropEndX =
        crop.first ? static_cast<uint32_t>(crop.second.w) : inputWidth;
    uint32_t cropEndY =
        crop.first ? static_cast<uint32_t>(crop.second.h) : inputHeight;

    color_space_t inputImgFormat;
    switch (inputMetaData.format) {
    case imageparams::Format::NV12: {
        inputImgFormat = CS_NV12;
        break;
    }
    case imageparams::Format::RGB_PLANAR: {
        inputImgFormat = CS_RGB;
        break;
    }
    default:
        // Should never happen...
        throw invalid_argument("Invalid input image format");
    }

    // Set input params.
    vect_desc_mfd_t input;
    input.data_addr_fd = inputBuffer.getFd();
    input.data_addr_offset = inputOffset;
    input.pitch = inputPitch;
    input.shape.h = inputHeight;
    input.shape.w = inputWidth;
    input.shape.d = 3;
    input.roi.xoffset = cropStartX;
    input.roi.yoffset = cropStartY;
    input.roi.height = cropEndY;
    input.roi.width = cropEndX;
    input.color_space = inputImgFormat;

    // Safe casts since meta data is already bounds checked.
    uint32_t outputHeight = static_cast<uint32_t>(outputMetaData.height);
    uint32_t outputWidth = static_cast<uint32_t>(outputMetaData.width);
    uint32_t outputPitch = static_cast<uint32_t>(outputMetaData.rowPitch);
    uint32_t outputOffset =
        static_cast<uint32_t>(outputBuffer.getStartOffset());

    color_space_t outputImgFormat;
    switch (outputMetaData.format) {
    case imageparams::Format::NV12: {
        outputImgFormat = CS_NV12;
        break;
    }
    case imageparams::Format::RGB_PLANAR: {
        outputImgFormat = CS_RGB;
        break;
    }
    default:
        // Should never happen...
        throw invalid_argument("Invalid input image format");
    }

    // Set output params.
    vect_desc_mfd_t output;
    output.data_addr_fd = outputBuffer.getFd();
    output.data_addr_offset = outputOffset;
    output.pitch = outputPitch;
    output.shape.h = outputHeight;
    output.shape.w = outputWidth;
    output.shape.d = 3;
    output.roi.xoffset = 0;
    output.roi.yoffset = 0;
    output.roi.width = outputWidth;
    output.roi.height = outputHeight;
    output.color_space = outputImgFormat;

    // Unclear if and why this is needed...
    deformation_extra_t dext;
    memset(&dext, 0, sizeof(dext));

    int ret = vproc_image_deformation_mfd(&input, &output, &dext);
    if (ret < 0) {
        throw runtime_error("Could not perform crop scale convert op");
    }
}
} // namespace backendunit
} // namespace larod
