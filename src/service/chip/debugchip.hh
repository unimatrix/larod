/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <string>
#include <thread>
#include <unordered_set>

#include "backendunit.hh"
#include "buffercontextcache.hh"

namespace larod {
namespace backendunit {

class DebugChip : public BackEndUnit, public BufferContextCache {
public:
    DebugChip();
    ~DebugChip();

    std::pair<std::vector<TensorMetadata>, std::vector<TensorMetadata>>
        loadModelVirtual(const span<uint8_t>& data, const uint64_t modelId,
                         const ParamsMap& params) override;
    void deleteModelVirtual(const uint64_t modelId) override;
    void runJobVirtual(const uint64_t modelId,
                       std::vector<Tensor>& inputTensors,
                       std::vector<Tensor>& outputTensors,
                       const ParamsMap& params) override;
    larodChip getChip() override { return LAROD_CHIP_DEBUG; };

    void deleteBufferContext(const uint64_t cookie) noexcept override;

    static inline const size_t NBR_OF_INPUT_TENSORS = 1;
    static inline const size_t NBR_OF_OUTPUT_TENSORS = 1;

    static inline const size_t INPUT_TENSOR_BYTE_SIZE = 1;
    static inline const size_t OUTPUT_TENSOR_BYTE_SIZE = 4096;
    static const larodTensorDataType TENSOR_DATA_TYPE =
        LAROD_TENSOR_DATA_TYPE_UINT8;

    static const larodTensorLayout INPUT_TENSOR_LAYOUT =
        LAROD_TENSOR_LAYOUT_UNSPECIFIED;
    static const larodTensorLayout OUTPUT_TENSOR_LAYOUT =
        LAROD_TENSOR_LAYOUT_UNSPECIFIED;

    static inline const std::vector<size_t> INPUT_TENSOR_DIMS = {
        INPUT_TENSOR_BYTE_SIZE, 1, 1, 1};
    static inline const std::vector<size_t> INPUT_TENSOR_PITCHES = {
        INPUT_TENSOR_BYTE_SIZE, 1, 1, 1};
    static inline const std::vector<size_t> OUTPUT_TENSOR_DIMS = {
        OUTPUT_TENSOR_BYTE_SIZE, 1, 1, 1};
    static inline const std::vector<size_t> OUTPUT_TENSOR_PITCHES = {
        OUTPUT_TENSOR_BYTE_SIZE, 1, 1, 1};

    static inline const std::string INPUT_TENSOR_NAME = "DebugChipInput";
    static inline const std::string OUTPUT_TENSOR_NAME = "DebugChipOutput";

    static inline const std::vector<TensorMetadata> INPUT_TENSOR_METADATA = {
        NBR_OF_INPUT_TENSORS,
        TensorMetadata(TENSOR_DATA_TYPE, INPUT_TENSOR_LAYOUT, INPUT_TENSOR_DIMS,
                       INPUT_TENSOR_PITCHES, INPUT_TENSOR_BYTE_SIZE,
                       INPUT_TENSOR_NAME)};
    static inline const std::vector<TensorMetadata> OUTPUT_TENSOR_METADATA = {
        NBR_OF_OUTPUT_TENSORS,
        TensorMetadata(TENSOR_DATA_TYPE, OUTPUT_TENSOR_LAYOUT,
                       OUTPUT_TENSOR_DIMS, OUTPUT_TENSOR_PITCHES,
                       OUTPUT_TENSOR_BYTE_SIZE, OUTPUT_TENSOR_NAME)};

private:
    static inline const std::string THIS_NAMESPACE = "DebugChip::";

    // Every buffer requires a context, but this class in itself does nothing
    // except containing the cookie.
    struct BufferContext {
        BufferContext(const uint64_t modelId, const uint64_t cookie);
        ~BufferContext();
        BufferContext(const BufferContext&) = delete;
        BufferContext operator=(const BufferContext&) = delete;

        const uint64_t modelId;
        const uint64_t cookie;

        static inline const std::string THIS_NAMESPACE =
            DebugChip::THIS_NAMESPACE + "BufferContext::";
    };

private:
    // Maps model IDs to buffer cookies to buffer contexts.
    std::unordered_map<uint64_t, std::unordered_map<uint64_t, BufferContext>>
        contexts;
    std::mutex mtxContexts;

    std::mutex mtxModels; ///< Mutex for model set.
    std::unordered_set<uint64_t> models;
    std::vector<std::thread> procJobQueueThreads;
    std::vector<std::thread> procLoadModelQueueThreads;

    int getMinModelFormatVersion() const override { return 0; }
};

} // namespace backendunit
} // namespace larod
