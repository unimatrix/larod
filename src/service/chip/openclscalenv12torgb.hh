/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <string>

#define CL_KERNEL(...) #__VA_ARGS__

std::string openCLscalenv12torgbSource(CL_KERNEL(
    float blerp(float c00, float c10, float c01, float c11, float tx,
                float ty) {
        return mix(mix(c00, c10, tx), mix(c01, c11, tx), ty);
    }

    float4 blerp4(float4 c00, float4 c10, float4 c01, float4 c11, float4 tx,
                  float4 ty) {
        return mix(mix(c00, c10, tx), mix(c01, c11, tx), ty);
    }

    /**
     * Kernel designed to run scaling for a single row in the image
     */
    __kernel void scaleToRgb(__global __read_only uchar* src, int srcCbCrOffset,
                             int cropX, int cropY, int srcWidth, int srcPitch,
                             int srcHeight, __global __write_only uchar* dest,
                             int destWidth, int dstPitch, int destHeight,
                             float scaleFactorX, float scaleFactorY) {
        // This kernel is designed to scale a nv12 image, and convert the result
        // to rgb-interleaved. One whole row is operated on per kernel
        // iteration.
        //
        // The y-layer in the nv12 image is scaled with bilinear interpolation,
        // for cbcr nearest-neighbor is used.

        // Get the row to work on.
        const int id = get_global_id(0);
        const int groupSize = get_global_size(0);

        // Get the cbcr layer for the input.
        uchar* srcCbCr = src + srcCbCrOffset;

        for (int y = id; y < destHeight; y += groupSize) {
            // Get float-index into the source image (y coord).
            float srcY = y * scaleFactorY;

            // Trunc to int coordinate
            int yi = srcY;

            // How much should we interpolate between rows yi and yi+1?
            float mixy = srcY - floor(srcY);
            float16 mixy16 = (float16)(mixy);

            // Get pointers into source rows, and dest row being operated on.
            uchar* srcRow = src + (cropY + yi) * srcPitch + cropX;
            uchar* srcRow2 = srcRow + srcPitch;
            uchar* destRow = dest + y * dstPitch;
            uchar* srcCbCrRow = srcCbCr + srcPitch * ((cropY + yi) / 2) + cropX;

            // In the following loop, the scaling is performed 16-elements at a
            // time. One smaller loop after this takes care of the trailing
            // destWidth%16 elements, and an if-statement after that handles the
            // final element if destWidth is odd.

            // Vector of float x-coordinates into source image.
            float16 srcXs = {
                0 * scaleFactorX,  1 * scaleFactorX,  2 * scaleFactorX,
                3 * scaleFactorX,  4 * scaleFactorX,  5 * scaleFactorX,
                6 * scaleFactorX,  7 * scaleFactorX,  8 * scaleFactorX,
                9 * scaleFactorX,  10 * scaleFactorX, 11 * scaleFactorX,
                12 * scaleFactorX, 13 * scaleFactorX, 14 * scaleFactorX,
                15 * scaleFactorX};
            int x = 0;
            while (x < destWidth - 16) {
                // Trunc to int coordinates.
                int16 xi = convert_int16(srcXs);
                const int16 xi2 = xi + 1;

                // Load 2x2 pixel square for interpolation (times 16).
                const float16 c00 = {
                    srcRow[xi.s0], srcRow[xi.s1], srcRow[xi.s2], srcRow[xi.s3],
                    srcRow[xi.s4], srcRow[xi.s5], srcRow[xi.s6], srcRow[xi.s7],
                    srcRow[xi.s8], srcRow[xi.s9], srcRow[xi.sa], srcRow[xi.sb],
                    srcRow[xi.sc], srcRow[xi.sd], srcRow[xi.se], srcRow[xi.sf]};
                const float16 c10 = {
                    srcRow[xi2.s0], srcRow[xi2.s1], srcRow[xi2.s2],
                    srcRow[xi2.s3], srcRow[xi2.s4], srcRow[xi2.s5],
                    srcRow[xi2.s6], srcRow[xi2.s7], srcRow[xi2.s8],
                    srcRow[xi2.s9], srcRow[xi2.sa], srcRow[xi2.sb],
                    srcRow[xi2.sc], srcRow[xi2.sd], srcRow[xi2.se],
                    srcRow[xi2.sf]};
                const float16 c01 = {
                    srcRow2[xi.s0], srcRow2[xi.s1], srcRow2[xi.s2],
                    srcRow2[xi.s3], srcRow2[xi.s4], srcRow2[xi.s5],
                    srcRow2[xi.s6], srcRow2[xi.s7], srcRow2[xi.s8],
                    srcRow2[xi.s9], srcRow2[xi.sa], srcRow2[xi.sb],
                    srcRow2[xi.sc], srcRow2[xi.sd], srcRow2[xi.se],
                    srcRow2[xi.sf]};
                const float16 c11 = {
                    srcRow2[xi2.s0], srcRow2[xi2.s1], srcRow2[xi2.s2],
                    srcRow2[xi2.s3], srcRow2[xi2.s4], srcRow2[xi2.s5],
                    srcRow2[xi2.s6], srcRow2[xi2.s7], srcRow2[xi2.s8],
                    srcRow2[xi2.s9], srcRow2[xi2.sa], srcRow2[xi2.sb],
                    srcRow2[xi2.sc], srcRow2[xi2.sd], srcRow2[xi2.se],
                    srcRow2[xi2.sf]};

                // How much to interpolate in x-axis.
                const float16 mixX = srcXs - floor(srcXs);

                // Interpolate x-axis for row yi.
                const float16 mix1 = c00 + (c10 - c00) * mixX;

                // Interpolate x-axis for row yi+1.
                const float16 mix2 = c01 + (c11 - c01) * mixX;

                // Interpolate y-axis.
                const float16 yVal = mix1 + (mix2 - mix1) * mixy16;

                // TODO: The following line gives slightly better perfomance,
                // since it uses built-in instructions.
                // const float16 yVal =
                //     mix(mix(c00, c10, mixX), mix(c01, c11, mixX), mixy16);

                // Trunc xi down to even number, for cbcr extraction.
                xi &= ~0x01;

                // Load cbcr.
                float16 cb = {
                    srcCbCrRow[xi.s0], srcCbCrRow[xi.s1], srcCbCrRow[xi.s2],
                    srcCbCrRow[xi.s3], srcCbCrRow[xi.s4], srcCbCrRow[xi.s5],
                    srcCbCrRow[xi.s6], srcCbCrRow[xi.s7], srcCbCrRow[xi.s8],
                    srcCbCrRow[xi.s9], srcCbCrRow[xi.sa], srcCbCrRow[xi.sb],
                    srcCbCrRow[xi.sc], srcCbCrRow[xi.sd], srcCbCrRow[xi.se],
                    srcCbCrRow[xi.sf],
                };
                cb -= 128.0f;
                float16 cr = {
                    srcCbCrRow[xi.s0 + 1], srcCbCrRow[xi.s1 + 1],
                    srcCbCrRow[xi.s2 + 1], srcCbCrRow[xi.s3 + 1],
                    srcCbCrRow[xi.s4 + 1], srcCbCrRow[xi.s5 + 1],
                    srcCbCrRow[xi.s6 + 1], srcCbCrRow[xi.s7 + 1],
                    srcCbCrRow[xi.s8 + 1], srcCbCrRow[xi.s9 + 1],
                    srcCbCrRow[xi.sa + 1], srcCbCrRow[xi.sb + 1],
                    srcCbCrRow[xi.sc + 1], srcCbCrRow[xi.sd + 1],
                    srcCbCrRow[xi.se + 1], srcCbCrRow[xi.sf + 1],
                };
                cr -= 128.0f;

                // Calculate rgb vaues.
                const uchar16 r = convert_uchar16_sat(yVal + R_CR_CONST * cr);
                const uchar16 g = convert_uchar16_sat(yVal + G_CB_CONST * cb +
                                                      G_CR_CONST * cr);
                const uchar16 b = convert_uchar16_sat(yVal + B_CB_CONST * cb);

                // Interleave rgb values.
                const uchar16 res0 = {
                    r.s0, g.s0, b.s0, r.s1, g.s1, b.s1, r.s2, g.s2,
                    b.s2, r.s3, g.s3, b.s3, r.s4, g.s4, b.s4, r.s5,
                };
                const uchar16 res1 = {
                    g.s5, b.s5, r.s6, g.s6, b.s6, r.s7, g.s7, b.s7,
                    r.s8, g.s8, b.s8, r.s9, g.s9, b.s9, r.sa, g.sa,
                };
                const uchar16 res2 = {
                    b.sa, r.sb, g.sb, b.sb, r.sc, g.sc, b.sc, r.sd,
                    g.sd, b.sd, r.se, g.se, b.se, r.sf, g.sf, b.sf,
                };

                // Write rgb values.
                vstore16(res0, 0, destRow + x * 3);
                vstore16(res1, 1, destRow + x * 3);
                vstore16(res2, 2, destRow + x * 3);

                srcXs += 16 * scaleFactorX;
                x += 16;
            }

            // These two following blocks are of the exact same structure as the
            // vector-loop above, the only difference is the number of elements
            // being operated on.

            float srcX = srcXs.s0;
            while (x < destWidth - 1) {
                int xi = srcX;

                float yVal = blerp(srcRow[xi], srcRow[xi + 1], srcRow2[xi],
                                   srcRow[xi + 1], srcX - floor(srcX),
                                   srcY - floor(srcY));
                float cb = srcCbCrRow[xi & ~0x01] - 128.0f;
                float cr = srcCbCrRow[(xi & ~0x01) + 1] - 128.0f;

                float r = yVal + R_CR_CONST * cr;
                float g = yVal + G_CB_CONST * cb + G_CR_CONST * cr;
                float b = yVal + B_CB_CONST * cb;

                destRow[x * 3 + 0] = convert_uchar_sat(r);
                destRow[x * 3 + 1] = convert_uchar_sat(g);
                destRow[x * 3 + 2] = convert_uchar_sat(b);

                srcX += scaleFactorX;
                ++x;
            }

            if (x < destWidth) {
                int xi = srcX;
                float yVal = mix(srcRow[xi], srcRow2[xi], srcY - floor(srcY));
                float cb = srcCbCrRow[xi & ~0x01] - 128.0f;
                float cr = srcCbCrRow[(xi & ~0x01) + 1] - 128.0f;

                float r = yVal + R_CR_CONST * cr;
                float g = yVal + G_CB_CONST * cb + G_CR_CONST * cr;
                float b = yVal + B_CB_CONST * cb;

                destRow[x * 3 + 0] = convert_uchar_sat(r);
                destRow[x * 3 + 1] = convert_uchar_sat(g);
                destRow[x * 3 + 2] = convert_uchar_sat(b);
            }
        }
    }));
