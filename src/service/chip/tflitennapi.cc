/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "tflitennapi.hh"

#include <sys/mman.h>

using namespace std;

namespace larod {
namespace backendunit {

TFLiteNNAPI::BufferContext::BufferContext(const Buffer& buffer,
                                          const size_t size,
                                          const int reqProt) {
    if (static_cast<uint64_t>(buffer.getStartOffset()) >
        numeric_limits<size_t>::max()) {
        throw range_error("Buffer offset is too big (" +
                          to_string(buffer.getStartOffset()) + ")");
    }

    // According to NNAPI, the offset must be page aligned.
    size_t offset = static_cast<size_t>(buffer.getStartOffset());
    assert(static_cast<size_t>(sysconf(_SC_PAGE_SIZE)) <=
           numeric_limits<size_t>::max());
    if (offset % static_cast<size_t>(sysconf(_SC_PAGE_SIZE)) != 0) {
        throw invalid_argument("Buffer offset (" +
                               to_string(buffer.getStartOffset()) +
                               ") is not page aligned");
    }

    int fd = buffer.getFd();
    prot = Buffer::getMaxProt(fd, reqProt);
    int ret = NnApiImplementation()->ANeuralNetworksMemory_createFromFd(
        size, prot, fd, offset, &memory);
    if (ret != ANEURALNETWORKS_NO_ERROR) {
        throw runtime_error("Could not create buffer handle from fd (" +
                            to_string(ret) + ")");
    }
}

TFLiteNNAPI::BufferContext::~BufferContext() {
    NnApiImplementation()->ANeuralNetworksMemory_free(memory);
}

TFLiteNNAPI::BufferContext& TFLiteNNAPI::BufferContext::
    operator=(TFLiteNNAPI::BufferContext&& other) noexcept {
    if (this != &other) {
        memory = other.memory;
        prot = other.prot;

        other.memory = nullptr;
        other.prot = 0;
    }

    return *this;
}

TFLiteNNAPI::BufferContext::BufferContext(
    TFLiteNNAPI::BufferContext&& other) noexcept {
    *this = std::move(other);
}

TfLiteBufferHandle TFLiteNNAPI::BufferContext::getHandle(
    tflite::StatefulNnApiDelegate& statefulNnApiDelegate) {
    // Unclear what this function should do. It seems like it is never called.
    static tflite::StatefulNnApiDelegate::CopyToHostTensorFnPtr memoryCallback =
        [](TfLiteTensor*, ANeuralNetworksMemory*, size_t, size_t,
           void*) -> TfLiteStatus {
        LOG.error("NNAPI CopyToHostTensorFnPtr is not implemented");

        return kTfLiteError;
    };

    return statefulNnApiDelegate.RegisterNnapiMemory(memory, memoryCallback,
                                                     nullptr);
}

TFLiteNNAPI::TFLiteNNAPI() {
}

TFLiteNNAPI::~TFLiteNNAPI() {
    // We own NNAPI delegate and we must make sure that no job is running (and
    // using it) while we destroy it.
    stopProcessingQueues();

    // We must make sure no one uses the models that depend on the delegate.
    unique_lock<mutex> lock(mtxInterpreters);
    interpreters.clear();
    lock.unlock();
}

unique_ptr<tflite::Interpreter>
    TFLiteNNAPI::buildInterpreter(const tflite::FlatBufferModel& model) {
    tflite::ops::builtin::BuiltinOpResolver resolver;
    tflite::InterpreterBuilder builder(model, resolver);
    unique_ptr<tflite::Interpreter> interpreter;
    builder(&interpreter);

    if (interpreter->ModifyGraphWithDelegate(&statefulNnApiDelegate) !=
        kTfLiteOk) {
        string msg = "Could not set NNAPI delegate";
        LOG.error(msg);

        throw runtime_error(msg);
    }

    return interpreter;
}

void TFLiteNNAPI::deleteBufferContext(uint64_t cookie) noexcept {
    LOG.debug(THIS_NAMESPACE + __func__ +
              "(): Deleting buffer context for cookie=" + to_string(cookie));
    unique_lock<mutex> lock(mtxBufContexts);
    auto it = bufferContexts.find(cookie);
    if (it == bufferContexts.end()) {
        // Should never happen...
        LOG.error("Could not find NNAPI buffer context");
        return;
    }

    bufferContexts.erase(it);
}

shared_ptr<TFLiteNNAPI::BufferContext>
    TFLiteNNAPI::createBufferContext(Buffer& buffer, const size_t size,
                                     const int reqProt) {
    uint64_t cookie = buffer.getCookie();

    unique_lock<mutex> lock(mtxBufContexts);
    auto it = bufferContexts.find(cookie);
    if (it == bufferContexts.end()) {
        shared_ptr<TFLiteNNAPI::BufferContext> bufCtx;
        try {
            bufCtx = make_shared<BufferContext>(buffer, size, reqProt);
        } catch (exception& e) {
            throw runtime_error("Could not create buffer context: " +
                                string(e.what()));
        }

        auto [insertedIt, success] =
            bufferContexts.insert(make_pair(cookie, std::move(bufCtx)));
        if (!success) {
            // Should never happen...
            throw runtime_error("Could not insert buffer context in map");
        }

        buffer.addObserver(this);

        return insertedIt->second;
    }

    if (!(it->second->getProt() & reqProt)) {
        throw invalid_argument(
            "Could not create buffer context: Missing access rights");
    }

    return it->second;
}

// TODO: Temporarily disable buffer_handles since it's broken for models with
//       QUANTIZE nodes.
#if 0
void TFLiteNNAPI::setupInput(const vector<TfLiteTensor*>& tfInputTensors,
                             const vector<Tensor>& inputTensors,
                             tflite::Interpreter* interpreter) {
    for (size_t i = 0; i < tfInputTensors.size(); ++i) {
        Buffer& providedInput = inputTensors[i].getBuffer();
        auto fdProps = providedInput.getFdProps();
        size_t size = tfInputTensors[i]->bytes;
        bool useBufferHandle = providedInput.getFdProps() & LAROD_FD_PROP_MAP;

        if (useBufferHandle) {
            auto bufCtx = createBufferContext(providedInput, size, PROT_READ);

            TfLiteBufferHandle bufferHandle =
                bufCtx->getHandle(statefulNnApiDelegate);

            TfLiteStatus ret = interpreter->SetBufferHandle(
                interpreter->inputs()[i], bufferHandle, &statefulNnApiDelegate);
            if (ret != kTfLiteOk) {
                throw runtime_error(
                    "Could not set buffer handle on input tensor (" +
                    to_string(ret) + ")");
            }
        } else if (fdProps & LAROD_FD_PROP_READWRITE) {
            TfLiteStatus ret = interpreter->SetBufferHandle(
                interpreter->inputs()[i], kTfLiteNullBufferHandle,
                &statefulNnApiDelegate);
            if (ret != kTfLiteOk) {
                throw runtime_error(
                    "Could not reset buffer handle on input tensor (" +
                    to_string(ret) + ")");
            }
            span<uint8_t> inputBuffer(tfInputTensors[i]->data.uint8, size);
            providedInput.read(inputBuffer);
        } else {
            // Should never happen...
            assert(false);
            throw invalid_argument("Input tensor fd property is unknown; "
                                   "supported: " +
                                   Buffer::getPropNames(supportedInputFdProps));
        }
    }
}

void TFLiteNNAPI::setupOutput(const vector<TfLiteTensor*>& tfOutputTensors,
                              const vector<Tensor>& outputTensors,
                              tflite::Interpreter* interpreter) {
    for (size_t i = 0; i < tfOutputTensors.size(); ++i) {
        Buffer& providedOutput = outputTensors[i].getBuffer();
        bool useBufferHandle = providedOutput.getFdProps() & LAROD_FD_PROP_MAP;

        if (!useBufferHandle) {
            TfLiteStatus ret = interpreter->SetBufferHandle(
                interpreter->outputs()[i], kTfLiteNullBufferHandle,
                &statefulNnApiDelegate);
            if (ret != kTfLiteOk) {
                throw runtime_error(
                    "Could not reset buffer handle on output tensor (" +
                    to_string(ret) + ")");
            }
            continue;
        }

        auto bufCtx = createBufferContext(
            providedOutput, tfOutputTensors[i]->bytes, PROT_WRITE);
        TfLiteBufferHandle bufferHandle =
            bufCtx->getHandle(statefulNnApiDelegate);

        TfLiteStatus ret = interpreter->SetBufferHandle(
            interpreter->outputs()[i], bufferHandle, &statefulNnApiDelegate);
        if (ret != kTfLiteOk) {
            throw runtime_error(
                "Could not set buffer handle on output tensor (" +
                to_string(ret) + ")");
        }
    }
}
#endif

} // namespace backendunit
} // namespace larod
