/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <EGL/egl.h>
#include <tensorflow/lite/delegates/gpu/gl_delegate.h>

#include "log.h"
#include "tflite.hh"

namespace larod {
namespace backendunit {

class TFLiteGPU : public TFLite {
public:
    TFLiteGPU();
    ~TFLiteGPU();

    TFLiteGPU(const TFLiteGPU&) = delete;
    TFLiteGPU& operator=(const TFLiteGPU&) = delete;

    larodChip getChip() override { return LAROD_CHIP_TFLITE_GLGPU; };

protected:
    void runJobVirtual(const uint64_t modelId,
                       std::vector<fdhandling::Buffer>& inputFds,
                       std::vector<fdhandling::Buffer>& outputFds,
                       const ParamsMap& params) override;

private:
    class EGLDisplayWrapper {
    public:
        EGLDisplay display = nullptr;

        EGLDisplayWrapper();
        ~EGLDisplayWrapper();
    };

    class EGLContextWrapper {
    public:
        EGLDisplayWrapper displayWrapper;
        EGLContext context = nullptr;

        EGLContextWrapper();
        ~EGLContextWrapper();

    private:
        static constexpr EGLint const ATTR_LIST[7] = {
            EGL_RED_SIZE, 1, EGL_GREEN_SIZE, 1, EGL_BLUE_SIZE, 1, EGL_NONE};
        static constexpr EGLint const CONTEXT_ATTRS[5] = {
            EGL_CONTEXT_MAJOR_VERSION, 3, EGL_CONTEXT_MINOR_VERSION, 1,
            EGL_NONE};
    };

private:
    static inline const std::string THIS_NAMESPACE = "TFLiteGPU::";
    static constexpr int32_t ALLOW_FP16 = 1;

    std::unique_ptr<TfLiteDelegate, std::function<void(TfLiteDelegate*)>>
        delegate{nullptr,
                 [](TfLiteDelegate* d) { TfLiteGpuDelegateDelete(d); }};
    EGLContextWrapper contextWrapper;

    std::unique_ptr<tflite::Interpreter>
        buildInterpreter(const tflite::FlatBufferModel& model) override;

    /**
     * @brief Bind EGLContext to current rendering thread.
     *
     * Since the tflite interpreter is constructed in another thread than the
     * one running jobs, we must keep track of the EGLContext. One must call
     * bindEGLContext() (unbindEGLContext()) before (after) each interpreter
     * call.
     */
    void bindEGLContext() const;
    void unbindEGLContext() const;

    EGLContext const& context() const { return contextWrapper.context; }
    EGLDisplay const& display() const {
        return contextWrapper.displayWrapper.display;
    }
};

} // namespace backendunit
} // namespace larod
