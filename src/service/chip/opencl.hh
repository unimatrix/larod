/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <string>

#include "backendunit.hh"
#include "imageparams.hh"
#include "jobrequest.hh"
#include "log.hh"
#include "paramsmap.hh"

namespace larod {
namespace backendunit {

class OpenCL : public BackEndUnit {
public:
    OpenCL();
    ~OpenCL();

    OpenCL(const OpenCL&) = delete;
    OpenCL& operator=(const OpenCL&) = delete;

    larodChip getChip() override { return LAROD_CHIP_OPENCL; };

protected:
    std::pair<std::vector<TensorMetadata>, std::vector<TensorMetadata>>
        loadModelVirtual(const span<uint8_t>& data, const uint64_t modelId,
                         const ParamsMap& params) override;
    void deleteModelVirtual(const uint64_t modelId) override;
    void runJobVirtual(const uint64_t modelId,
                       std::vector<Tensor>& inputTensors,
                       std::vector<Tensor>& outputTensors,
                       const ParamsMap& params) override;

    int getMinModelFormatVersion() const override { return -1; }

private:
    /**
     * @brief Context for an OpenCL device.
     *
     * Each OpenCL device has its own command queue and possibly configuration
     * of group sizes.
     */
    struct DeviceContext {
        const std::string NAME;
        cl::CommandQueue queue;
        const size_t MAX_WORK_GROUP_SIZE;
        const size_t PREFERRED_WORK_GROUP_SIZE;

        DeviceContext(const std::string& name, const cl::Context& context,
                      const cl::Device& device, size_t maxWorkGroupSize,
                      size_t preferredWorkGroupSize)
            : NAME(name), queue(context, device),
              MAX_WORK_GROUP_SIZE(maxWorkGroupSize),
              PREFERRED_WORK_GROUP_SIZE(preferredWorkGroupSize) {}
    };

    /**
     * @brief Context for model.
     *
     * When loading a model, one of these Context is created to store the
     * input/output metadata, OpenCL buffers and a pointer to the specific
     * device context to use. The specific device is chosen at construction if a
     * "device" parameter is present in the @c ParamsMap, otherwise the first
     * element in the device context vector is chosen.
     */
    struct Context {
        Context(
            const ParamsMap& map,
            const std::vector<std::shared_ptr<DeviceContext>>& deviceContexts);

        imageparams::MetaData inputMetaData;
        imageparams::MetaData outputMetaData;
        cl::Buffer input;
        cl::Buffer output;
        std::shared_ptr<DeviceContext> deviceContext;
    };

    cl::Platform platform;
    cl::Context context;
    cl::Program program;
    cl::Kernel kernelY;
    cl::Kernel kernelCbCr;
    cl::Kernel kernelConvert;
    cl::Kernel kernelScaleConvert;

    std::vector<cl::Device> devices;
    std::mutex mtxDevices;
    /// A vector of device contexts that corresponds 1-to-1 to the devices in
    /// the @c devices vector.
    std::vector<std::shared_ptr<DeviceContext>> deviceContexts;

    /**
     * @brief Initializes OpenCL.
     *
     * Initializes OpenCL devices and compiles kernels. After this, larod uses
     * quite a bit of RAM, so this is done when loading the first model instead
     * of at construction.
     */
    void initializeDevices();

    static inline const std::string THIS_NAMESPACE = "OpenCL::";

    // Note that this is potentially hardware specific.
    static constexpr size_t DEFAULT_NUM_WORK_GROUPS = 4;

    /**
     * @brief Get the work group size multiple of a device.
     *
     * For an OpenCL device, this takes the minimum preferred work-group-size
     * multiple of all the kernels.  They are generally always the same for all
     * the kernels, but if they're not we take the smallest to enable a more
     * fine-grained work size for the kernels.
     */
    size_t getMinWorkGroupSizeMultiple(const cl::Device& dev);

    /**
     * @brief Calculate the number of local/global work items.
     *
     * Calculates the work items using DEFAULT_NUM_WORK_GROUPS as the desired
     * number of work groups.
     *
     * @return A tuple consisting of <globalWorkSize,localWorkSize>.
     */
    std::tuple<size_t, size_t> calcWorkSizes(DeviceContext* devCtx,
                                             size_t totalWorkItems);

    std::unordered_map<uint64_t, std::shared_ptr<Context>> contexts;
    std::mutex mtxContexts;

    std::vector<TensorMetadata>
        createTensorsFromMetaData(const imageparams::MetaData& metaData);

    void cropScaleConvert(DeviceContext* devCtx, cl::Buffer& input,
                          cl::Buffer& output,
                          const imageparams::MetaData& inputMetaData,
                          const imageparams::MetaData& outputMetaData,
                          const imageparams::CropInfo& cropInfo);

    void cropScale(DeviceContext* devCtx, cl::Buffer& input, cl::Buffer& output,
                   const imageparams::MetaData& inputMetaData,
                   const imageparams::MetaData& outputMetaData,
                   const imageparams::CropInfo& cropInfo);

    void colorConvert(DeviceContext* devCtx, cl::Buffer& input,
                      cl::Buffer& output,
                      const imageparams::MetaData& inputMetaData,
                      const imageparams::MetaData& outputMetaData);

    inline static size_t roundDown(size_t x, size_t mult);

    template<typename... Args>
    static void setKernelArgs(cl::Kernel& kernel, Args... args);
};

} // namespace backendunit
} // namespace larod
