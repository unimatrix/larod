/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "cvflownnctrl.hh"

#include <cavalry/cavalry_ioctl.h>
#include <cavalry_mem.h>
#include <errno.h>
#include <math.h>
#include <sys/mman.h>

#include "cavalry.hh"
#include "model.hh"
#include "modelformat_generated.h"

using namespace std;

using larod::allocator::Cavalry;
using larod::allocator::MemFd;

namespace larod {
namespace backendunit {

CvFlowNNCtrl::CvFlowNNCtrl() {
    // Initialize nnctrl.
    if (nnctrl_init(Cavalry::getInstance()->getCavalryFd(), 0) < 0) {
        throw runtime_error("NNCtrl init failed");
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

CvFlowNNCtrl::~CvFlowNNCtrl() {
    stopProcessingQueues();

    unique_lock<mutex> lock(mtxModels);
    models.clear();
    lock.unlock();

    nnctrl_exit();

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed");
}

void CvFlowNNCtrl::Net::setIOConfigs(const uint8_t* data) {
    // Initialize a temporary net to retrieve I/O names and bitmaps.
    net_cfg config{};
    config.net_feed_virt = data;
    config.reuse_mem = 1;
    int tmpID = nnctrl_init_net_by_mfd(&config, NULL, NULL);
    if (tmpID < 0) {
        throw runtime_error("Could not initialize net");
    }

    net_input_mfd_cfg inTmp = {};
    net_output_mfd_cfg outTmp = {};

    int ret = nnctrl_get_net_io_cfg_by_mfd(tmpID, &inTmp, &outTmp);
    if (ret) {
        int exitRet = nnctrl_exit_net(tmpID);
        if (exitRet < 0) {
            LOG.warning("Could not free net resources (" + to_string(exitRet) +
                        ")");
        }
        throw runtime_error("Could not get net configuration (" +
                            to_string(ret) + ")");
    }

    // Set the necessary variables in the real configurations. The name, bitmap
    // and no_mem variables are the only ones that are not set automatically
    // later on when using these structs for the "real" initialization. The
    // names are stored in bufferNames because the I/O configs need a "const
    // char*".
    netIn.in_num = inTmp.in_num;
    netOut.out_num = outTmp.out_num;
    for (size_t i = 0; i < inTmp.in_num; ++i) {
        netIn.in_desc[i].no_mem = 1;
        netIn.in_desc[i].rotate_flip_bitmap =
            inTmp.in_desc[i].rotate_flip_bitmap;
        bufferNames.emplace_back(inTmp.in_desc[i].name);
    }
    for (size_t i = 0; i < outTmp.out_num; ++i) {
        netOut.out_desc[i].no_mem = 1;
        netOut.out_desc[i].rotate_flip_bitmap =
            outTmp.out_desc[i].rotate_flip_bitmap;
        bufferNames.emplace_back(outTmp.out_desc[i].name);
    }

    for (size_t i = 0; i < bufferNames.size(); ++i) {
        if (i < netIn.in_num) {
            netIn.in_desc[i].name = bufferNames[i].c_str();
        } else {
            netOut.out_desc[i - netIn.in_num].name = bufferNames[i].c_str();
        }
    }

    ret = nnctrl_exit_net(tmpID);
    if (ret) {
        throw runtime_error("Could not free net resources (" + to_string(ret) +
                            ")");
    }
}

CvFlowNNCtrl::Net::Net(const uint8_t* data) : id(-1) {
    // Retrieve input/output configuration.
    setIOConfigs(data);

    // Initialize network.
    net_cfg config{};
    config.net_feed_virt = data;
    config.reuse_mem = 1;
    id = nnctrl_init_net_by_mfd(&config, &netIn, &netOut);
    if (id < 0) {
        throw runtime_error("Could not initialize net");
    }
    idPtr.reset(&id);

    // Allocate memory for network.
    unique_ptr<Cavalry::Buffer> netBuf =
        Cavalry::convertBufferPtr(Cavalry::getInstance()->allocate(
            config.net_mem_total, LAROD_FD_PROP_DMABUF | LAROD_FD_PROP_MAP,
            {}));

    cavalry_mfd_desc memDescMfd{};
    memDescMfd.fd = netBuf->getFd();
    memDescMfd.virt_addr = netBuf->virtAddr;
    memDescMfd.mem_size = netBuf->getMaxSize();

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Loading net");
    int ret = nnctrl_load_net_by_mfd(*idPtr, &memDescMfd, &netIn, &netOut);
    if (ret) {
        throw runtime_error("Could not load net (" + to_string(ret) + ")");
    }

    // nnctrl_load_net_by_mfd() writes to the given buffer, so we flush the
    // cache here.
    netBuf->flushCache();

    cavMemBuffers.push_back(std::move(netBuf));

    allocateBuffers();
}

void CvFlowNNCtrl::Net::allocateBuffers() {
    LOG.debug(THIS_NAMESPACE + __func__ +
              "(): Allocating input/output buffers");

    for (size_t i = 0; i < netIn.in_num; ++i) {
        unique_ptr<Cavalry::Buffer> inBuf =
            Cavalry::convertBufferPtr(Cavalry::getInstance()->allocate(
                netIn.in_desc[i].size, LAROD_FD_PROP_DMABUF | LAROD_FD_PROP_MAP,
                {}));

        netIn.in_desc[i].mem_fd = inBuf->getFd();
        netIn.in_desc[i].virt = static_cast<uint8_t*>(inBuf->virtAddr);
        netIn.in_desc[i].fd_offset = 0;

        cavMemBuffers.push_back(std::move(inBuf));
    }

    for (size_t i = 0; i < netOut.out_num; ++i) {
        unique_ptr<Cavalry::Buffer> outBuf =
            Cavalry::convertBufferPtr(Cavalry::getInstance()->allocate(
                netOut.out_desc[i].size,
                LAROD_FD_PROP_DMABUF | LAROD_FD_PROP_MAP, {}));

        netOut.out_desc[i].mem_fd = outBuf->getFd();
        netOut.out_desc[i].virt = static_cast<uint8_t*>(outBuf->virtAddr);
        netOut.out_desc[i].fd_offset = 0;

        cavMemBuffers.push_back(std::move(outBuf));
    }
}

tuple<net_input_mfd_cfg, net_output_mfd_cfg>
    CvFlowNNCtrl::Net::getIOConfigs() const {
    return {netIn, netOut};
}

larodTensorDataType CvFlowNNCtrl::getDataType(uint8_t sign, uint8_t sz,
                                              uint8_t expbits) {
    for (const auto& [fmt, type] : DATA_TYPES) {
        if (fmt.sign == sign && fmt.size == sz && fmt.expbits == expbits) {
            return type;
        }
    }

    LOG.debug(THIS_NAMESPACE + __func__ +
              "(): Unknown data type (sign=" + to_string(sign) +
              ", sz=" + to_string(sz) + ", expbits=" + to_string(expbits) +
              "), defaulting to unspecifed");

    return LAROD_TENSOR_DATA_TYPE_UNSPECIFIED;
}

template<typename T> TensorMetadata CvFlowNNCtrl::createTensorMetadata(T desc) {
    auto larodType = getDataType(desc->data_fmt.sign, desc->data_fmt.size,
                                 desc->data_fmt.expbits);
    uint8_t sizeBytes = static_cast<uint8_t>(1 << desc->data_fmt.size);
    size_t sz = desc->size;

    // Calculate pitches.
    vector<size_t> pitches(4);
    pitches[3] =
        static_cast<size_t>(ceil(desc->dim.width * sizeBytes / 32.)) * 32;
    pitches[2] = pitches[3] * desc->dim.height;
    pitches[1] = pitches[2] * desc->dim.depth;
    pitches[0] = pitches[1] * desc->dim.plane;

    return TensorMetadata(
        larodType, LAROD_TENSOR_LAYOUT_UNSPECIFIED,
        {desc->dim.plane, desc->dim.depth, desc->dim.height, desc->dim.width},
        pitches, sz, desc->name);
}

pair<vector<TensorMetadata>, vector<TensorMetadata>>
    CvFlowNNCtrl::getTensorMetadata(int netId) {
    net_input_cfg netIn = {};
    net_output_cfg netOut = {};

    if (nnctrl_get_net_io_cfg(netId, &netIn, &netOut) < 0) {
        throw runtime_error("NNCtrl get net io failed");
    }

    vector<TensorMetadata> inputTensorMetadata;
    for (unsigned int i = 0; i < netIn.in_num; i++) {
        inputTensorMetadata.push_back(createTensorMetadata(&netIn.in_desc[i]));
    }

    vector<TensorMetadata> outputTensorMetadata;
    for (unsigned int i = 0; i < netOut.out_num; i++) {
        outputTensorMetadata.push_back(
            createTensorMetadata(&netOut.out_desc[i]));
    }

    return {inputTensorMetadata, outputTensorMetadata};
}

pair<vector<TensorMetadata>, vector<TensorMetadata>>
    CvFlowNNCtrl::loadModelVirtual(const span<uint8_t>& data,
                                   const uint64_t modelId,
                                   const ParamsMap& params) {
    // We don't support any additional parameters.
    if (params.size()) {
        throw runtime_error("Additional parameters are not supported");
    }

    // Check model format. We first try to unpack the model data interpreted as
    // a .larod flatbuffer and if it fails, we use the raw model data.
    const uint8_t* modelData = data.data();
    const modelformat::Model* fbModel = verifyFbModel(data);
    const modelformat::CvFlow* fbCvFlow =
        fbModel ? fbModel->backEndData_as_CvFlow() : nullptr;
    if (fbCvFlow) {
        modelData =
            reinterpret_cast<const uint8_t*>(fbCvFlow->nativeData()->data());
    }

    shared_ptr<Net> net = make_shared<Net>(modelData);

    // Extract tensor metadata
    auto [inputTensorMetadata, outputTensorMetadata] =
        getTensorMetadata(net->getId());

    unique_lock<mutex> lock(mtxModels);
    bool ret = models.insert(make_pair(modelId, net)).second;
    lock.unlock();

    if (!ret) {
        // Should never happen...
        throw runtime_error("Could not load model: ID " + to_string(modelId) +
                            " already exists");
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Model " + to_string(modelId) +
              " loaded (id=" + to_string(net->getId()) + ")");

    return {inputTensorMetadata, outputTensorMetadata};
}

void CvFlowNNCtrl::deleteModelVirtual(const uint64_t modelId) {
    lock_guard<mutex> lockGuard(mtxModels);

    auto it = models.find(modelId);
    if (it == models.end()) {
        // Should never happen...
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    models.erase(it);

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Deleted model " +
              to_string(modelId));
}

void CvFlowNNCtrl::prepareInputs(net_input_mfd_cfg& netIn,
                                 const vector<Tensor>& inputTensors) const {
    assert(netIn.in_num == inputTensors.size());
    for (unsigned int i = 0; i < netIn.in_num; i++) {
        input_mfd_desc& desc = netIn.in_desc[i];
        const Buffer& buffer = inputTensors[i].getBuffer();
        if (buffer.getFdProps() & LAROD_FD_PROP_DMABUF) {
            desc.virt = nullptr;
            desc.mem_fd = buffer.getFd();
            desc.fd_offset =
                static_cast<unsigned long>(buffer.getStartOffset());

            const Cavalry::Buffer* cavInBuf = Cavalry::castBuffer(buffer);
            if (cavInBuf) {
                // larod allocated the buffer. Flush the cache.
                cavInBuf->flushCache();
            }
        } else if (buffer.getFdProps() & LAROD_FD_PROP_READWRITE) {
            span<uint8_t> inputBuffer(desc.virt,
                                      static_cast<size_t>(desc.size));
            buffer.read(inputBuffer);

            // Size for desc is offset exclusive which is adjusted for here.
            Cavalry::flushCache(desc.mem_fd, desc.size + desc.fd_offset,
                                static_cast<int64_t>(desc.fd_offset));
        } else {
            throw invalid_argument(
                "Input tensor fd property not supported by this chip; "
                "supported: LAROD_FD_PROP_READWRITE, LAROD_FD_PROP_DMABUF");
        }
    }
}

void CvFlowNNCtrl::prepareOutputs(net_output_mfd_cfg& netOut,
                                  const vector<Tensor>& outputTensors) const {
    assert(netOut.out_num == outputTensors.size());
    for (unsigned int i = 0; i < netOut.out_num; i++) {
        output_mfd_desc& desc = netOut.out_desc[i];
        const Buffer& buffer = outputTensors[i].getBuffer();
        if (buffer.getFdProps() & LAROD_FD_PROP_DMABUF) {
            desc.virt = nullptr;
            desc.mem_fd = buffer.getFd();
            desc.fd_offset =
                static_cast<unsigned long>(buffer.getStartOffset());

            const Cavalry::Buffer* cavOutBuf = Cavalry::castBuffer(buffer);
            if (cavOutBuf) {
                // larod allocated the buffer. Invalidate the cache.
                cavOutBuf->invalidateCache();
            }
        } else if (buffer.getFdProps() & LAROD_FD_PROP_READWRITE) {
            // The output is written to the default cavalry memory and copied
            // into output fd after inference. Invalidate cache here to make
            // this write visible.

            // Size for desc is offset exclusive which is adjusted for here.
            Cavalry::invalidateCache(desc.mem_fd, desc.size + desc.fd_offset,
                                     static_cast<int64_t>(desc.fd_offset));
        } else {
            throw invalid_argument(
                "Output tensor fd property not supported by this chip; "
                "supported: LAROD_FD_PROP_READWRITE, LAROD_FD_PROP_DMABUF");
        }
    }
}

void CvFlowNNCtrl::copyOutputs(net_output_mfd_cfg& netOut,
                               const vector<Tensor>& outputTensors) const {
    assert(netOut.out_num == outputTensors.size());
    for (unsigned int i = 0; i < netOut.out_num; i++) {
        output_mfd_desc& desc = netOut.out_desc[i];

        const Buffer& buffer = outputTensors[i].getBuffer();

        if (buffer.getFdProps() & LAROD_FD_PROP_DMABUF) {
            // Do nothing here. The output has already been written to the given
            // cavalry memory fd.
        } else if (buffer.getFdProps() & LAROD_FD_PROP_READWRITE) {
            span<uint8_t> outputBuffer(desc.virt,
                                       static_cast<size_t>(desc.size));
            buffer.write(outputBuffer);
        } else {
            // Should never happen, since it has already been checked.
            throw invalid_argument(
                "Output tensor fd property not supported by this chip; "
                "supported: LAROD_FD_PROP_READWRITE, LAROD_FD_PROP_DMABUF");
        }
    }
}

void CvFlowNNCtrl::runJobVirtual(const uint64_t modelId,
                                 vector<Tensor>& inputTensors,
                                 vector<Tensor>& outputTensors,
                                 const ParamsMap& params) {
    if (params.size()) {
        throw runtime_error("Additional parameters are not supported");
    }

    // Get the Net. We save a local reference just in case someone wants to
    // delete it while we run inference.
    shared_ptr<Net> net;
    try {
        lock_guard<mutex> lockGuard(mtxModels);
        net = models.at(modelId);
    } catch (...) {
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    auto [netIn, netOut] = net->getIOConfigs();

    prepareInputs(netIn, inputTensors);
    prepareOutputs(netOut, outputTensors);

    // Run inference.
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Starting job...");
    net_result net_ret = {};
    int result =
        nnctrl_run_net_by_mfd(net->getId(), &net_ret, NULL, &netIn, &netOut);
    int retry = 10;
    while (result == -EAGAIN && retry) {
        LOG.debug(THIS_NAMESPACE + __func__ +
                  "(): Got EAGAIN, trying again...");
        result = nnctrl_resume_net(net->getId(), &net_ret);
        retry--;
    }
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Job done");

    if (result < 0) {
        throw runtime_error("NNCtrl run net failed: " + to_string(result));
    }

    copyOutputs(netOut, outputTensors);
}

unique_ptr<Buffer> CvFlowNNCtrl::allocateInput(const size_t size,
                                               const uint32_t fdProps,
                                               const ParamsMap& params) const {
    return allocate(size, fdProps, params);
}

unique_ptr<Buffer> CvFlowNNCtrl::allocateOutput(const size_t size,
                                                const uint32_t fdProps,
                                                const ParamsMap& params) const {
    return allocate(size, fdProps, params);
}

unique_ptr<Buffer> CvFlowNNCtrl::allocate(const size_t size,
                                          const uint32_t fdProps,
                                          const ParamsMap& params) {
    if ((fdProps & LAROD_FD_PROP_DMABUF) &&
        (fdProps & LAROD_FD_PROP_READWRITE)) {
        throw new invalid_argument(
            "Can not allocate buffer with both properties: LAROD_PROP_DMABUF "
            ", LAROD_PROP_READWRITE");
    }

    if (fdProps == 0) {
        return Cavalry::getInstance()->allocate(
            size, LAROD_FD_PROP_DMABUF | LAROD_FD_PROP_MAP, params);
    } else if (fdProps & LAROD_FD_PROP_DMABUF) {
        return Cavalry::getInstance()->allocate(size, fdProps, params);
    }

    return MemFd::getInstance()->allocate(size, fdProps, params);
}

} // namespace backendunit
} // namespace larod
