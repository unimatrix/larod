/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "debugchip.hh"

#include <numeric>
#include <sys/mman.h>

#include "log.hh"
#include "memfd.hh"
#include "model.hh"

using namespace std;

namespace larod {
namespace backendunit {

DebugChip::DebugChip()
    : BackEndUnit(allocator::MemFd::getInstance(),
                  allocator::MemFd::getInstance()) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

DebugChip::~DebugChip() {
    stopProcessingQueues();

    if (contexts.size() > 0) {
        LOG.error(THIS_NAMESPACE + __func__ +
                  "(): " + to_string(contexts.size()) +
                  " model(s) have not destroyed all their BufferContexts");
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destroyed");
}

pair<vector<TensorMetadata>, vector<TensorMetadata>>
    DebugChip::loadModelVirtual(const span<uint8_t>&, const uint64_t modelId,
                                const ParamsMap& params) {
    // We don't support any additional parameters.
    if (params.size()) {
        throw runtime_error("Additional parameters are not supported");
    }

    unique_lock<mutex> lock(mtxModels);

    auto ret = models.insert(modelId);
    if (!ret.second) {
        // Should never happen...
        string msg = "Could not load model: ID " + to_string(modelId) +
                     " already exists";
        LOG.error(msg);
        throw runtime_error(msg);
    }

    lock.unlock();

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Model " + to_string(modelId) +
              " loaded");

    return {INPUT_TENSOR_METADATA, OUTPUT_TENSOR_METADATA};
}

void DebugChip::deleteModelVirtual(const uint64_t modelId) {
    lock_guard<mutex> lockGuard(mtxModels);

    auto it = models.find(modelId);
    if (it == models.end()) {
        // Should never happen...
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    models.erase(it);

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Deleted model " +
              to_string(modelId));
}

void DebugChip::runJobVirtual(const uint64_t modelId,
                              vector<Tensor>& inputTensors,
                              vector<Tensor>& outputTensors,
                              const ParamsMap& params) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Running job...");

    // We don't support any additional parameters.
    if (params.size()) {
        throw runtime_error("Additional parameters are not supported");
    }

    unique_lock<mutex> lock(mtxModels);

    auto it = models.find(modelId);
    if (it == models.end()) {
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    lock.unlock();

    // Set up input BufferContexts.
    for (size_t i = 0; i < inputTensors.size(); ++i) {
        if (auto& buf = inputTensors[i].getBuffer(); buf.isSaved()) {
            const uint64_t cookie = buf.getCookie();

            unique_lock<mutex> lock(mtxContexts);

            if (contexts[modelId].count(cookie) == 0) {
                contexts[modelId].emplace(piecewise_construct,
                                          forward_as_tuple(cookie),
                                          forward_as_tuple(modelId, cookie));
                buf.addObserver(this);
            }
        }
    }

    // Set up output BufferContexts.
    for (size_t i = 0; i < outputTensors.size(); ++i) {
        if (auto& buf = outputTensors[i].getBuffer(); buf.isSaved()) {
            const uint64_t cookie = buf.getCookie();

            unique_lock<mutex> lock(mtxContexts);

            if (contexts[modelId].count(cookie) == 0) {
                contexts[modelId].emplace(piecewise_construct,
                                          forward_as_tuple(cookie),
                                          forward_as_tuple(modelId, cookie));
                buf.addObserver(this);
            }
        }
    }

    for (size_t i = 0; i < inputTensors.size(); ++i) {
        Buffer& buffer = inputTensors[i].getBuffer();

        if (buffer.getFdProps() & LAROD_FD_PROP_MAP) {
            // Do nothing. Pretend that the mapped buffer is read from directly.
            buffer.getMapping(INPUT_TENSOR_METADATA[i].getByteSize(),
                              PROT_READ);
        } else if (buffer.getFdProps() & LAROD_FD_PROP_READWRITE) {
            // Read from input fd's without using the data. If a buffer is
            // mapped, don't read to simulate speed increase.
            vector<uint8_t> buf(INPUT_TENSOR_METADATA[i].getByteSize());
            buffer.read(buf);
        } else {
            // Buffer can neither be mapped or read from so an error is thrown.
            throw invalid_argument(
                "Input tensor fd property not supported by this chip; "
                "supported: LAROD_FD_PROP_READWRITE, LAROD_FD_PROP_MAP");
        }
    }

    // Write sequential data to the output file descriptors.
    size_t totalOutputSize = 0;
    for (size_t i = 0; i < outputTensors.size(); ++i) {
        Buffer& buffer = outputTensors[i].getBuffer();
        if (buffer.getFdProps() & LAROD_FD_PROP_MAP) {
            size_t outputSize = OUTPUT_TENSOR_METADATA[i].getByteSize();
            uint8_t* outMapping = buffer.getMapping(outputSize, PROT_WRITE);
            span<uint8_t> buf(outMapping, outputSize);

            // Write directly to the mapped pointer.
            iota(buf.begin(), buf.end(), 0);
            totalOutputSize += buf.size();
        } else if (buffer.getFdProps() & LAROD_FD_PROP_READWRITE) {
            vector<uint8_t> buf(OUTPUT_TENSOR_METADATA[i].getByteSize());

            // Output can't be memory mapped; copy data from temp vector to fd.
            iota(buf.begin(), buf.end(), 0);
            buffer.write(buf);
            totalOutputSize += buf.size();
        } else {
            // Buffer can neither be mapped or written to so an error is thrown.
            throw invalid_argument(
                "Output tensor fd property not supported by this chip; "
                "supported: LAROD_FD_PROP_READWRITE, LAROD_FD_PROP_MAP");
        }
    }

    // Simulate work by sleeping for a while. The total sleep time is
    // totalOutputSize * 1250 ns. For example, using a total output size of 4 KB
    // yields 4 * 1000 * 1250 ns = 5 ms sleep time. There is no simulated
    // loading time if totalOutputSize <= 1.
    if (totalOutputSize > 1) {
        this_thread::sleep_for(chrono::nanoseconds(totalOutputSize * 1250));
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Job done");
}

void DebugChip::deleteBufferContext(const uint64_t cookie) noexcept {
    bool hasDeleted = false;
    unique_lock<mutex> lock(mtxContexts);

    auto it = contexts.begin();
    while (it != contexts.end()) {
        unordered_map<uint64_t, BufferContext>& ctxs = it->second;

        if (ctxs.count(cookie) != 0) {
            hasDeleted = true;
            ctxs.erase(cookie);
        }

        if (ctxs.size() == 0) {
            it = contexts.erase(it);
        } else {
            ++it;
        }
    }

    lock.unlock();

    // We should at least have saved one context when this function is called.
    if (!hasDeleted) {
        LOG.error(THIS_NAMESPACE + __func__ + " tried to delete cookie " +
                  to_string(cookie) + " which was not found");
    }
}

DebugChip::BufferContext::BufferContext(const uint64_t modelId,
                                        const uint64_t cookie)
    : modelId(modelId), cookie(cookie) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Created context with cookie " +
              to_string(cookie) + " for model " + to_string(modelId));
}

DebugChip::BufferContext::~BufferContext() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destroyed context with cookie " +
              to_string(cookie) + " for model " + to_string(modelId));
}

} // namespace backendunit
} // namespace larod
