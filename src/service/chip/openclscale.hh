/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <string>

#define CL_KERNEL(...) #__VA_ARGS__

std::string openCLscaleSource(CL_KERNEL(

    // Bi-linear interpolation, using built-in mix().
    float blerp(float c00, float c10, float c01, float c11, float tx,
                float ty) {
        return mix(mix(c00, c10, tx), mix(c01, c11, tx), ty);
    }

    __kernel void scaleCbCr(__global __read_only uchar* src, int srcCbCrOffset,
                            int cropX, int cropY, int srcWidth, int srcPitch,
                            int srcHeight, __global __write_only uchar* dest,
                            int destWidth, int dstPitch, int destHeight,
                            float scaleFactorX, float scaleFactorY) {
        // This kernel scales the CbCr part of an YCbCr image.
        // Each kernel invokation runs over an entire row in the output.
        //
        // The scaled output is computed by nearest-neighbor.

        // Get row to operate on.
        const int id = get_global_id(0);
        const int groupSize = get_global_size(0);

        uchar* srcCbCr = src + srcCbCrOffset;
        uchar* dstCbCr = dest + dstPitch * destHeight;

        cropY /= 2;

        for (int pos = id; pos < (destHeight + 1) / 2; pos += groupSize) {
            int yi = pos * scaleFactorY;
            uchar* srcCbCrRow = srcCbCr + (yi + cropY) * srcPitch + cropX;
            uchar* dstCbCrRow = dstCbCr + pos * dstPitch;

            // The algorithm is split into 2 parts,
            // first a 16-element vector loop, to SIMD most of the work,
            // and then a single-element loop to finish off the remainder.

            // This vector represents the sub-pixel x-coordinate in the source
            // image
            float16 srcX16 = {
                0 * scaleFactorX,  1 * scaleFactorX,  2 * scaleFactorX,
                3 * scaleFactorX,  4 * scaleFactorX,  5 * scaleFactorX,
                6 * scaleFactorX,  7 * scaleFactorX,  8 * scaleFactorX,
                9 * scaleFactorX,  10 * scaleFactorX, 11 * scaleFactorX,
                12 * scaleFactorX, 13 * scaleFactorX, 14 * scaleFactorX,
                15 * scaleFactorX};
            int xs = 15;
            while (xs < (destWidth + 1) / 2) {
                // Convert the sub-pixel x-coordinate to a nearest-neighbor
                // coordinate (by truncation).
                const int16 xi = 2 * convert_int16(srcX16);

                // Load 2 16-batches of CbCr, i.e. 16 pairs of cbcr.
                uchar16 cbcr1 = {
                    srcCbCrRow[xi.s0], srcCbCrRow[xi.s0 + 1],
                    srcCbCrRow[xi.s1], srcCbCrRow[xi.s1 + 1],
                    srcCbCrRow[xi.s2], srcCbCrRow[xi.s2 + 1],
                    srcCbCrRow[xi.s3], srcCbCrRow[xi.s3 + 1],
                    srcCbCrRow[xi.s4], srcCbCrRow[xi.s4 + 1],
                    srcCbCrRow[xi.s5], srcCbCrRow[xi.s5 + 1],
                    srcCbCrRow[xi.s6], srcCbCrRow[xi.s6 + 1],
                    srcCbCrRow[xi.s7], srcCbCrRow[xi.s7 + 1],
                };
                uchar16 cbcr2 = {
                    srcCbCrRow[xi.s8], srcCbCrRow[xi.s8 + 1],
                    srcCbCrRow[xi.s9], srcCbCrRow[xi.s9 + 1],
                    srcCbCrRow[xi.sa], srcCbCrRow[xi.sa + 1],
                    srcCbCrRow[xi.sb], srcCbCrRow[xi.sb + 1],
                    srcCbCrRow[xi.sc], srcCbCrRow[xi.sc + 1],
                    srcCbCrRow[xi.sd], srcCbCrRow[xi.sd + 1],
                    srcCbCrRow[xi.se], srcCbCrRow[xi.se + 1],
                    srcCbCrRow[xi.sf], srcCbCrRow[xi.sf + 1],
                };

                // Since values will be packed in dest, we use vector-store.
                vstore16(cbcr1, 0, dstCbCrRow);
                dstCbCrRow += 16;
                vstore16(cbcr2, 0, dstCbCrRow);
                dstCbCrRow += 16;

                srcX16 += 16 * scaleFactorX;
                xs += 16;
            }

            // Handle remainder...
            float srcX = srcX16.s0;
            for (int x = xs - 15; x < (destWidth + 1) / 2; ++x) {
                int xi = srcX;
                *dstCbCrRow++ = srcCbCrRow[2 * xi];     // cb
                *dstCbCrRow++ = srcCbCrRow[2 * xi + 1]; // cr
                srcX += scaleFactorX;
            }
        }
    }

    __kernel void scaleY(__global __read_only uchar* src, int cropX, int cropY,
                         int srcWidth, int srcPitch, int srcHeight,
                         __global __write_only uchar* dest, int destWidth,
                         int dstPitch, int destHeight, float scaleFactorX,
                         float scaleFactorY) {
        // This kernel is designed to scale the Y layer of a YCbCr image
        // using bilinear interpolation.
        // Each kernel invocation works on one whole row in the output image.

        // Get the output row to work on
        const int id = get_global_id(0);
        const int groupSize = get_global_size(0);

        for (int y = id; y < destHeight; y += groupSize) {
            float srcY =
                y * scaleFactorY; // Sub-pixel y-coordinate in source image.
            int yi = srcY;        // Truncated y-coordinate in source image.

            // In bilinear interpolation we interpolate between a 2x2-tile of
            // pixels. This variable, mixY, denotes how much we should
            // interpolate in the y-axis. Basically:
            //    mixY = 0 -> use row[yi] directly,
            //    mixY = 0.5 -> use (row[yi] + row[yi+1]) / 2
            //    mixY = 1 -> use row[yi+1] directly
            float mixY = srcY - floor(srcY);

            // Extend mixY to a 16-vector, since we're doing most of the work
            // with 16-vectors. Note also that the vector operation is using
            // SIMD over the x-axis, which means that the interpolation for the
            // y-axis is the same across all 16 values.
            float16 mixY16 = (float16)(mixY);

            uchar* srcRow = src + (yi + cropY) * srcPitch +
                            cropX; // Pointer into source row index yi
            uchar* srcRow2 =
                srcRow + srcPitch; // Pointer into source row index yi+1
            uchar* destRow = dest + y * dstPitch; // Pointer to destination row

            // 16-vector of subpixel x-coordinates into the source image.
            float16 srcX16 = {
                0 * scaleFactorX,  1 * scaleFactorX,  2 * scaleFactorX,
                3 * scaleFactorX,  4 * scaleFactorX,  5 * scaleFactorX,
                6 * scaleFactorX,  7 * scaleFactorX,  8 * scaleFactorX,
                9 * scaleFactorX,  10 * scaleFactorX, 11 * scaleFactorX,
                12 * scaleFactorX, 13 * scaleFactorX, 14 * scaleFactorX,
                15 * scaleFactorX};

            // Vector of x-coordinates into the destination image.  Since we use
            // vector-store below, we shouldn't actually need to use a vector to
            // store all 16 of these coordinates (only xVec.s0 is actually
            // needed). But if I remember correctly, when implementing this
            // kernel we ran into some weird behaviour when only using a single
            // int x=0, and we couldn't find the reason why so...
            // TODO: Change into a single int.
            int16 xVec = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

            // Most of the work is done in 16-vector SIMD operations here.  The
            // remainder is handled below.
            while (xVec.sf < destWidth - 1) {
                const int16 xi =
                    convert_int16(srcX16); // Truncate sub-pixel x-coordinates
                const int16 xi2 = xi + 1;  // Get interpolation coordinates x+1

                // Get pixel(0, 0) for interpolation
                const float16 c00 = {
                    srcRow[xi.s0], srcRow[xi.s1], srcRow[xi.s2], srcRow[xi.s3],
                    srcRow[xi.s4], srcRow[xi.s5], srcRow[xi.s6], srcRow[xi.s7],
                    srcRow[xi.s8], srcRow[xi.s9], srcRow[xi.sa], srcRow[xi.sb],
                    srcRow[xi.sc], srcRow[xi.sd], srcRow[xi.se], srcRow[xi.sf]};

                // Get pixel(1, 0) for interpolation
                const float16 c10 = {
                    srcRow[xi2.s0], srcRow[xi2.s1], srcRow[xi2.s2],
                    srcRow[xi2.s3], srcRow[xi2.s4], srcRow[xi2.s5],
                    srcRow[xi2.s6], srcRow[xi2.s7], srcRow[xi2.s8],
                    srcRow[xi2.s9], srcRow[xi2.sa], srcRow[xi2.sb],
                    srcRow[xi2.sc], srcRow[xi2.sd], srcRow[xi2.se],
                    srcRow[xi2.sf]};

                // Get pixel(0, 1) for interpolation
                const float16 c01 = {
                    srcRow2[xi.s0], srcRow2[xi.s1], srcRow2[xi.s2],
                    srcRow2[xi.s3], srcRow2[xi.s4], srcRow2[xi.s5],
                    srcRow2[xi.s6], srcRow2[xi.s7], srcRow2[xi.s8],
                    srcRow2[xi.s9], srcRow2[xi.sa], srcRow2[xi.sb],
                    srcRow2[xi.sc], srcRow2[xi.sd], srcRow2[xi.se],
                    srcRow2[xi.sf]};

                // Get pixel(1, 1) for interpolation
                const float16 c11 = {
                    srcRow2[xi2.s0], srcRow2[xi2.s1], srcRow2[xi2.s2],
                    srcRow2[xi2.s3], srcRow2[xi2.s4], srcRow2[xi2.s5],
                    srcRow2[xi2.s6], srcRow2[xi2.s7], srcRow2[xi2.s8],
                    srcRow2[xi2.s9], srcRow2[xi2.sa], srcRow2[xi2.sb],
                    srcRow2[xi2.sc], srcRow2[xi2.sd], srcRow2[xi2.se],
                    srcRow2[xi2.sf]};

                // Calculate interpolation factor for x-axis (per-element)
                const float16 mixX = srcX16 - floor(srcX16);

                const float16 a =
                    c00 +
                    (c10 - c00) *
                        mixX; // Interpolate x-axis for first row (index yi)
                const float16 b =
                    c01 +
                    (c11 - c01) *
                        mixX; // Interpolate x-axis for second row (index yi+1)
                const float16 resf = a + (b - a) * mixY16; // Interpolate y-axis

                // TODO: The following line gives slightly better perfomance,
                // since it uses built-in instructions, but it doesn't work with
                // some devices...
                //
                // const float16 resf =
                //     mix(mix(c00, c10, mixX), mix(c01, c11, mixX), mixY16);

                // Truncate result to uchar
                const uchar16 res = convert_uchar16(resf);

                // Vector store, since values are packed in the dest image
                vstore16(res, 0, destRow + xVec.s0);

                srcX16 += 16 * scaleFactorX;
                xVec += 16;
            }

            // Single element loop to handle remainder
            int x = xVec.s0;
            float srcX = srcX16.s0;
            while (x < destWidth - 1) {
                int xi = srcX;

                destRow[x] = blerp(srcRow[xi], srcRow[xi + 1], srcRow2[xi],
                                   srcRow[xi + 1], srcX - floor(srcX),
                                   srcY - floor(srcY));

                srcX += scaleFactorX;
                ++x;
            }

            // Handle last element if destination width is odd
            if (x < destWidth) {
                int xi = srcX;
                destRow[x] = mix(srcRow[xi], srcRow2[xi], srcY - floor(srcY));
            }
        }
    }));
