/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <cstdint> // vproc.h requires this...
#include <vproc.h>

#include "backendunit.hh"
#include "imageparams.hh"

namespace larod {
namespace backendunit {

class CvFlowVProc : public BackEndUnit {
public:
    CvFlowVProc();
    ~CvFlowVProc();

    larodChip getChip() override { return LAROD_CHIP_CVFLOW_PROC; };
    int getMinModelFormatVersion() const override { return 0; }

    enum class OpType { CONVERT_FORMAT, CROP_SCALE, DEFORMATION };

protected:
    std::pair<std::vector<TensorMetadata>, std::vector<TensorMetadata>>
        loadModelVirtual(const span<uint8_t>&, const uint64_t modelId,
                         const ParamsMap& params) override;
    void deleteModelVirtual(const uint64_t modelId) override;
    void runJobVirtual(const uint64_t modelId,
                       std::vector<Tensor>& inputTensors,
                       std::vector<Tensor>& outputTensors,
                       const ParamsMap& params) override;

    std::unique_ptr<Buffer>
        allocateInput(const size_t size, const uint32_t fdProps,
                      const ParamsMap& params) const override;

    std::unique_ptr<Buffer>
        allocateOutput(const size_t size, const uint32_t fdProps,
                       const ParamsMap& params) const override;

private:
    /**
     * @brief Model context.
     *
     * Created in loadModelVirtual() from meta data extracted from the @c
     * ParamsMap.
     */
    struct Context {
        Context(const std::pair<imageparams::MetaData, imageparams::MetaData>&
                    metaData);

        std::pair<imageparams::MetaData, imageparams::MetaData> imgMetaData;
    };

private:
    static inline const std::string THIS_NAMESPACE = "CvFlowVProc::";
    /// Firmware binary name.
    static inline const std::string BIN_NAME = "/lib/firmware/vproc.bin";
    static constexpr size_t NV12_HW_ALIGNMENT = 2;
    static constexpr size_t PITCH_ALIGNMENT = 32;
    static constexpr double DEFORM_MAX_SCALE_FACTOR = 4.0;
    static constexpr double CROP_SCALE_MAX_FACTOR = 256.0;

    static yuv2rgb_mat_t yuv2RgbMat;

    std::unique_ptr<larod::Buffer> firmwareBinary;
    std::unordered_map<uint64_t, std::shared_ptr<Context>> contexts;
    std::mutex mtxContexts;

    static uint32_t initVProc();
    static OpType checkOpParams(
        const std::pair<imageparams::MetaData, imageparams::MetaData>&
            imgMetaData,
        const std::pair<bool, struct imageparams::CropInfo>& crop);
    static void checkScaleFactor(const double widthQuotient,
                                 const double heightQuotient,
                                 const double maxScaleFactor);
    static std::vector<TensorMetadata> createTensorMetadataFromImgMetadata(
        const imageparams::MetaData& imgMetadata);
    static void checkMetaData(imageparams::MetaData& metadata,
                              const std::string& desc);
    static void convertFormat(const Buffer& inputBuffer,
                              const Buffer& outputBuffer,
                              const imageparams::MetaData& inputMetaData,
                              const imageparams::MetaData& outputMetaData);
    static void cropScale(const Buffer& inputBuffer, const Buffer& outputBuffer,
                          const imageparams::MetaData& inputMetaData,
                          const imageparams::MetaData& outputMetaData,
                          const std::pair<bool, imageparams::CropInfo> crop);
    static void
        cropScaleConvert(const Buffer& inputBuffer, const Buffer& outputBuffer,
                         const imageparams::MetaData& inputMetaData,
                         const imageparams::MetaData& outputMetaData,
                         const std::pair<bool, imageparams::CropInfo> crop);

    static std::unique_ptr<Buffer> allocate(const size_t size,
                                            const uint32_t fdProps,
                                            const ParamsMap& params);
};

} // namespace backendunit
} // namespace larod
