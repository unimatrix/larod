/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <memory>
#include <string>

#include "span.hh"

namespace larod {

/**
 * @brief Class representing a file descriptor.
 */
class FileDescriptor {
public:
    explicit FileDescriptor() : fd(-1) {}

    /**
     * @brief Constructor.
     *
     * @param fd File descriptor to take ownership of.
     */
    FileDescriptor(const int&& fd);

    /**
     * @brief Destructor.
     *
     * Closes the owned fd.
     */
    ~FileDescriptor();

    FileDescriptor(const FileDescriptor& other);
    FileDescriptor(FileDescriptor&& other) noexcept;
    FileDescriptor& operator=(const FileDescriptor& other);
    FileDescriptor& operator=(FileDescriptor&& other);

    operator int() const {
        return fd; // Be careful! Don't pass to functions that might invalidate
                   // this fd (e.g. syscall close()).
    }

    /**
     * @brief Map fd using the mmap system call.
     *
     * Uses prot @p mmapProt and flags MAP_SHARED.
     *
     * The @p sz mapped bytes will be unmapped at the resulting address when the
     * shared_ptr is destroyed.
     *
     * @param offset Number of bytes to offset mapping with.
     * @param sz Number of bytes to map.
     * @param mmapProt Mmap prot bitmask to be used.
     * @return Shared ptr to resulting map.
     */
    std::shared_ptr<uint8_t[]> map(const off_t offset, const size_t sz,
                                   const int mmapProt) const;

    /**
     * @brief Read from fd.
     *
     * @param buf Buffer to store read bytes in.
     * @param sz Number of bytes to read.
     * @return Number of read bytes.
     */
    size_t read(const span<uint8_t>& buf, const size_t sz) const;

    /**
     * @brief Write to fd.
     *
     * @param buf Buffer to write bytes from.
     * @param sz Number of bytes to write.
     * @return Number of written bytes.
     */
    size_t write(const span<uint8_t>& buf, const size_t sz) const;

    /**
     * @brief Explicitly close the fd.
     *
     * @c fd will be invalid after this call (will be set to -1).
     */
    void close();

private:
    static inline std::string THIS_NAMESPACE = "FileDescriptor::";

    int fd = -1;

    void reset();

    static int duplicate(const int fd);
};

} // namespace larod
