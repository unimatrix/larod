/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "allocator.hh"

#include <cassert>

#include "larod.h"
#include "log.hh"

using namespace std;

namespace larod {
namespace allocator {

Allocator::Allocator(const uint32_t fdProps) : supportedFdProps(fdProps) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

Allocator::~Allocator() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed");
}

std::unique_ptr<Buffer> Allocator::allocate(const size_t size,
                                            const uint32_t fdProps,
                                            const ParamsMap& params) {
    if ((fdProps & LAROD_FD_PROP_READWRITE) &&
        !(supportedFdProps & LAROD_FD_PROP_READWRITE)) {
        throw invalid_argument("Read/write buffer is not supported");
    }

    if ((fdProps & LAROD_FD_PROP_MAP) &&
        !(supportedFdProps & LAROD_FD_PROP_MAP)) {
        throw invalid_argument("Mappable buffer is not supported");
    }

    if ((fdProps & LAROD_FD_PROP_DMABUF) &&
        !(supportedFdProps & LAROD_FD_PROP_DMABUF)) {
        throw invalid_argument("DMA buffer is not supported");
    }

    unique_lock<mutex> lock(mtxAlloc);
    auto buf = allocateVirtual(size, fdProps, params);
    lock.unlock();

    assert(buf->getFdProps());

    return buf;
}

} // namespace allocator
} // namespace larod
