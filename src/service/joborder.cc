/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "joborder.hh"

#include "session.hh"

using namespace std;

namespace larod {

JobOrder::JobOrder(weak_ptr<Session> session, const size_t chipId)
    : Msg(session), CHIP_ID(chipId) {
}

shared_ptr<JobRequest> JobOrder::getJobRequest() {
    if (auto sessionPtr = session.lock()) {
        return sessionPtr->popJobRequest(CHIP_ID);
    } else {
        throw SessionExpired("Session has expired");
    }
}

} // namespace larod
