/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "service.hh"

#include <systemd/sd-daemon.h>
#include <systemd/sd-event.h>

#include "debugchip.hh"
#include "larod.h"
#include "log.hh"
#include "version.hh"

#ifdef LAROD_USE_CVFLOW_NNCTRL
#include "cvflownnctrl.hh"
#endif
#ifdef LAROD_USE_CVFLOW_VPROC
#include "cvflowvproc.hh"
#endif
#ifdef LAROD_USE_TFLITE_CPU
#include "tflitecpu.hh"
#endif
#ifdef LAROD_USE_TPU
#include "tflitetpu.hh"
#endif
#ifdef LAROD_USE_GPU
#include "tflitegpu.hh"
#endif
#ifdef LAROD_USE_LIBYUV
#include "libyuv.hh"
#endif
#ifdef LAROD_USE_OPENCL
#include "opencl.hh"
#endif

using namespace std;

namespace larod {

Service::Service(string name, string objPath, sdbusplus::bus::bus& b,
                 vector<larodChip> chips)
    : ServiceServerObj(b, objPath.c_str()), SERVICE_NAME(name),
      SERVICE_OBJ_PATH(objPath), bus(b) {
    // Initialize back end units
    for (larodChip chip : chips) {
        switch (chip) {
#ifdef LAROD_USE_CVFLOW_NNCTRL
        case LAROD_CHIP_CVFLOW_NN:
            units.push_back(make_unique<backendunit::CvFlowNNCtrl>());
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Created backend unit LAROD_CHIP_CVFLOW_NN");
            break;
#endif
#ifdef LAROD_USE_CVFLOW_VPROC
        case LAROD_CHIP_CVFLOW_PROC:
            units.push_back(make_unique<backendunit::CvFlowVProc>());
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Created backend unit LAROD_CHIP_CVFLOW_PROC");
            break;
#endif
#ifdef LAROD_USE_TFLITE_CPU
        case LAROD_CHIP_TFLITE_CPU:
            units.push_back(make_unique<backendunit::TFLiteCPU>());
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Created backend unit LAROD_CHIP_TFLITE_CPU");
            break;
#endif
#ifdef LAROD_USE_TPU
        case LAROD_CHIP_TPU:
            units.push_back(make_unique<backendunit::TFLiteTPU>());
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Created backend unit LAROD_CHIP_TPU");
            break;
#endif
#ifdef LAROD_USE_GPU
        case LAROD_CHIP_TFLITE_GLGPU:
            units.push_back(make_unique<backendunit::TFLiteGPU>());
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Created backend unit LAROD_CHIP_TFLITE_GLGPU");
            break;
#endif
#ifdef LAROD_USE_LIBYUV
        case LAROD_CHIP_LIBYUV:
            units.push_back(make_unique<backendunit::LibYuv>());
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Created backend unit LAROD_CHIP_LIBYUV");
            break;
#endif
#ifdef LAROD_USE_OPENCL
        case LAROD_CHIP_OPENCL:
            units.push_back(make_unique<backendunit::OpenCL>());
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Created backend unit LAROD_CHIP_OPENCL");
            break;
#endif
        case LAROD_CHIP_DEBUG:
            units.push_back(make_unique<backendunit::DebugChip>());
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Created backend unit LAROD_CHIP_DEBUG");
            break;
        default:
            throw invalid_argument("Invalid argument for enum chip type: " +
                                   to_string(chip));
        }
    }

    for (auto& unit : units) {
        unit->startProcessingQueues();
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed " + SERVICE_NAME +
              " " + SERVICE_OBJ_PATH);
}

int Service::sdbusIOHandler(sd_event_source*, int, uint32_t, void* userdata) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Called");

    sdbusplus::bus::bus* bus = static_cast<sdbusplus::bus::bus*>(userdata);
    while (bus->process_discard() > 0) {
        LOG.debug(THIS_NAMESPACE + __func__ + "(): Handled");
    }

    return 0;
}

int Service::stopServiceIOHandler(sd_event_source*, int, uint32_t,
                                  void* userdata) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Called");

    Service* service = static_cast<Service*>(userdata);
    service->stop();

    return 0;
}

void Service::clientLostHandler(sdbusplus::message::message& msg) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Called");

    string clientName;
    try {
        msg.read(clientName);
    } catch (sdbusplus::exception::SdBusError& e) {
        LOG.error(string("Could not to read message: ") + e.what() + "(" +
                  to_string(e.get_errno()) + ")");
        return;
    }

    // The client has lost its connection to the bus.
    uint64_t sessionId;
    try {
        sessionId = sessions.at(clientName)->getSessionId();
    } catch (out_of_range& e) {
        LOG.error(string("Could not find session: ") + e.what());

        return;
    }

    // Before deleting the Session, we should make sure that the D-Bus interface
    // for the Session is shutdown. This is because if the Session has outgoing
    // requests, it will not be destroyed right away, and might shutdown the
    // D-Bus interface later when it is running its destructor. Since sd-bus is
    // not thread-safe this might interfere with our other threads doing sd-bus
    // processing (this function is called from the sd-event loop).
    sessions.at(clientName)->stopBusProcessing();

    try {
        deleteSession(sessionId);
    } catch (runtime_error& e) {
        LOG.error(string("Could not delete session: ") + e.what());
        return;
    }

    LOG.info("Session " + to_string(sessionId) + " killed since client's (" +
             clientName + ") connection has been lost");
}

void Service::start(int stopEventFd) {
    // Create event for handling sd-bus messages.
    sd_event* evnt = NULL;
    int ret = sd_event_default(&evnt);
    if (ret < 0) {
        throw runtime_error("Could not create event: " +
                            string(strerror(-ret)));
    }

    event.reset(evnt);

    // Enable automatic service watchdog support.
    ret = sd_event_set_watchdog(event.get(), true);
    if (ret < 0) {
        throw runtime_error("Could not set watchdog for event: " +
                            string(strerror(-ret)));
    }

    // Process all pending sd-bus events before doing any sd-bus related async
    // waits with an I/O service.
    LOG.debug(THIS_NAMESPACE + __func__ +
              "(): Processing initial D-Bus events");
    while (bus.process_discard() > 0) {
        LOG.debug(THIS_NAMESPACE + __func__ +
                  "(): Initial skip of D-Bus event");
    }

    // Add sd-bus messages I/O event.
    ret = sd_event_add_io(event.get(), nullptr, bus.get_fd(),
                          EPOLLIN | EPOLLPRI | EPOLLRDHUP | EPOLLERR | EPOLLHUP,
                          sdbusIOHandler, &bus);
    if (ret < 0) {
        throw runtime_error("Could not add I/O event source for sd-bus bus: " +
                            string(strerror(-ret)));
    }

    // Add service stop eventfd.
    if (stopEventFd >= 0) {
        ret = sd_event_add_io(event.get(), nullptr, stopEventFd,
                              EPOLLIN | EPOLLPRI | EPOLLRDHUP | EPOLLERR |
                                  EPOLLHUP,
                              stopServiceIOHandler, this);
        if (ret < 0) {
            throw runtime_error(
                "Could not add I/O event source for service stop eventfd: " +
                string(strerror(-ret)));
        }
    }

    // Notify the service manager.
    sd_notifyf(false, "READY=1\n"
                      "STATUS=Daemon startup completed, processing events...");

    // Run event loop.
    LOG.info("Service started");
    ret = sd_event_loop(event.get());
    if (ret < 0) {
        LOG.error("Service stopped (" + to_string(ret) +
                  "): " + string(strerror(-ret)));
    } else {
        LOG.info("Service stopped");
    }
}

void Service::stop() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopping service...");

    if (!event) {
        throw runtime_error("Service is not running");
    }

    // Stop the event loop.
    int ret = sd_event_exit(event.get(), EXIT_SUCCESS);
    if (ret < 0) {
        throw runtime_error("Could not stop service: " +
                            string(strerror(-ret)));
    }

    // Tell sessions to stop processing.
    for (const auto& [ignore, session] : sessions) {
        session->stopBusProcessing();
    }
}

tuple<uint64_t, sdbusplus::message::unix_fd, sdbusplus::message::unix_fd,
      int32_t, string>
    Service::createSession(string clientName, uint32_t versionMajor,
                           uint32_t versionMinor, uint32_t versionPatch) {
    const uint64_t ID = Session::getNextId();

    // Compare library and service versions.
    if (versionMajor != LAROD_VERSION_MAJOR ||
        versionMinor != LAROD_VERSION_MINOR ||
        versionPatch != LAROD_VERSION_PATCH) {
        string libVersion = to_string(versionMajor) + "." +
                            to_string(versionMinor) + "." +
                            to_string(versionPatch);
        string errMsg = "Could not create session";
        string versionInfo = "version mismatch between library (" + libVersion +
                             ") and service (" + LAROD_VERSION_STR + ")";
        LOG.error(errMsg + " with ID " + to_string(ID) + ": " + versionInfo);

        return {0, 0, 0, LAROD_ERROR_VERSION_MISMATCH,
                errMsg + ": " + versionInfo};
    }

    pair<unordered_map<string, shared_ptr<Session>>::iterator, bool> retSession;
    try {
        retSession = sessions.emplace(
            clientName,
            make_shared<Session>(
                bus, SERVICE_OBJ_PATH + "/Session/" + to_string(ID), units));
    } catch (exception& e) {
        string errMsg = "Could not create session";
        LOG.error(errMsg + " with ID " + to_string(ID) + ": " + e.what());

        return {0, 0, 0, LAROD_ERROR_CREATE_SESSION, errMsg + ": " + e.what()};
    }

    if (!retSession.second) {
        // Should never happen...
        throw runtime_error("Could not create session: Client " + clientName +
                            " already exists");
    }

    // Create a match triggering the callback when client disconnects from bus.
    pair<unordered_map<string, sdbusplus::bus::match::match>::iterator, bool>
        retMatches;
    try {
        retMatches = matches.insert(make_pair(
            clientName,
            sdbusplus::bus::match::match(
                bus, filterNameChangeMatch(clientName),
                bind(&Service::clientLostHandler, this, placeholders::_1))));
    } catch (exception& e) {
        string errMsg = "Could not create a D-Bus match rule";
        LOG.error(errMsg + " for " + clientName + ": " + e.what());

        return {0, 0, 0, LAROD_ERROR_CREATE_SESSION, errMsg + ": " + e.what()};
    }

    if (!retMatches.second) {
        // Should never happen...
        throw runtime_error("Could not create a match rule: Client " +
                            clientName + " already exists");
    }

    // Save the client for this session.
    pair<unordered_map<uint64_t, string>::iterator, bool> retClients =
        clients.insert(make_pair(ID, clientName));
    if (!retClients.second) {
        // Should never happen...
        throw runtime_error("Could not create a client for session: ID " +
                            to_string(ID) + " already exists");
    }

    LOG.info("Created a new session ID: " +
             to_string(retSession.first->second->getSessionId()) +
             ", client: " + clientName);

    const auto [readFd, writeFd] =
        retSession.first->second->getClientAsyncFds();

    return {ID, readFd, writeFd, LAROD_ERROR_NONE, ""};
}

void Service::deleteSession(const uint64_t sessionId) {
    string clientName;
    try {
        clientName = clients.at(sessionId);
    } catch (out_of_range& e) {
        throw runtime_error("Session with id " + to_string(sessionId) +
                            " does not have a corresponding D-Bus client name");
    }

    if (!sessions.erase(clientName)) {
        throw runtime_error("Session with D-Bus client name " + clientName +
                            " does not exist");
    }

    if (!clients.erase(sessionId)) {
        throw runtime_error("Session with id " + to_string(sessionId) +
                            " does not have a corresponding D-Bus client name");
    }

    // We do no longer need to listen to the D-Bus signal triggered when this
    // client disconnects from the bus.
    if (!matches.erase(clientName)) {
        throw runtime_error("Session with D-Bus client name " + clientName +
                            " has no client lost match");
    }
}

uint64_t Service::nbrOfSessions() const {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): " + to_string(sessions.size()));
    return sessions.size();
}

vector<int32_t> Service::availableChips() const {
    vector<int32_t> chips;
    chips.reserve(units.size());
    for (const auto& unit : units) {
        int32_t chip = unit->getChip();
        chips.push_back(chip);
        LOG.debug(THIS_NAMESPACE + __func__ + "(): " + to_string(chip));
    }

    return chips;
}

} // namespace larod
