/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "jobrequest.hh"

#include <cassert>
#include <cerrno>
#include <unistd.h>

#include "log.hh"
#include "session.hh"

using namespace std;

namespace larod {

JobRequest::JobRequest(weak_ptr<Session> session, const uint64_t modelId,
                       vector<Tensor>&& inputTensors,
                       vector<Tensor>&& outputTensors, const uint8_t priority,
                       const uint64_t metaData, const ParamsMap&& params)
    : AsyncMsg(session), MODEL_ID(modelId),
      INPUT_TENSORS(std::move(inputTensors)),
      OUTPUT_TENSORS(std::move(outputTensors)), PRIORITY(priority),
      META_DATA(metaData), PARAMS(std::move(params)) {
}

void JobRequest::signal() {
    if (auto sessionPtr = session.lock()) {
        sessionPtr->sendJobResult(this);
    } else {
        LOG.warning("Could not send job result: Session has expired");
    }
}

} // namespace larod
