/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <memory>
#include <string.h>

#include "allocator.hh"
#include "paramsmap.hh"

namespace larod {
namespace allocator {

/**
 * @brief Allocator that uses the cavalry API.
 */
class Cavalry : public Allocator {
public:
    ~Cavalry();

    Cavalry(const Cavalry&) = delete;
    Cavalry& operator=(const Cavalry&) = delete;

    /**
     * @brief Get an instance of this singleton.
     *
     * @return An instance of this class.
     */
    static std::shared_ptr<Cavalry> getInstance() {
        // Since Cavalry() is private, we cannot call
        // make_shared<Cavalry>() here. Instead we have to use
        // `new`. Note that this is also guaranteed to be thread-safe
        // (since C++11).
        static std::shared_ptr<Cavalry> instance(new Cavalry);

        return instance;
    }

    const FileDescriptor& getCavalryFd() { return fdCavalry; }

    /**
     * @brief Flushes the cache with Cavalry for the allocated memory.
     *
     * Should be called whenever CPU has written to the allocated memory
     * and something else wants to read.
     */
    static void flushCache(const int fd, const size_t size,
                           const int64_t offset);

    /**
     * @brief Invalidates the cache with Cavalry for the allocated
     * memory.
     *
     * Should be called whenever something has written to the allocated
     * memory and CPU wants to read.
     */
    static void invalidateCache(const int fd, const size_t size,
                                const int64_t offset);

protected:
    std::unique_ptr<larod::Buffer>
        allocateVirtual(const size_t size, const uint32_t fdProps,
                        const ParamsMap& params) override;

private:
    static inline std::string THIS_NAMESPACE = "Cavalry::";

    Cavalry();

    FileDescriptor fdCavalry;

public:
    class Buffer : public larod::Buffer {
    public:
        Buffer(const int fd, const size_t maxSize, const uint32_t fdProps,
               void* virtAddr);
        ~Buffer();

        /**
         * @brief Flushes the cache with Cavalry for the allocated memory.
         *
         * Should be called whenever CPU has written to the allocated memory
         * and something else wants to read.
         */
        void flushCache() const;

        /**
         * @brief Invalidates the cache with Cavalry for the allocated
         * memory.
         *
         * Should be called whenever something has written to the allocated
         * memory and CPU wants to read.
         */
        void invalidateCache() const;

        // Is equivalent to pointer returned by getMapping().
        void* virtAddr = nullptr;

    private:
        static inline std::string THIS_NAMESPACE =
            Cavalry::THIS_NAMESPACE + "Buffer::";

        // TODO: Refactor cavFd and virtAddr members.
        // Duplicated fd and mmapped address are saved here in order to make
        // the destructor work properly. The superclass' fd member will do a
        // regular close when Cavalry needs to do cavalry_mem_free_mfd().
        // Therefore, cavFd can not be an instance of FileDescriptor.
        int cavFd = -1;
    };

public:
    /**
     * @brief Returns a downcasted pointer to a buffer if the buffer is an
     * instance of @c Cavalry::Buffer.
     *
     * @param buf The buffer to cast.
     * @return A pointer to a downcasted type. If @c buf is not an instance of
     * @c Cavalry::Buffer, @c nullptr is returned.
     */
    static Cavalry::Buffer* castBuffer(const larod::Buffer& buf);

    /**
     * @brief Cast a @c larod::Buffer to a @c Cavalry::Buffer.
     *
     * The returned pointer will have exclusive ownership of the buffer.
     *
     * @param ptr The buffer to convert to a @c Cavalry::Buffer.
     * @return A pointer with exclusive ownership to the downcasted type.
     */
    static std::unique_ptr<Cavalry::Buffer>
        convertBufferPtr(std::unique_ptr<larod::Buffer>&& ptr);
};

} // namespace allocator
} // namespace larod
