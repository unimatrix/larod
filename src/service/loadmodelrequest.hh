/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <utility>
#include <variant>
#include <vector>

#include "asyncmsg.hh"
#include "model.hh"
#include "paramsmap.hh"
#include "tensormetadata.hh"

namespace larod {

class BackEndUnit; // Forward declaration.

class LoadModelRequest : public AsyncMsg {
    friend class BackEndUnit;

public:
    LoadModelRequest(std::weak_ptr<Session> session, BackEndUnit* backEndUnit,
                     const uint64_t creatorId, const uint64_t chipId,
                     const std::vector<uint8_t>&& data,
                     const Model::Access access, const std::string name,
                     const uint64_t metaData, const ParamsMap&& params);

    void signal() override;
    uint64_t getModelId() const { return modelId; }
    void setTensorMetadata(
        std::pair<std::vector<TensorMetadata>, std::vector<TensorMetadata>>&&
            tensorMetadata);
    void setTensorMetadata(
        const std::pair<std::vector<TensorMetadata>,
                        std::vector<TensorMetadata>>& tensorMetadata);
    std::pair<std::vector<TensorMetadata>, std::vector<TensorMetadata>>
        getTensorMetadata() const {
        return {inputTensorMetadata, outputTensorMetadata};
    }

    const uint64_t CREATOR_ID;
    const uint64_t CHIP_ID;
    const std::vector<uint8_t> DATA;
    const Model::Access ACCESS;
    const std::string NAME;
    const uint64_t META_DATA;
    const ParamsMap PARAMS;

private:
    uint64_t modelId;
    BackEndUnit* backEndUnit; // Used only as a std::experimental::observer_ptr.
    std::vector<TensorMetadata> inputTensorMetadata;
    std::vector<TensorMetadata> outputTensorMetadata;
};

} // namespace larod
