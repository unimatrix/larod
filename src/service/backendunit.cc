/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "backendunit.hh"

#include <thread>

#include "log.hh"
#include "model.hh"
#include "tensor.hh"

using namespace std;

namespace larod {

BackEndUnit::BackEndUnit(shared_ptr<allocator::Allocator> inputAllocator,
                         shared_ptr<allocator::Allocator> outputAllocator)
    : inputBufAllocator(inputAllocator), outputBufAllocator(outputAllocator),
      waitForJobQueue(true), waitForLoadModelQueue(true),
      keepProcessingQueues(false) {
}

BackEndUnit::~BackEndUnit() {
    // Sub-classes should've already stopped any processing of the queues.
    assert(!keepProcessingQueues);
    assert(!procQueueThreads.size());
    stopProcessingQueues();
}

Model BackEndUnit::loadModel(const uint64_t callerId, const span<uint8_t>& data,
                             const Model::Access access, const string& name,
                             const ParamsMap& params) {
    lock_guard<mutex> lockGuard(mtxModels);

    const auto& [inputTensorsMetadata, outputTensorsMetadata] =
        loadModelVirtual(data, modelId, params);
    const Model& model = addModel(callerId, modelId, name, data.size(), access,
                                  inputTensorsMetadata, outputTensorsMetadata);
    ++modelId;

    return model;
}

const modelformat::Model*
    BackEndUnit::verifyFbModel(const span<uint8_t>& model) {
    flatbuffers::Verifier verifier(model.data(), model.size());
    if (!modelformat::VerifyModelBuffer(verifier)) {
        return nullptr;
    }

    const modelformat::Model* fbModel = modelformat::GetModel(model.data());
    int version = getMinModelFormatVersion();
    if (fbModel->version() < version) {
        throw invalid_argument("Model has format version " +
                               to_string(fbModel->version()) +
                               " but chip requires >= " + to_string(version));
    }

    return fbModel;
}

Model&
    BackEndUnit::addModel(const uint64_t& callerId, const uint64_t& modelId,
                          const string& name, const size_t& size,
                          const Model::Access& access,
                          const vector<TensorMetadata> inputTensorMetadata,
                          const vector<TensorMetadata> outputTensorMetadata) {
    auto [it, ret] = models.emplace(
        piecewise_construct, forward_as_tuple(modelId),
        forward_as_tuple(modelId, name, size, access, inputTensorMetadata,
                         outputTensorMetadata));
    if (!ret) {
        // Should never happen...
        string msg = "Could not map ID to model: ID " + to_string(modelId) +
                     " already exists";
        LOG.error(msg);
        throw runtime_error(msg);
    }

    Model& model = it->second;
    ret = modelCreators.insert({model, callerId}).second;
    if (!ret) {
        // Should never happen...
        string msg = "Could not map model to creator: Model ID " +
                     to_string(modelId) + " already exists";
        LOG.error(msg);
        throw runtime_error(msg);
    }

    return model;
}

void BackEndUnit::addModel(LoadModelRequest* req) {
    // Check for errors.
    if (req->getException()) {
        return;
    }

    // Model has been loaded successfully.
    try {
        lock_guard<mutex> lockGuard(mtxModels);
        addModel(req->CREATOR_ID, req->getModelId(), req->NAME,
                 req->DATA.size(), req->ACCESS, req->inputTensorMetadata,
                 req->outputTensorMetadata);
    } catch (...) {
        req->setException(current_exception());
    }
}

void BackEndUnit::queueLoadModel(shared_ptr<LoadModelRequest> loadModelReq) {
    // Reserve a future model ID.
    {
        lock_guard<mutex> lockGuard(mtxModels);
        loadModelReq->modelId = modelId++;
    }

    {
        lock_guard<mutex> lockGuard(mtxLoadModelQueue);
        loadModelQueue.push(loadModelReq);
        LOG.debug(THIS_NAMESPACE + __func__ + "(): Pushed (size = " +
                  to_string(loadModelQueue.size()) + ")");
    }

    condVarLoadModelQueue.notify_all();
}

void BackEndUnit::deleteModel(const uint64_t callerId, const uint64_t modelId) {
    lock_guard<mutex> lockGuard(mtxModels);

    auto it = models.find(modelId);
    if (it == models.end()) {
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    Model& model = it->second;
    if (hasPermission(callerId, model)) {
        deleteModelVirtual(modelId);
    } else {
        throw PermissionDenied("Caller " + to_string(callerId) +
                               " does not have permissions for model " +
                               string(model));
    }

    // Delete from modelCreators
    bool removed = modelCreators.erase(model);
    if (!removed) {
        // Should never happen...
        LOG.error("Could not find creator for model " + to_string(modelId));
    }

    // Delete from models
    models.erase(it);
}

void BackEndUnit::queueJob(shared_ptr<JobOrder> jobOrder) {
    lock_guard<mutex> lockGuard(mtxJobQueue);

    jobQueue.push(jobOrder);
    condVarJobQueue.notify_all();

    LOG.debug(THIS_NAMESPACE + __func__ +
              "(): Pushed (size = " + to_string(jobQueue.size()) + ")");
}

bool BackEndUnit::hasPermission(const uint64_t callerId, Model& model) {
    if (model.getAccessType() == Model::Access::PRIVATE) {
        try {
            uint64_t creator = modelCreators.at(model);
            if (creator != callerId) {
                return false;
            }
        } catch (out_of_range& e) {
            // Should never happen...
            LOG.error("Creator for model " + string(model) + " not found");
            throw;
        }
    }

    return true;
}

Model BackEndUnit::getModel(const uint64_t modelId) const {
    lock_guard<mutex> lockGuard(mtxModels);

    auto it = models.find(modelId);
    if (it == models.end()) {
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    return it->second;
}

unordered_map<uint64_t, Model> BackEndUnit::getModels() const {
    lock_guard<mutex> lockGuard(mtxModels);

    return models;
}

void BackEndUnit::checkTensors(
    const pair<const vector<Tensor>&, const vector<Tensor>&>& lhs,
    const pair<const vector<TensorMetadata>&, const vector<TensorMetadata>&>&
        rhs) {
    const auto& [lhsInputTensors, lhsOutputTensors] = lhs;
    const auto& [rhsInputTensors, rhsOutputTensors] = rhs;

    // Check number of inputs.
    if (lhsInputTensors.size() != rhsInputTensors.size()) {
        throw invalid_argument("Number of input tensors mismatch (got " +
                               to_string(lhsInputTensors.size()) +
                               ", expected " +
                               to_string(rhsInputTensors.size()) + ")");
    }

    // Check number of outputs.
    if (lhsOutputTensors.size() != rhsOutputTensors.size()) {
        throw TensorMismatch("Number of output tensors mismatch (got " +
                             to_string(lhsOutputTensors.size()) +
                             ", expected " +
                             to_string(rhsOutputTensors.size()) + ")");
    }

    // Check input attributes.
    for (size_t i = 0; i < lhsInputTensors.size(); ++i) {
        try {
            TensorMetadata::assertEqual(lhsInputTensors[i], rhsInputTensors[i]);
        } catch (invalid_argument& e) {
            throw TensorMismatch("Input tensor " + to_string(i) +
                                 " mismatch: " + e.what());
        }
    }

    // Check output attributes.
    for (size_t i = 0; i < lhsOutputTensors.size(); ++i) {
        try {
            TensorMetadata::assertEqual(lhsOutputTensors[i],
                                        rhsOutputTensors[i]);
        } catch (invalid_argument& e) {
            throw TensorMismatch("Output tensor " + to_string(i) +
                                 " mismatch: " + e.what());
        }
    }
}

void BackEndUnit::checkJobRequest(JobRequest& jobReq) {
    uint64_t sessionId;
    try {
        sessionId = jobReq.getSessionId();
    } catch (Msg::SessionExpired& e) {
        jobReq.setException(
            InvalidJobRequest(string("Could not get session ID: ") + e.what()));

        return;
    }

    // Note the position of locking mtxModels: We obtain the mtxModels lock only
    // AFTER we have performed jobReq.getSessionId() above. The call to
    // jobReq.getSessionId() can in rare cases lead to session destructor being
    // called, attempting to clean up private models for that session. The
    // clean-up operation leads to attempt to lock mtxModels possibly leading to
    // a deadlock if the non-recursive mtxModels is already locked in this
    // method.
    unique_lock<mutex> modelLock(mtxModels);

    // Check if model is valid.
    auto it = models.find(jobReq.MODEL_ID);
    if (it == models.end()) {
        jobReq.setException(ModelNotFound(
            "Model " + to_string(jobReq.MODEL_ID) + " not found"));

        return;
    }
    // Check permissions on model.
    Model& model = it->second;
    if (!hasPermission(sessionId, model)) {
        jobReq.setException(PermissionDenied(
            "Caller " + to_string(sessionId) +
            " does not have permissions for model " + string(model)));

        return;
    }

    const auto modelTensorMetadata = model.getTensorMetadata();

    modelLock.unlock();

    // Check input and output tensors against what model expects.
    try {
        checkTensors({jobReq.INPUT_TENSORS, jobReq.OUTPUT_TENSORS},
                     modelTensorMetadata);
    } catch (...) {
        jobReq.setException(current_exception());

        return;
    }

    const auto& [modelInputTensorMetadata, modelOutputTensorMetadata] =
        modelTensorMetadata;

    // Check input file descriptors.
    assert(jobReq.INPUT_TENSORS.size() == modelInputTensorMetadata.size());
    for (size_t i = 0; i < modelInputTensorMetadata.size(); ++i) {
        const auto& buffer = jobReq.INPUT_TENSORS[i].getBuffer();

        // Note that the fd size can be zero (which means that there is no
        // explicit set max size). If there is an explict set fd max size, it
        // cannot however be less than the byte size plus fd offset.
        if (buffer.getMaxSize() &&
            (static_cast<uint64_t>(buffer.getMaxSize()) <
             static_cast<uint64_t>(buffer.getStartOffset()) +
                 static_cast<uint64_t>(
                     modelInputTensorMetadata[i].getByteSize()))) {
            jobReq.setException(FileDescriptorError(
                "Input tensor " + to_string(i) + " has fd size (" +
                to_string(buffer.getMaxSize()) +
                ") less than required byte size (" +
                to_string(modelInputTensorMetadata[i].getByteSize()) +
                ") plus fd offset (" + to_string(buffer.getStartOffset()) +
                ")"));

            return;
        }
    }

    // Check output file descriptors.
    assert(jobReq.OUTPUT_TENSORS.size() == modelOutputTensorMetadata.size());
    for (size_t i = 0; i < modelOutputTensorMetadata.size(); ++i) {
        const auto& buffer = jobReq.OUTPUT_TENSORS[i].getBuffer();

        // Note that the fd size can be zero (which means that there is no
        // explicit set max size). If there is an explict set fd max size, it
        // cannot however be less than the byte size plus fd offset.
        if (buffer.getMaxSize() &&
            (static_cast<uint64_t>(buffer.getMaxSize()) <
             static_cast<uint64_t>(buffer.getStartOffset()) +
                 static_cast<uint64_t>(
                     modelOutputTensorMetadata[i].getByteSize()))) {
            jobReq.setException(FileDescriptorError(
                "Output tensor " + to_string(i) + " has fd size (" +
                to_string(buffer.getMaxSize()) +
                ") less than required byte size (" +
                to_string(modelOutputTensorMetadata[i].getByteSize()) +
                ") plus fd offset (" + to_string(buffer.getStartOffset()) +
                ")"));

            return;
        }
    }
}

shared_ptr<JobRequest> BackEndUnit::popJobQueueWait() {
    unique_lock<mutex> lockInfQueue(mtxJobQueue);

    waitForQueue(jobQueue, condVarJobQueue, lockInfQueue, waitForJobQueue);

    if (!waitForJobQueue) {
        // We got a stop waiting signal.
        return nullptr;
    }

    shared_ptr<JobRequest> jobReq = popQueue(jobQueue);
    lockInfQueue.unlock();

    checkJobRequest(*jobReq);

    return jobReq;
}

shared_ptr<LoadModelRequest> BackEndUnit::popLoadModelQueueWait() {
    unique_lock<mutex> lock(mtxLoadModelQueue);

    waitForQueue(loadModelQueue, condVarLoadModelQueue, lock,
                 waitForLoadModelQueue);

    if (!waitForLoadModelQueue) {
        // We got a stop waiting signal.
        return nullptr;
    }

    return popQueue(loadModelQueue);
}

template<typename T> T BackEndUnit::popQueue(queue<T>& queue) {
    if (queue.empty()) {
        throw runtime_error("Can not pop empty queue");
    }

    T element(queue.front());
    queue.pop();

    return element;
}

shared_ptr<JobRequest>
    BackEndUnit::popQueue(queue<shared_ptr<JobOrder>>& queue) {
    auto jobOrder = popQueue<shared_ptr<JobOrder>>(queue);
    shared_ptr<JobRequest> jobReq;
    try {
        jobReq = jobOrder->getJobRequest();
    } catch (Msg::SessionExpired& e) {
        throw InvalidJobRequest(string("Could not get job request: ") +
                                e.what());
    }

    return jobReq;
}

template<typename T, typename C, typename L>
void BackEndUnit::waitForQueue(queue<T>& queue, C& condVar,
                               unique_lock<L>& lock, atomic<bool>& pred) {
    if (!queue.empty()) {
        return;
    }

    // Queue is empty. Wait for signal.
    condVar.wait(lock, [&] { return !queue.empty() || !pred; });
}

void BackEndUnit::stopJobQueueWait() {
    lock_guard<mutex> lockGuard(mtxJobQueue);
    waitForJobQueue = false;
    condVarJobQueue.notify_all();
}

void BackEndUnit::stopLoadModelQueueWait() {
    lock_guard<mutex> lockGuard(mtxLoadModelQueue);
    waitForLoadModelQueue = false;
    condVarLoadModelQueue.notify_all();
}

size_t BackEndUnit::getJobQueueSize() const {
    lock_guard<mutex> lockGuard(mtxJobQueue);
    return jobQueue.size();
}

size_t BackEndUnit::getLoadModelQueueSize() const {
    lock_guard<mutex> lockGuard(mtxLoadModelQueue);
    return loadModelQueue.size();
}

void BackEndUnit::startProcessingQueues() {
    // Start queue processing threads.
    keepProcessingQueues = true;
    procQueueThreads.emplace_back(&BackEndUnit::processJobQueue, this);
    procQueueThreads.emplace_back(&BackEndUnit::processLoadModelQueue, this);
}

void BackEndUnit::stopProcessingQueues() noexcept {
    if (!procQueueThreads.size()) {
        // This is the only place where we join the queue processing threads and
        // erase them. If there aren't any threads, that means we have already
        // called this function before and there is therefore nothing to stop.
        return;
    }

    keepProcessingQueues = false;

    // Eventually notify them if they are waiting.
    stopJobQueueWait();
    stopLoadModelQueueWait();

    for (thread& th : procQueueThreads) {
        try {
            th.join();
        } catch (system_error& e) {
            LOG.error("Could not join thread: " + string(e.what()));
        }
    }

    procQueueThreads.clear();

    LOG.debug(THIS_NAMESPACE + __func__ +
              "(): Stopped queue processing threads");
}

void BackEndUnit::processJobQueue() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Started");

    while (keepProcessingQueues) {
        shared_ptr<JobRequest> jobReq;
        try {
            jobReq = popJobQueueWait();
        } catch (InvalidJobRequest& e) {
            // We just ignore this particular request.
            LOG.warning(string("Skipping job request: ") + e.what());
            continue;
        }

        // Check for nullptr, since that is returned if the wait is canceled.
        if (!jobReq) {
            if (!keepProcessingQueues) {
                // Returned from wait with signal to stop further processing.
                break;
            }

            throw runtime_error("Job request is null");
        } else if (jobReq->getException()) { // Check if the request is valid.
            jobReq->signal();
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Skipping job request; it had exceptions");
            continue;
        }

        try {
            runJobVirtual(jobReq->MODEL_ID, jobReq->INPUT_TENSORS,
                          jobReq->OUTPUT_TENSORS, jobReq->PARAMS);
        } catch (...) {
            jobReq->setException(current_exception());
        }

        // Signal job has finished.
        jobReq->signal();
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopped");
}

void BackEndUnit::processLoadModelQueue() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Started");

    while (keepProcessingQueues) {
        shared_ptr<LoadModelRequest> loadModelReq = popLoadModelQueueWait();

        // Check for nullptr, since that is returned if the wait is canceled.
        if (!loadModelReq) {
            if (!keepProcessingQueues) {
                // Returned from wait with signal to stop further processing.
                break;
            }

            throw runtime_error("Load model request is null");
        }

        try {
            loadModelReq->setTensorMetadata(loadModelVirtual(
                const_cast<vector<uint8_t>&>(loadModelReq->DATA),
                loadModelReq->getModelId(), loadModelReq->PARAMS));
        } catch (...) {
            loadModelReq->setException(current_exception());
        }

        // Signal model has been loaded.
        loadModelReq->signal();
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopped");
}
} // namespace larod
