/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <memory>
#include <string.h>

#include "buffer.hh"
#include "paramsmap.hh"

namespace larod {
namespace allocator {

class Allocator {
public:
    Allocator(const uint32_t supportedFdProps);
    virtual ~Allocator();

    /**
     * @brief Allocate a buffer.
     *
     * Mutex @c mtxAlloc will be locked before calling the corresponding pure
     * virtual function allocateVirtual(). All calls to allocateVirtual() will
     * thus be synchronized.
     *
     * @param size Size to allocate.
     * @param fdProps Fd props for allocation. At least one bit is guaranteed to
     * match @c supportedFdProps (i.e. `(fdProps & supportedFdProps) != 0`).
     * @param params Extra optional params.
     * @return Allocated buffer.
     */
    std::unique_ptr<Buffer> allocate(const size_t size, const uint32_t fdProps,
                                     const ParamsMap& params);

protected:
    virtual std::unique_ptr<Buffer>
        allocateVirtual(const size_t size, const uint32_t fdProps,
                        const ParamsMap& params) = 0;

private:
    static inline std::string THIS_NAMESPACE = "Allocator::";

    const uint32_t supportedFdProps;
    mutable std::mutex mtxAlloc;
};

} // namespace allocator
} // namespace larod
