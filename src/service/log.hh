/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <chrono>
#include <fstream>
#include <mutex>
#include <string>
#include <unistd.h>

namespace larod {

/**
 * @brief Internal logger to log information, warnings and errors.
 *
 * The global variable @c LOG is used in different files across the program to
 * log.
 */
class Log {
public:
    /// Time format of messages.
    static inline const std::string TIME_FORMAT = "%Y-%m-%dT%H:%M:%S";

    Log();

    /**
     * @brief Print debug information.
     *
     * @param msg Debug message to log.
     */
    static void debug(const std::string& msg);

    /**
     * @brief Print information.
     *
     * @param msg Information message to log.
     */
    static void info(const std::string& msg);

    /**
     * @brief Print warning.
     *
     * @param msg Warning message to log.
     */
    static void warning(const std::string& msg);

    /**
     * @brief Print error.
     *
     * @param msg Error message to log.
     */
    static void error(const std::string& msg);

    /**
     * @brief Set if logging should print to syslog.
     *
     * @param print Flag to indicate logging to syslog. False means no.
     */
    static void setPrintToSyslog(bool print) {
        printToSyslog = print;
    }

private:
    static inline const std::string ANSI_BOLD = "\x1B[1m";
    static inline const std::string ANSI_BOLD_COLOR_RED = "\x1B[1;31m";
    static inline const std::string ANSI_BOLD_COLOR_YELLOW = "\x1B[1;33m";
    static inline const std::string ANSI_RESET = "\x1B[0m";

    static inline const bool PRINT_TO_TERMINAL =
        isatty(STDOUT_FILENO) || isatty(STDERR_FILENO);
    static inline bool printToSyslog = true;
    static inline std::mutex mtx;

    /**
     * @brief Prints message to a specific output stream.
     *
     * @param msg Message to print.
     * @param os Stream to print to.
     */
    static void print(const std::string& msg, std::ostream& os);
};

extern const Log LOG; ///< initialization of program's internal logger (global)

} // namespace larod
