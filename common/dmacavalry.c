/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "dmacavalry.h"

#include <assert.h>
#include <cavalry/cavalry_ioctl.h>
#include <cavalry_mem.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "log.h"

static int fdCavalry = -1;

bool cavalryInit() {
    fdCavalry = open(CAVALRY_DEV_NODE, O_RDWR, 0);
    if (fdCavalry < 0) {
        logError("Could not open cav mem: %s", strerror(errno));

        return false;
    }

    // cavalry_mem_init() can only be called once per process.
    if (cavalry_mem_init(fdCavalry, 0) < 0) {
        logError("Could not init cav mem");

        return false;
    }

    return true;
}

void cavalryExit() {
    cavalry_mem_exit();

    if (fdCavalry >= 0 && close(fdCavalry)) {
        logWarning("Could not close cavalry fd");
    }
}

bool cavalryAllocFd(size_t size, DmaBuffer* buffer) {
    assert(buffer);

    int ret = 0;
    uint8_t nrTries = 8;
    do {
        ret = cavalry_mem_alloc_mfd(size, &buffer->fd, &buffer->data, 1);
        // The kernel allocation may fail and in that case the cavalry
        // driver will return EBUSY (by setting errno). Let's try again...
    } while (ret < 0 && errno == EBUSY && --nrTries);

    if (ret) {
        logError("Could not alloc cav mem fd");

        return false;
    }

    buffer->size = size;

    return true;
}

bool cavalryFreeFd(DmaBuffer* buffer) {
    int ret = cavalry_mem_free_mfd(buffer->size, buffer->fd, buffer->data);
    if (ret) {
        logError("Could not free cavalry memory fd");

        return false;
    }

    return true;
}

bool cavalryWriteData(void* data, size_t size, DmaBuffer* buffer) {
    // Write to mapped memory.
    memcpy(buffer->data, data, size);

    int ret =
        cavalry_mem_sync_cache_mfd(buffer->size, 0, buffer->fd, true, false);
    if (ret) {
        logError("Could not sync cache");

        return false;
    }

    return true;
}

const FdOps cavalryOps = {
    .initFramework = cavalryInit,
    .exitFramework = cavalryExit,
    .allocFd = cavalryAllocFd,
    .freeFd = cavalryFreeFd,
    .writeData = cavalryWriteData,
};
