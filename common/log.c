/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "log.h"

#include <errno.h>
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define NSEC_PER_MSEC 1000000L

static void logPrintTime(FILE* stream);

/**
 * @brief Wrapper for isatty().
 *
 * This caches the results from isatty() for stdout, stderr and stdin once and
 * returns these values for all future calls.
 *
 * @param fd File descriptor to pass to isatty().
 * @return True if isatty() returns 1, otherwise false.
 */
static bool isattyWrapper(int fd);

const char* TIME_FORMAT = "%d-%02d-%02dT%02d:%02d:%02d.%03d";

const char* ANSI_BOLD = "\x1B[1m";
const char* ANSI_BOLD_COLOR_RED = "\x1B[1;31m";
const char* ANSI_BOLD_COLOR_YELLOW = "\x1B[1;33m";
const char* ANSI_RESET = "\x1B[0m";

static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;

void logDebug(bool debug, const char* fmt, ...) {
    if (!debug) {
        return;
    }

    pthread_mutex_lock(&mtx);

    logPrintTime(stdout);
    const bool PRINT_TO_TERMINAL = isattyWrapper(STDOUT_FILENO);
    if (PRINT_TO_TERMINAL) {
        fprintf(stdout, "%sDEBUG: ", ANSI_BOLD);
    } else {
        fprintf(stdout, "DEBUG: ");
    }

    va_list args;
    va_start(args, fmt);

    vfprintf(stdout, fmt, args);
    if (PRINT_TO_TERMINAL) {
        fprintf(stdout, "%s\n", ANSI_RESET);
    } else {
        fprintf(stdout, "\n");
    }

    va_end(args);

    pthread_mutex_unlock(&mtx);
}

void logInfo(const char* fmt, ...) {
    pthread_mutex_lock(&mtx);

    logPrintTime(stdout);

    va_list args;
    va_start(args, fmt);

    vfprintf(stdout, fmt, args);
    fprintf(stdout, "\n");

    va_end(args);

    pthread_mutex_unlock(&mtx);
}

void logWarning(const char* fmt, ...) {
    pthread_mutex_lock(&mtx);

    logPrintTime(stderr);
    const bool PRINT_TO_TERMINAL = isattyWrapper(STDERR_FILENO);
    if (PRINT_TO_TERMINAL) {
        fprintf(stderr, "%sWARNING: ", ANSI_BOLD_COLOR_YELLOW);
    } else {
        fprintf(stderr, "WARNING: ");
    }

    va_list args;
    va_start(args, fmt);

    vfprintf(stderr, fmt, args);
    if (PRINT_TO_TERMINAL) {
        fprintf(stderr, "%s\n", ANSI_RESET);
    } else {
        fprintf(stderr, "\n");
    }

    va_end(args);

    pthread_mutex_unlock(&mtx);
}

void logError(const char* fmt, ...) {
    pthread_mutex_lock(&mtx);

    logPrintTime(stderr);
    const bool PRINT_TO_TERMINAL = isattyWrapper(STDERR_FILENO);
    if (PRINT_TO_TERMINAL) {
        fprintf(stderr, "%sERROR: ", ANSI_BOLD_COLOR_RED);
    } else {
        fprintf(stderr, "ERROR: ");
    }

    va_list args;
    va_start(args, fmt);

    vfprintf(stderr, fmt, args);
    if (PRINT_TO_TERMINAL) {
        fprintf(stderr, "%s\n", ANSI_RESET);
    } else {
        fprintf(stderr, "\n");
    }

    va_end(args);

    pthread_mutex_unlock(&mtx);
}

static void logPrintTime(FILE* stream) {
    static struct timespec ts;

    if (clock_gettime(CLOCK_REALTIME, &ts)) {
        fprintf(stream, "[clock error: %s] ", strerror(errno));
        return;
    }

    static struct tm* tm;
    tm = localtime(&ts.tv_sec);

    fprintf(stream, TIME_FORMAT, tm->tm_year + 1900, tm->tm_mon + 1,
            tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec,
            ts.tv_nsec / NSEC_PER_MSEC);
    fprintf(stream, " ");
}

static bool isattyWrapper(int fd) {
    static int stdoutCache = -1;
    static int stderrCache = -1;
    static int stdinCache = -1;

    switch (fd) {
    case STDOUT_FILENO:
        if (stdoutCache == -1) {
            stdoutCache = isatty(fd);
        }

        return stdoutCache;
    case STDERR_FILENO:
        if (stderrCache == -1) {
            stderrCache = isatty(fd);
        }

        return stderrCache;
    case STDIN_FILENO:
        if (stdinCache == -1) {
            stdinCache = isatty(fd);
        }

        return stdinCache;
    }

    return isatty(fd);
}
