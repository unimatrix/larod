#!/usr/bin/env python3

"""
Script to convert a TFLite or CvFlow model to larod's model format.

Copyright 2019 Axis Communications
SPDX-License-Identifier: Apache-2.0
"""

__version__ = "2.0"
__author__ = "Axis Communications"
__url__ = "https://gitlab.com/unimatrix/larod"

import argparse
import logging
import os
import sys

import larodconverter

# Argument parsing.
arg_parser = argparse.ArgumentParser(
    description="Converts a TFLite (.tflite) or CvFlow (.bin) model to larod's"
    " model format.",
    epilog="Report bugs to " + __author__ + ".")
arg_parser.add_argument(
    "input_format",
    metavar="INPUT_FORMAT",
    nargs=1,
    help="String specifying the format of the model which is to be converted."
    " Valid options are \"tflite\" and \"cvflow\".")
arg_parser.add_argument(
    "input_file",
    metavar="INPUT_FILE",
    nargs=1,
    help="Path to model file to be converted.")
arg_parser.add_argument(
    "-o",
    "--output_file",
    metavar="OUTPUT_FILE",
    help="Path to output file, which will be in a format that larod can parse."
    " Default path is INPUT_FILE but with .larod file extension instead.")
arg_parser.add_argument(
    "-d", "--debug", action="store_true", help="output debug information.")
arg_parser.add_argument(
    "-v",
    "--version",
    action="version",
    version="larod-convert " + __version__)
args = arg_parser.parse_args()

# Setup logger.
logging.basicConfig(format="%(levelname)s: %(message)s")
log = logging.getLogger(__name__)
if args.debug:
    log.setLevel(logging.DEBUG)
else:
    log.setLevel(logging.INFO)

# Dictionary which from user input will be populated, then input to converter.
converter_args = dict()

log.debug("Parsing input")
# Make sure input file is valid.
input_file = args.input_file[0]
if not (os.path.isfile(input_file)):
    sys.exit("ERROR: Could not find input file \"" + input_file + "\"")
converter_args["input_file"] = input_file

# Make sure requested input format is valid.
input_format = args.input_format[0].lower()
if not (input_format == "tflite" or input_format == "cvflow"):
    sys.exit("ERROR: Invalid input format \"" + input_format + "\" given")

# Give a default name to output file if not specified.
if args.output_file:
    output_file = args.output_file
else:
    output_file = os.path.splitext(input_file)[0] + ".larod"
converter_args["output_file"] = output_file

log.debug("Creating converter")
if input_format == "tflite":
    converter = larodconverter.TFLiteConverter(**converter_args)
else:
    converter = larodconverter.CvFlowConverter(**converter_args)
log.info("Building the model flatbuffer")
converter.build()
log.debug("Writing the model to file")
converter.output()

log.info("A larod model \"" + output_file + "\" has been produced")
